# Db

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.3.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

# Punch List

- Android and IOS mobile apps with Ionic Capactor
- setup CI/CD
- strongs numbers with more than one number should appear in tabs or you need to update your text to assign only one number per word(s)
-    currently, you open more than one card, one for each number.

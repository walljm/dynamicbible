import { Component, Inject } from '@angular/core';
import { FormGroup, UntypedFormBuilder } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CardItem } from 'src/app/models/card-state';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-card-edit-modal',
  templateUrl: 'card-edit-modal.component.html',
  styleUrls: ['./card-edit-modal.component.scss'],
})
export class CardEditModalComponent {
  form: FormGroup;
  dialogTitle = 'Save Page using Current Cards';
  oldCard: CardItem;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: CardEditModalData,
    public dialogRef: MatDialogRef<CardEditModalComponent>,
    private appService: AppService,
    private fb: UntypedFormBuilder
  ) {
    this.oldCard = data.card;
    this.form = this.fb.group(data.card);
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.appService.updateCard(
      {
        qry: this.form.get('qry').value,
        type: this.form.get('type').value,
      } as CardItem,
      this.oldCard
    );
    this.dialogRef.close();
  }
}

export interface CardEditModalData {
  card: CardItem;
}

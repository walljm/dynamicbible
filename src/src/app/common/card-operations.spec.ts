import { CardItem,CardType } from '../models/card-state';
import { BibleReference, Overlap } from './bible-reference';
import { mergeCardList } from './card-operations';

describe('Card Merging', () => {
  it('Should merge two equal reference cards', () => {
    const cardList: CardItem[] = [
      { type: CardType.Passage, qry: 'Genesis 1:1', data: null },
      { type: CardType.Passage, qry: 'Genesis 1:1', data: null },
    ];
    for (const strat of [Overlap.Equal, Overlap.Subset]) {
      const merged = mergeCardList(cardList, strat);
      expect(merged.length).toBe(1);
      expect(merged[0].qry).toEqual(new BibleReference('Genesis 1:1').toString());
    }
  });

  it('Should merge two equal word cards', () => {
    const cardList: CardItem[] = [
      { type: CardType.Word, qry: 'sin', data: null },
      { type: CardType.Word, qry: 'sin', data: null },
    ];
    for (const strat of [Overlap.Equal]) {
      const merged = mergeCardList(cardList, strat);
      expect(merged.length).toBe(1);
      expect(merged[0].qry).toEqual('sin');
    }
  });

  it('Should merge two equal word cards with one in the middle', () => {
    const cardList: CardItem[] = [
      { type: CardType.Word, qry: 'sin', data: null },
      { type: CardType.Word, qry: 'love', data: null },
      { type: CardType.Word, qry: 'sin', data: null },
    ];
    for (const strat of [Overlap.Equal]) {
      const merged = mergeCardList(cardList, strat);
      expect(merged.length).toBe(2);
      expect(merged[0].qry).toEqual('sin');
      expect(merged[1].qry).toEqual('love');
    }
  });

  it('Should merge two equal strongs cards', () => {
    const cardList: CardItem[] = [
      { type: CardType.Strongs, qry: 'G123', data: null },
      { type: CardType.Strongs, qry: 'G123', data: null },
    ];
    for (const strat of [Overlap.Equal]) {
      const merged = mergeCardList(cardList, strat);
      expect(merged.length).toBe(1);
      expect(merged[0].qry).toEqual('G123');
    }
  });

  it('Should merge two equal strongs cards with one in the middle', () => {
    const cardList: CardItem[] = [
      { type: CardType.Strongs, qry: 'G123', data: null },
      { type: CardType.Passage, qry: 'Genesis 1:1', data: null },
      { type: CardType.Strongs, qry: 'G123', data: null },
    ];
    for (const strat of [Overlap.Equal]) {
      const merged = mergeCardList(cardList, strat);
      expect(merged.length).toBe(2);
      expect(merged[0].qry).toEqual('G123');
      expect(merged[1].qry).toEqual(new BibleReference('Genesis 1:1').toString());
    }
  });

  it('Should merge two equal cards with one in the middle', () => {
    const cardList: CardItem[] = [
      { type: CardType.Passage, qry: 'Genesis 1:1', data: null },
      { type: CardType.Passage, qry: 'Genesis 1:2', data: null },
      { type: CardType.Passage, qry: 'Genesis 1:1', data: null },
    ];
    for (const strat of [Overlap.Equal, Overlap.Subset]) {
      const merged = mergeCardList(cardList, strat);
      expect(merged.length).toBe(2);
      expect(merged[0].qry).toEqual(new BibleReference('Genesis 1:1').toString());
      expect(merged[1].qry).toEqual(new BibleReference('Genesis 1:2').toString());
    }
  });

  it('Should merge two intersecting cards', () => {
    const cardList: CardItem[] = [
      { type: CardType.Passage, qry: 'Genesis 1:1-2', data: null },
      { type: CardType.Passage, qry: 'Genesis 1:1', data: null },
    ];
    for (const strat of [Overlap.Intersect, Overlap.Subset]) {
      const merged = mergeCardList(cardList, strat);
      expect(merged.length).toBe(1);
      expect(merged[0].qry).toEqual(new BibleReference('Genesis 1:1 - 2').toString());
    }
  });

  it('Should merge two intersecting cards with one in the middle', () => {
    const cardList: CardItem[] = [
      { type: CardType.Passage, qry: 'Genesis 1:1-2', data: null },
      { type: CardType.Passage, qry: 'Genesis 1:4', data: null },
      { type: CardType.Passage, qry: 'Genesis 1:1', data: null },
    ];
    for (const strat of [Overlap.Intersect, Overlap.Subset]) {
      const merged = mergeCardList(cardList, strat);
      expect(merged.length).toBe(2);
      expect(merged[0].qry).toEqual(new BibleReference('Genesis 1:1 - 2').toString());
      expect(merged[1].qry).toEqual(new BibleReference('Genesis 1:4').toString());
    }
  });

  it('Should not merge two cards of different card types', () => {
    const cardList: CardItem[] = [
      { type: CardType.Passage, qry: 'Genesis 1:1', data: null },
      { type: CardType.Word, qry: 'sin', data: null },
    ];
    for (const strat of [Overlap.Equal, Overlap.Subset, Overlap.Intersect, Overlap.None]) {
      const merged = mergeCardList(cardList, strat);
      expect(merged.length).toBe(2);
      expect(merged[0].qry).toEqual(new BibleReference('Genesis 1:1').toString());
      expect(merged[1].qry).toEqual('sin');
    }
  });
});

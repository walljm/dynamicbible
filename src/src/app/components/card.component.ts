import { Clipboard } from '@angular/cdk/clipboard';
import { Component, ElementRef, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';

import { MoveDirection } from '../common/move-direction';
import { SubscriberBase } from '../common/subscriber-base';
import { AddToPageModalComponent } from '../components/add-to-page-modal/add-to-page-modal.component';
import { CardItem } from '../models/card-state';
import { AppService } from '../services/app.service';
import { CardEditModalComponent } from './card-edit-modal/card-edit-modal.component';

@Component({
  template: '',
})
export class CardComponent extends SubscriberBase {
  @Output()
  closeCard = new EventEmitter<CardItem>();

  @Input()
  cardItem: CardItem;

  icon$: Observable<string>;

  constructor(
    protected elementRef: ElementRef,
    protected dialog: MatDialog,
    protected appService: AppService,
    protected clipboard: Clipboard
  ) {
    super();
  }

  protected copyToClip(text: string) {
    this.clipboard.copy(text);
    const pending = this.clipboard.beginCopy(text);
    let remainingAttempts = 3;
    const attempt = () => {
      const result = pending.copy();
      if (!result && --remainingAttempts) {
        setTimeout(attempt, 10);
      } else {
        // Remember to destroy when you're done!
        pending.destroy();
      }
    };
    attempt();
  }

  close(ev) {
    let translate = 'translate3d(200%, 0, 0)';
    if (ev != null && ev.direction === 2) {
      translate = 'translate3d(-200%, 0, 0)';
    }
    const d = 250;
    this.elementRef.nativeElement.parentElement.animate(
      {
        transform: ['none', translate],
      },
      {
        fill: 'forwards',
        duration: d,
        iterations: 1,
        easing: 'ease-in-out',
      }
    );

    setTimeout(() => {
      this.closeCard.emit(this.cardItem);
    }, d);
  }

  addToSavedPage() {
    this.dialog.open(AddToPageModalComponent, {
      data: this.cardItem,
      autoFocus: 'content',
    });
  }

  moveCardDown() {
    this.appService.moveCard(this.cardItem, MoveDirection.Down);
  }

  moveCardUp() {
    this.appService.moveCard(this.cardItem, MoveDirection.Up);
  }

  editReferenceModal() {
    this.dialog.open(CardEditModalComponent, {
      data: {
        card: { ...this.cardItem } as CardItem,
      },
    });
  }
}

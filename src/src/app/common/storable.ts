export interface IStorable<T> {
  readonly createdOn: string;
  readonly type: StorableType;
  readonly value: T;
}

export class Storable<T> implements IStorable<T> {
  constructor(v: T, type: StorableType = StorableType.modified) {
    this.value = v;
    this.type = type;
    this.createdOn = new Date().toISOString();
  }

  type: StorableType;
  createdOn: string;
  value: T;
}

export interface UserVersion {
  version: number;
}

export enum StorableType {
  initial,
  modified,
}

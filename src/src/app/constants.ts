export const CardFonts = [
  'Merriweather',
  'PT Sans',
  'PT Serif',
  'Open Sans',
  'Roboto',
  'Roboto Condensed',
  'Inconsolata',
];

export const FirebaseConfig = {
  apiKey: 'AIzaSyA3UV4s56CV2EumgvZmyJBTyU-vhv0xhc8',
  authDomain: 'dynamicbible-7c6cf.firebaseapp.com',
  databaseURL: 'https://dynamicbible-7c6cf.firebaseio.com',
  projectId: 'dynamicbible-7c6cf',
  storageBucket: 'dynamicbible-7c6cf.appspot.com',
  messagingSenderId: '200739882604',
};
// export const FirebaseConfig = {
//   apiKey: 'AIzaSyA4b587psiOnpjbzu0t6z75A_hFksPyQkI',
//   authDomain: 'dynamic-bible-testing.firebaseapp.com',
//   databaseURL: 'https://dynamic-bible-testing.firebaseio.com',
//   projectId: 'dynamic-bible-testing',
//   storageBucket: 'dynamic-bible-testing.appspot.com',
//   messagingSenderId: '813845246474',
// };

﻿namespace DynamicBibleUtility.Geolocation;

/// <summary>
/// An index of Bible location data by verse reference.
/// </summary>
public class BibleLocationIndexByVerse
{
    /// <summary>
    /// A mapping of verse references to location names.
    /// The verse reference keys are stored using their short string form in order
    /// to simplify hashing (no custom hash code implementation) and to simplify
    /// JSON serialization (so that the short string form is only serialized).
    /// Only the location name, rather than the full location data, is stored
    /// since the name can be used in a parallel lookup in the <see cref="BibleLocationIndexByName" />.
    /// </summary>
    public readonly IDictionary<string, List<string>> VerseToLocationNameLookup = new Dictionary<string, List<string>>();

    /// <summary>
    /// Creates the index from the provided locations.
    /// </summary>
    /// <param name="locations">The locations to put in the index.</param>
    /// <exception cref="System.NullReferenceException">Thrown if the locations are null.</exception>
    public BibleLocationIndexByVerse(IEnumerable<BibleLocationReference> locations)
    {
        // INDEX THE LOCATIONS BY VERSE REFERENCE.
        foreach (var location in locations)
        {
            // ONLY INDEX THE LOCATION IF IT HAS GEOGRAPHIC COORDINATES.
            // The information currently isn't useful without these coordinates.
            if (location.HasGeographicCoordinates)
            {
                // INDEX THE LOCATION NAME BY ALL ITS VERSE REFERENCES.
                foreach (var verse_reference in location.VerseReferences)
                {
                    // MAKE SURE THE VERSE HAS AN EXISTING COLLECTION FOR LOCATION DATA.
                    var verse_reference_string = verse_reference.ToString();
                    var verse_reference_exists_in_index = VerseToLocationNameLookup.ContainsKey(verse_reference_string);
                    if (!verse_reference_exists_in_index)
                    {
                        VerseToLocationNameLookup[verse_reference_string] = [];
                    }

                    // ADD THE LOCATION NAME FOR THE VERSE.
                    VerseToLocationNameLookup[verse_reference_string].Add(location.Name);
                }
            }
        }
    }
}
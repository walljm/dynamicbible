import { COMMA, ENTER, SEMICOLON } from '@angular/cdk/keycodes';
import { Component, Inject } from '@angular/core';
import { UntypedFormBuilder,UntypedFormGroup } from '@angular/forms';
import { MatChipInputEvent } from '@angular/material/chips';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UUID } from 'angular2-uuid';
import { CardItem } from 'src/app/models/card-state';
import { NoteItem } from 'src/app/models/note-state';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-note-edit-modal',
  templateUrl: 'note-edit-modal.component.html',
  styleUrls: ['./note-edit-modal.component.scss'],
})
export class NoteEditModalComponent {
  noteForm: UntypedFormGroup;
  data: NoteItem;
  isNew = false;

  //#region Cross References
  visible = true;
  removable = true;
  addOnBlur = true;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON];
  references: string[] = [];
  //#endregion

  constructor(
    @Inject(MAT_DIALOG_DATA) public cardItem: CardItem,
    public dialogRef: MatDialogRef<NoteEditModalComponent>,
    private appService: AppService,
    private fb: UntypedFormBuilder
  ) {
    if (cardItem) {
      this.data = cardItem.data as NoteItem;
      this.references = this.data.xref.split(';').map((v) => v.trim());
    } else {
      this.isNew = true;
      this.data = {
        id: UUID.UUID(),
        title: 'Title Here',
        content: '# Content Here\nIn Markdown format.',
        xref: '',
      };
    }
    this.noteForm = this.fb.group(this.data);
  }

  //#region cross refs

  add(event: MatChipInputEvent): void {
    const value = event.value;

    if ((value || '').trim()) {
      this.references.push(value.trim());
    }
  }

  remove(reference: string): void {
    const index = this.references.indexOf(reference);

    if (index >= 0) {
      this.references.splice(index, 1);
    }
  }

  //#endregion

  cancel() {
    this.dialogRef.close();
  }

  save() {
    this.appService.saveNote({
      ...this.data,
      title: this.noteForm.get('title').value,
      xref: this.references.reduce((p, c) => `${p};${c}`),
      content: this.noteForm.get('content').value,
    });

    this.dialogRef.close();
  }
}

import { moveItemInArray } from '@angular/cdk/drag-drop';

import { MoveDirection } from './move-direction';

/**
 * Moves an item up (1 index towards 0) or down (1 index away from 0) immutably, returning a new array as the value.
 * @param items Array in which to move the item.
 * @param item Item to move up or down
 * @param direction MoveDirection to go.
 */
export function moveItemUpOrDown<T = any>(items: readonly T[], item: T, direction: MoveDirection): readonly T[] {
  const idx = items.indexOf(item);

  if (
    (idx === 0 && direction === MoveDirection.Up) || // can't go up if you're at the top
    (idx === items.length - 1 && direction === MoveDirection.Down) // can't go down if you're at the bottom
  ) {
    // you can't go up or down.
    return items;
  }

  return moveItem(items, idx, direction === MoveDirection.Up ? idx - 1 : idx + 1);
}

/**
 * Moves an item one index in an array to another immutably, returning a new array as the value.
 * @param items Array in which to move the item.
 * @param fromIndex Starting index of the item.
 * @param toIndex Index to which the item should be moved.
 */
export function moveItem<T = any>(items: readonly T[], fromIndex: number, toIndex: number): readonly T[] {
  const array = [...items];
  moveItemInArray(array, fromIndex, toIndex); // copy the array.
  return array;
}

import { BibleReference, Overlap } from './bible-reference';

describe('Reference', () => {
  it('Should understand the * shorthand', () => {
    const ref = new BibleReference('Gen 1:1-*');
    expect(ref.section.end.chapter).toBe(1);
    expect(ref.section.end.verse).toBe(31);
  });

  it('Should parse the reference: Gen 1-2', () => {
    const ref = new BibleReference('Gen 1-2').toString();
    expect(ref).toBe('Genesis 1:1 - 2:25');
  });

  it('Should parse the reference: Gen 1:1', () => {
    const ref = new BibleReference('Gen 1:1').toString();
    expect(ref).toBe('Genesis 1:1');
  });

  it('Should parse the reference: Gen 1:1-11', () => {
    const ref = new BibleReference('Gen 1:1-11').toString();
    expect(ref).toBe('Genesis 1:1 - 11');
  });

  it('Should parse the reference: John 3:16', () => {
    const ref = new BibleReference('John 3:16').toString();
    expect(ref).toBe('John 3:16');
  });

  it('Should parse the reference: John 3-6', () => {
    const ref = new BibleReference('Jn 3-6').toString();
    expect(ref).toBe('John 3:1 - 6:71');
  });

  it('Should parse the reference: 1 Corinthians 3:5-6:5', () => {
    const ref = new BibleReference('1 Corinthians 3:5-6:5').toString();
    expect(ref).toBe('1 Corinthians 3:5 - 6:5');
  });

  it('Should parse the reference: 2 Samuel 5:5-6:5', () => {
    expect(new BibleReference('II sam 5:5-6:5').toString()).toBe('2 Samuel 5:5 - 6:5');
  });

  const booknames = [
    { abbr: 'gen', actual: 'Genesis' },
    { abbr: 'ge', actual: 'Genesis' },
    { abbr: 'genesis', actual: 'Genesis' },
    { abbr: 'gn', actual: 'Genesis' },
    { abbr: 'exodus', actual: 'Exodus' },
    { abbr: 'ex', actual: 'Exodus' },
    { abbr: 'exo', actual: 'Exodus' },
    { abbr: 'exod', actual: 'Exodus' },
    { abbr: 'exd', actual: 'Exodus' },
    { abbr: 'leviticus', actual: 'Leviticus' },
    { abbr: 'lev', actual: 'Leviticus' },
    { abbr: 'le', actual: 'Leviticus' },
    { abbr: 'levi', actual: 'Leviticus' },
    { abbr: 'lv', actual: 'Leviticus' },
    { abbr: 'numbers', actual: 'Numbers' },
    { abbr: 'num', actual: 'Numbers' },
    { abbr: 'nu', actual: 'Numbers' },
    { abbr: 'numb', actual: 'Numbers' },
    { abbr: 'number', actual: 'Numbers' },
    { abbr: 'deuteronomy', actual: 'Deuteronomy' },
    { abbr: 'deut', actual: 'Deuteronomy' },
    { abbr: 'de', actual: 'Deuteronomy' },
    { abbr: 'dt', actual: 'Deuteronomy' },
    { abbr: 'deu', actual: 'Deuteronomy' },
    { abbr: 'joshua', actual: 'Joshua' },
    { abbr: 'josh', actual: 'Joshua' },
    { abbr: 'jos', actual: 'Joshua' },
    { abbr: 'judges', actual: 'Judges' },
    { abbr: 'jud', actual: 'Judges' },
    { abbr: 'jdg', actual: 'Judges' },
    { abbr: 'judg', actual: 'Judges' },
    { abbr: 'ruth', actual: 'Ruth' },
    { abbr: 'ru', actual: 'Ruth' },
    { abbr: '1 samuel', actual: '1 Samuel' },
    { abbr: '1 sa', actual: '1 Samuel' },
    { abbr: '1 sam', actual: '1 Samuel' },
    { abbr: '1 sml', actual: '1 Samuel' },
    { abbr: 'i samuel', actual: '1 Samuel' },
    { abbr: 'i sa', actual: '1 Samuel' },
    { abbr: 'i sam', actual: '1 Samuel' },
    { abbr: 'i sml', actual: '1 Samuel' },
    { abbr: '1st samuel', actual: '1 Samuel' },
    { abbr: '1st sa', actual: '1 Samuel' },
    { abbr: '1st sam', actual: '1 Samuel' },
    { abbr: '1st sml', actual: '1 Samuel' },
    { abbr: 'first samuel', actual: '1 Samuel' },
    { abbr: 'first sa', actual: '1 Samuel' },
    { abbr: 'first sam', actual: '1 Samuel' },
    { abbr: 'first sml', actual: '1 Samuel' },
    { abbr: '2 samuel', actual: '2 Samuel' },
    { abbr: '2 sa', actual: '2 Samuel' },
    { abbr: '2 sam', actual: '2 Samuel' },
    { abbr: '2 sml', actual: '2 Samuel' },
    { abbr: 'ii samuel', actual: '2 Samuel' },
    { abbr: 'ii sa', actual: '2 Samuel' },
    { abbr: 'ii sam', actual: '2 Samuel' },
    { abbr: 'ii sml', actual: '2 Samuel' },
    { abbr: '2nd samuel', actual: '2 Samuel' },
    { abbr: '2nd sa', actual: '2 Samuel' },
    { abbr: '2nd sam', actual: '2 Samuel' },
    { abbr: '2nd sml', actual: '2 Samuel' },
    { abbr: 'second samuel', actual: '2 Samuel' },
    { abbr: 'second sa', actual: '2 Samuel' },
    { abbr: 'second sam', actual: '2 Samuel' },
    { abbr: 'second sml', actual: '2 Samuel' },
    { abbr: 'sec samuel', actual: '2 Samuel' },
    { abbr: 'sec sa', actual: '2 Samuel' },
    { abbr: 'sec sam', actual: '2 Samuel' },
    { abbr: 'sec sml', actual: '2 Samuel' },
    { abbr: '1 kings', actual: '1 Kings' },
    { abbr: '1 king', actual: '1 Kings' },
    { abbr: '1 kgs', actual: '1 Kings' },
    { abbr: '1 kn', actual: '1 Kings' },
    { abbr: '1 k', actual: '1 Kings' },
    { abbr: '1 ki', actual: '1 Kings' },
    { abbr: 'i kings', actual: '1 Kings' },
    { abbr: 'i king', actual: '1 Kings' },
    { abbr: 'i kgs', actual: '1 Kings' },
    { abbr: 'i kn', actual: '1 Kings' },
    { abbr: 'i k', actual: '1 Kings' },
    { abbr: 'i ki', actual: '1 Kings' },
    { abbr: '1st kings', actual: '1 Kings' },
    { abbr: '1st king', actual: '1 Kings' },
    { abbr: '1st kgs', actual: '1 Kings' },
    { abbr: '1st kn', actual: '1 Kings' },
    { abbr: '1st k', actual: '1 Kings' },
    { abbr: '1st ki', actual: '1 Kings' },
    { abbr: 'first kings', actual: '1 Kings' },
    { abbr: 'first king', actual: '1 Kings' },
    { abbr: 'first kgs', actual: '1 Kings' },
    { abbr: 'first kn', actual: '1 Kings' },
    { abbr: 'first k', actual: '1 Kings' },
    { abbr: 'first ki', actual: '1 Kings' },
    { abbr: '2 Kings', actual: '2 Kings' },
    { abbr: '2 king', actual: '2 Kings' },
    { abbr: '2 kgs', actual: '2 Kings' },
    { abbr: '2 kn', actual: '2 Kings' },
    { abbr: '2 k', actual: '2 Kings' },
    { abbr: '2 ki', actual: '2 Kings' },
    { abbr: 'ii kings', actual: '2 Kings' },
    { abbr: 'ii king', actual: '2 Kings' },
    { abbr: 'ii kgs', actual: '2 Kings' },
    { abbr: 'ii kn', actual: '2 Kings' },
    { abbr: 'ii k', actual: '2 Kings' },
    { abbr: 'ii ki', actual: '2 Kings' },
    { abbr: '2nd kings', actual: '2 Kings' },
    { abbr: '2nd king', actual: '2 Kings' },
    { abbr: '2nd kgs', actual: '2 Kings' },
    { abbr: '2nd kn', actual: '2 Kings' },
    { abbr: '2nd k', actual: '2 Kings' },
    { abbr: '2nd ki', actual: '2 Kings' },
    { abbr: 'second kings', actual: '2 Kings' },
    { abbr: 'second king', actual: '2 Kings' },
    { abbr: 'second kgs', actual: '2 Kings' },
    { abbr: 'second kn', actual: '2 Kings' },
    { abbr: 'second k', actual: '2 Kings' },
    { abbr: 'second ki', actual: '2 Kings' },
    { abbr: 'sec kings', actual: '2 Kings' },
    { abbr: 'sec king', actual: '2 Kings' },
    { abbr: 'sec kgs', actual: '2 Kings' },
    { abbr: 'sec kn', actual: '2 Kings' },
    { abbr: 'sec k', actual: '2 Kings' },
    { abbr: 'sec ki', actual: '2 Kings' },
    { abbr: '2 Chronicles', actual: '2 Chronicles' },
    { abbr: '2 chronicles', actual: '2 Chronicles' },
    { abbr: '2 chron', actual: '2 Chronicles' },
    { abbr: '2 ch', actual: '2 Chronicles' },
    { abbr: '2 chr', actual: '2 Chronicles' },
    { abbr: 'ii Chronicles', actual: '2 Chronicles' },
    { abbr: 'ii chronicles', actual: '2 Chronicles' },
    { abbr: 'ii chron', actual: '2 Chronicles' },
    { abbr: 'ii ch', actual: '2 Chronicles' },
    { abbr: 'ii chr', actual: '2 Chronicles' },
    { abbr: '2nd Chronicles', actual: '2 Chronicles' },
    { abbr: '2nd chronicles', actual: '2 Chronicles' },
    { abbr: '2nd chron', actual: '2 Chronicles' },
    { abbr: '2nd ch', actual: '2 Chronicles' },
    { abbr: '2nd chr', actual: '2 Chronicles' },
    { abbr: 'second Chronicles', actual: '2 Chronicles' },
    { abbr: 'second chronicles', actual: '2 Chronicles' },
    { abbr: 'second chron', actual: '2 Chronicles' },
    { abbr: 'second ch', actual: '2 Chronicles' },
    { abbr: 'second chr', actual: '2 Chronicles' },
    { abbr: 'sec Chronicles', actual: '2 Chronicles' },
    { abbr: 'sec chronicles', actual: '2 Chronicles' },
    { abbr: 'sec chron', actual: '2 Chronicles' },
    { abbr: 'sec ch', actual: '2 Chronicles' },
    { abbr: 'sec chr', actual: '2 Chronicles' },
    { abbr: '1 Chronicles', actual: '1 Chronicles' },
    { abbr: '1 chronicles', actual: '1 Chronicles' },
    { abbr: '1 chron', actual: '1 Chronicles' },
    { abbr: '1 ch', actual: '1 Chronicles' },
    { abbr: '1 chr', actual: '1 Chronicles' },
    { abbr: 'i Chronicles', actual: '1 Chronicles' },
    { abbr: 'i chronicles', actual: '1 Chronicles' },
    { abbr: 'i chron', actual: '1 Chronicles' },
    { abbr: 'i ch', actual: '1 Chronicles' },
    { abbr: 'i chr', actual: '1 Chronicles' },
    { abbr: '1st Chronicles', actual: '1 Chronicles' },
    { abbr: '1st chronicles', actual: '1 Chronicles' },
    { abbr: '1st chron', actual: '1 Chronicles' },
    { abbr: '1st ch', actual: '1 Chronicles' },
    { abbr: '1st chr', actual: '1 Chronicles' },
    { abbr: 'first Chronicles', actual: '1 Chronicles' },
    { abbr: 'first chronicles', actual: '1 Chronicles' },
    { abbr: 'first chron', actual: '1 Chronicles' },
    { abbr: 'first ch', actual: '1 Chronicles' },
    { abbr: 'first chr', actual: '1 Chronicles' },
    { abbr: 'ezra', actual: 'Ezra' },
    { abbr: 'ezr', actual: 'Ezra' },
    { abbr: 'nehemiah', actual: 'Nehemiah' },
    { abbr: 'neh', actual: 'Nehemiah' },
    { abbr: 'ne', actual: 'Nehemiah' },
    { abbr: 'nehamiah', actual: 'Nehemiah' },
    { abbr: 'esther', actual: 'Esther' },
    { abbr: 'est', actual: 'Esther' },
    { abbr: 'es', actual: 'Esther' },
    { abbr: 'esth', actual: 'Esther' },
    { abbr: 'job', actual: 'Job' },
    { abbr: 'jo', actual: 'Job' },
    { abbr: 'jb', actual: 'Job' },
    { abbr: 'psalms', actual: 'Psalm' },
    { abbr: 'ps', actual: 'Psalm' },
    { abbr: 'psa', actual: 'Psalm' },
    { abbr: 'psalm', actual: 'Psalm' },
    { abbr: 'psm', actual: 'Psalm' },
    { abbr: 'proverbs', actual: 'Proverbs' },
    { abbr: 'prov', actual: 'Proverbs' },
    { abbr: 'pr', actual: 'Proverbs' },
    { abbr: 'pro', actual: 'Proverbs' },
    { abbr: 'proverb', actual: 'Proverbs' },
    { abbr: 'prv', actual: 'Proverbs' },
    { abbr: 'prvbs', actual: 'Proverbs' },
    { abbr: 'ecclesiastes', actual: 'Ecclesiastes' },
    { abbr: 'eccl', actual: 'Ecclesiastes' },
    { abbr: 'ecc', actual: 'Ecclesiastes' },
    { abbr: 'eccles', actual: 'Ecclesiastes' },
    { abbr: 'ec', actual: 'Ecclesiastes' },
    { abbr: 'ecl', actual: 'Ecclesiastes' },
    { abbr: 'ecclesiaste', actual: 'Ecclesiastes' },
    { abbr: 'song of solomon', actual: 'Song of Solomon' },
    { abbr: 'song of songs', actual: 'Song of Solomon' },
    { abbr: 'sos', actual: 'Song of Solomon' },
    { abbr: 'ss', actual: 'Song of Solomon' },
    { abbr: 'son', actual: 'Song of Solomon' },
    { abbr: 'so', actual: 'Song of Solomon' },
    { abbr: 'song', actual: 'Song of Solomon' },
    { abbr: 'songs', actual: 'Song of Solomon' },
    { abbr: 'isaiah', actual: 'Isaiah' },
    { abbr: 'is', actual: 'Isaiah' },
    { abbr: 'isah', actual: 'Isaiah' },
    { abbr: 'isai', actual: 'Isaiah' },
    { abbr: 'ia', actual: 'Isaiah' },
    { abbr: 'jerimiah', actual: 'Jeremiah' },
    { abbr: 'jeremiah', actual: 'Jeremiah' },
    { abbr: 'jer', actual: 'Jeremiah' },
    { abbr: 'je', actual: 'Jeremiah' },
    { abbr: 'jere', actual: 'Jeremiah' },
    { abbr: 'lamentations', actual: 'Lamentations' },
    { abbr: 'lam', actual: 'Lamentations' },
    { abbr: 'la', actual: 'Lamentations' },
    { abbr: 'lamentation', actual: 'Lamentations' },
  ];

  for (const bk of booknames) {
    it('Should parse the references: ' + bk.abbr, () => {
      const book = BibleReference.parseBook(bk.abbr);
      expect(book.name).toBe(bk.actual);

      for (let i = 1; i < book.lastChapter; i++) {
        expect(new BibleReference(bk.abbr + ' ' + i + ':3-' + (i + 1) + ':6').toString()).toBe(
          bk.actual + ' ' + i + ':3 - ' + (i + 1) + ':6'
        );
      }

      expect(new BibleReference(bk.abbr + ' 1:4-2:5').toString()).toBe(bk.actual + ' 1:4 - 2:5');
      expect(new BibleReference(bk.abbr + ' 1:1 - 5').toString()).toBe(bk.actual + ' 1:1 - 5');
      expect(new BibleReference(bk.abbr + ' 1:4 - 5').toString()).toBe(bk.actual + ' 1:4 - 5');
    });
  }

  const refs = [{ src: '2 sam 3:4-6:8', actual: '2 Samuel 3:4 - 6:8' }];

  for (const ref of refs) {
    it('Should parse the reference: ' + ref.src, () => {
      expect(new BibleReference(ref.src).toString()).toBe(ref.actual);
    });
  }
});

describe('Reference Overlap Detection', () => {
  it('Different books dont overlap', () => {
    const leftRef = new BibleReference('Gen 1:1-5');
    const rightRef = new BibleReference('Exodus 1:3-7');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.None);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toBeNull();
  });

  it('Different chapters dont overlap', () => {
    const leftRef = new BibleReference('Gen 1:1-5');
    const rightRef = new BibleReference('Gen 2:1-5');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.None);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects Left intersects the front of Right', () => {
    const leftRef = new BibleReference('Gen 1:1-5');
    const rightRef = new BibleReference('Gen 1:4-6');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.Intersect);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toEqual(
      new BibleReference('Gen 1:1-6')
    );
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects Right intersects the front of left', () => {
    const leftRef = new BibleReference('Gen 1:7-10');
    const rightRef = new BibleReference('Gen 1:1-8');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.Intersect);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toEqual(
      new BibleReference('Gen 1:1-10')
    );
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects Left embedded in Right', () => {
    const leftRef = new BibleReference('Gen 1:2-8');
    const rightRef = new BibleReference('Gen 1:1-10');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.Subset);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toEqual(
      new BibleReference('Gen 1:1-10')
    );
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toEqual(new BibleReference('Gen 1:1-10'));
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects Both are Equal', () => {
    const leftRef = new BibleReference('Gen 1:1-10');
    const rightRef = new BibleReference('Gen 1:1-10');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.Equal);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Equal)).toEqual(new BibleReference('Gen 1:1-10'));
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toEqual(new BibleReference('Gen 1:1-10'));
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toEqual(
      new BibleReference('Gen 1:1-10')
    );
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects Right embedded in Left', () => {
    const leftRef = new BibleReference('Gen 1:1-10');
    const rightRef = new BibleReference('Gen 1:2-8');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.Subset);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Equal)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toEqual(new BibleReference('Gen 1:1-10'));
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toEqual(
      new BibleReference('Gen 1:1-10')
    );
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects Left chapter Overlaps right chapter', () => {
    const leftRef = new BibleReference('Gen 1:1-2:5');
    const rightRef = new BibleReference('Gen 2:1-10');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.Intersect);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Equal)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toEqual(
      new BibleReference('Gen 1:1-2:10')
    );
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects Right chapter Overlaps Left chapter', () => {
    const leftRef = new BibleReference('Gen 2:1-10');
    const rightRef = new BibleReference('Gen 1:1-2:5');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.Intersect);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Equal)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toEqual(
      new BibleReference('Gen 1:1-2:10')
    );
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects discontinuous chapters', () => {
    const leftRef = new BibleReference('Gen 2:6-10');
    const rightRef = new BibleReference('Gen 1:1-2:5');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.None);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Equal)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects left side embedded chapters', () => {
    const leftRef = new BibleReference('Gen 1:1-2:5');
    const rightRef = new BibleReference('Gen 1:1-3:1');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.Subset);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Equal)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toEqual(new BibleReference('Gen 1:1-3:1'));
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect).toString()).toEqual(
      new BibleReference('Gen 1:1-3:1').toString()
    );
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });

  it('Detects right side embedded chapters', () => {
    const leftRef = new BibleReference('Gen 1:1-3:1');
    const rightRef = new BibleReference('Gen 1:1-2:5');

    expect(BibleReference.overlap(leftRef, rightRef)).toBe(Overlap.Subset);
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Equal)).toBeNull();
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Subset)).toEqual(new BibleReference('Gen 1:1-3:1'));
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.Intersect).toString()).toEqual(
      new BibleReference('Gen 1:1-3:1').toString()
    );
    expect(BibleReference.mergeReference(leftRef, rightRef, Overlap.None)).toBeNull();
  });
});

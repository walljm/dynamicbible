import { CardType, DataReference } from '../models/card-state';
import { BibleReference, Overlap } from './bible-reference';

export function maybeMergeCards(
  leftCard: DataReference,
  rightCard: DataReference,
  strategy: Overlap
): DataReference | null {
  if (leftCard.type === rightCard.type) {
    switch (strategy) {
      case Overlap.Equal:
        if (leftCard.qry === rightCard.qry) {
          return { ...leftCard };
        }
        break;
      case Overlap.Intersect:
      case Overlap.Subset:
        if (leftCard.type === CardType.Passage) {
          const leftRef = new BibleReference(leftCard.qry);
          const rightRef = new BibleReference(rightCard.qry);
          const mergedRef = BibleReference.mergeReference(leftRef, rightRef, strategy);
          if (mergedRef !== null) {
            return {
              ...leftCard,
              qry: mergedRef.toString(),
            };
          }
        }
        break;
    }
  }
  return null;
}

export function mergeCardList(cardList: readonly DataReference[], strategy: Overlap) {
  if (strategy === Overlap.None || cardList.length === 0) {
    return [...cardList];
  }
  const cardListCopy = [...cardList];
  const removals = [];
  for (const cardIdx in cardListCopy) {
    if (cardIdx in removals) {
      continue;
    }
    for (let compareIdx = Number(cardIdx) + 1; compareIdx < cardListCopy.length; compareIdx++) {
      const leftCard = cardListCopy[cardIdx];
      const rightCard = cardListCopy[compareIdx];
      const mergedCard = maybeMergeCards(leftCard, rightCard, strategy);
      if (mergedCard != null) {
        cardListCopy[cardIdx] = mergedCard;
        removals.push(compareIdx);
      }
    }
  }
  for (const idx of removals) {
    cardListCopy.splice(idx, 1);
  }
  return cardListCopy;
}

﻿using System.Text.Json.Serialization;

namespace DynamicBible.DataPreparation.Models;

public class Index : List<IndexItem>
{
    public IndexItem? GetItem(string word)
    {
        return Find(i => i.Word == word);
    }
}

public class IndexItem
{
    [JsonPropertyName("w")]
    public string Word { get; set; } = "";

    [JsonPropertyName("r")]
    public List<string> References { get; set; } = [];
}
export interface WordLookupResult {
  readonly refs: readonly string[];
  readonly word: string;
}

export interface IndexResult {
  readonly r: readonly string[];
  readonly w: string;
}

export interface WordToStem {
  readonly w: string;
  readonly s: string;
}

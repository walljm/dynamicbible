import { Clipboard } from '@angular/cdk/clipboard';
import { ChangeDetectionStrategy,Component, ElementRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BibleReference } from 'src/app/common/bible-reference';
import { CardComponent } from 'src/app/components/card.component';
import { WordLookupResult } from 'src/app/models/words-state';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-words-card',
  templateUrl: 'words-card.component.html',
  styleUrls: ['./words-card.component.scss'],
  preserveWhitespaces: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WordsCardComponent extends CardComponent {
  @ViewChild('words') wordsElement: ElementRef;

  get data(): WordLookupResult {
    return this.cardItem.data as WordLookupResult;
  }

  constructor(
    protected elementRef: ElementRef,
    protected appService: AppService,
    protected clipboard: Clipboard,
    public dialog: MatDialog
  ) {
    super(elementRef, dialog, appService, clipboard);
    this.icon$ = appService.select((state) => state.settings.value.cardIcons.words);
  }

  copy() {
    const refs = (this.cardItem.data as WordLookupResult).refs.map((ref) =>
      BibleReference.makePassageFromReferenceKey(ref)
    );

    const text = refs.join(', ');
    this.copyToClip(text);
  }

  makePassage(p: string) {
    return BibleReference.makePassageFromReferenceKey(p);
  }

  openPassage(p: string) {
    const ref = BibleReference.makePassageFromReferenceKey(p);
    this.appService.getPassage(ref, this.cardItem);
  }
}

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SubscriberBase } from 'src/app/common/subscriber-base';
import { NoteEditModalComponent } from 'src/app/components/note/edit-modal/note-edit-modal.component';
import { CardItem, CardType } from 'src/app/models/card-state';
import { AppService } from 'src/app/services/app.service';
import { NavService } from 'src/app/services/nav.service';

@Component({
  selector: 'app-notes-admin',
  templateUrl: './notes-admin.page.html',
  styleUrls: ['./notes-admin.page.scss'],
})
export class NotesAdminPageComponent extends SubscriberBase implements OnInit {
  notes$ = this.appService.select((state) =>
    state.notes === null
      ? null
      : state.notes.value.map(
          (o) =>
            ({
              qry: o.id,
              data: o,
              type: CardType.Note,
            } as CardItem)
        )
  );

  constructor(public navService: NavService, private appService: AppService, private dialog: MatDialog) {
    super();
  }

  addNote() {
    this.dialog.open(NoteEditModalComponent, {
      autoFocus: 'note-content',
    });
  }
  ngOnInit() {
    this.navService.closeNav(); // close the nav immediately.
  }
}

import { Overlap } from '../common/bible-reference';
import { HashTable } from '../common/hashtable';
import { IStorable } from '../common/storable';
import { CardIcons, CardItem, DataReference } from './card-state';
import { NoteItem } from './note-state';
import { SavedPage } from './page-state';

export interface AppState {
  readonly currentSavedPage: SavedPage;
  readonly currentCards: IStorable<readonly DataReference[]>;

  readonly cardCache: HashTable<CardItem>;

  readonly savedPages: IStorable<readonly SavedPage[]>;
  readonly notes: IStorable<readonly NoteItem[]>;

  readonly settings: IStorable<Settings>;

  readonly savedPagesLoaded: boolean;
  readonly autocomplete: readonly string[];
  readonly error: Error;
  readonly user: User;
}

export interface Error {
  readonly msg: string;
}

export interface User {
  readonly uid: string;
  readonly displayName: string | null;
  readonly email: string | null;
  readonly providerId: string;
}

export interface Settings {
  readonly displaySettings: DisplaySettings;
  readonly pageSettings: PageSettings;
  readonly cardIcons: CardIcons;
}

export interface PageSettings {
  readonly mergeStrategy: Overlap;
}

export interface DisplaySettings {
  readonly showStrongsAsModal: boolean;

  readonly appendCardToBottom: boolean;
  readonly insertCardNextToItem: boolean;

  readonly clearSearchAfterQuery: boolean;

  readonly cardFontSize: number;
  readonly cardFontFamily: string;

  readonly showVersesOnNewLine: boolean;
  readonly showVerseNumbers: boolean;
  readonly showParagraphs: boolean;
  readonly showParagraphHeadings: boolean;

  readonly syncCardsAcrossDevices: boolean;
}

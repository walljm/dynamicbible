import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { lastValueFrom } from 'rxjs';

import { moveItem, moveItemUpOrDown } from '../common/array-operations';
import { BibleReference, Overlap, Section } from '../common/bible-reference';
import { getFromCardCache, removeFromCardCache, updateInCardCache } from '../common/card-cache-operations';
import { mergeCardList } from '../common/card-operations';
import { HashTable } from '../common/hashtable';
import { MoveDirection } from '../common/move-direction';
import { createReducingService, IReducingAction } from '../common/state-service';
import { IStorable, Storable, StorableType } from '../common/storable';
import { AppState, DisplaySettings, Error, PageSettings, Settings, User } from '../models/app-state';
import { CardItem, CardType, DataReference } from '../models/card-state';
import { NoteItem } from '../models/note-state';
import { SavedPage } from '../models/page-state';
import {
  BibleParagraph,
  BibleParagraphPassage,
  BiblePassage,
  BiblePassageResult,
  BibleVerse,
  Paragraph,
} from '../models/passage-state';
import {
  RMACCrossReference,
  RMACDefinition,
  StrongsCrossReference,
  StrongsDefinition,
  StrongsDictionary,
  StrongsResult,
} from '../models/strongs-state';
import { IndexResult, WordLookupResult, WordToStem } from '../models/words-state';

const initialState: AppState = {
  user: null,
  currentCards: {
    type: StorableType.initial,
    createdOn: new Date(0).toISOString(),
    value: [],
  },
  cardCache: {},
  autocomplete: [],
  currentSavedPage: null,
  savedPages: null,
  notes: {
    type: StorableType.initial,
    createdOn: new Date(0).toISOString(),
    value: [],
  },
  savedPagesLoaded: false,
  error: null,
  settings: {
    type: StorableType.initial,
    createdOn: new Date(0).toISOString(),
    value: {
      displaySettings: {
        showStrongsAsModal: false,
        appendCardToBottom: true,
        insertCardNextToItem: true,
        clearSearchAfterQuery: true,
        cardFontSize: 12,
        cardFontFamily: 'PT Serif',
        showVersesOnNewLine: false,
        showVerseNumbers: false,
        showParagraphs: true,
        showParagraphHeadings: true,
        syncCardsAcrossDevices: false,
      },
      pageSettings: {
        mergeStrategy: Overlap.Equal,
      },
      cardIcons: {
        words: 'font_download',
        passage: 'menu_book',
        strongs: 'speaker_notes',
        note: 'note',
        savedPage: 'inbox',
      },
    },
  },
};

interface AppAction extends IReducingAction<AppState> {}

export function getNewestStorable<T>(candidate: IStorable<T>, incumbant: IStorable<T>): IStorable<T> {
  // if the candidate is null, then return the state.
  if (!candidate) {
    return incumbant;
  }

  // only update if the settings are newer.
  if (!incumbant || new Date(candidate.createdOn) > new Date(incumbant.createdOn)) {
    return candidate;
  }

  // candidate didn't win. return state untouched.
  return incumbant;
}

//#region Saved Pages

export const savePageAction = (title: string): AppAction => {
  return {
    handle(state: AppState) {
      const savedPages = new Storable([
        ...(state.savedPages ? state.savedPages.value : []),
        {
          // create a new saved page object
          title: title,
          id: UUID.UUID().toString(),
          queries: [...mergeCardList(state.currentCards.value, state.settings.value.pageSettings.mergeStrategy)],
        },
      ]);

      return updateSavedPagesAction(savedPages).handle(state);
    },
  };
};

export const updateCurrentPageAction = (): AppAction => {
  return {
    handle(state: AppState) {
      const current = {
        id: state.currentSavedPage.id,
        title: state.currentSavedPage.title,
        queries: [...mergeCardList(state.currentCards.value, state.settings.value.pageSettings.mergeStrategy)],
      };

      const savedPages = new Storable<SavedPage[]>([
        ...state.savedPages.value.filter((o) => o.id !== state.currentSavedPage.id),
        current,
      ]);

      const item = getNewestStorable(savedPages, state.savedPages);
      return {
        ...state,
        currentSavedPage: current,
        savedPagesLoaded: true,
        savedPages: item,
      };
    },
  };
};

export const updateSavedPagesAction = (oldSavedPages: IStorable<readonly SavedPage[]>): AppAction => {
  return {
    handle(state: AppState) {
      const newSavedPages = getNewestStorable(oldSavedPages, state.savedPages);

      // only true if a currentSavedPage was set, indicating that the user
      //  is currently looking at a saved page.
      const hasCurrentSavedPage =
        state.currentSavedPage !== null &&
        state.currentSavedPage !== undefined &&
        oldSavedPages.value.some((o) => o.id === state.currentSavedPage.id);

      const currentSavedPage = hasCurrentSavedPage
        ? oldSavedPages.value.find((o) => o.id === state.currentSavedPage.id)
        : null;

      return {
        ...state,
        // if the currentSavedPage was loaded, replace it with the info from the
        //  new savedPages array, as it might have changed.
        currentCards: hasCurrentSavedPage ? new Storable(currentSavedPage.queries) : state.currentCards,
        currentSavedPage: hasCurrentSavedPage ? currentSavedPage : state.currentSavedPage,
        savedPagesLoaded: true,
        savedPages: newSavedPages, // update the savedPages
      };
    },
  };
};

export const updateSavedPageAction = (savedPage: SavedPage): AppAction => {
  return {
    handle(state: AppState) {
      const newSavedPages = new Storable<SavedPage[]>(
        state.savedPages.value.map((o) => {
          if (o.id === savedPage.id) {
            return savedPage;
          }
          return o;
        })
      );

      const savedPages = getNewestStorable(newSavedPages, state.savedPages);
      return updateSavedPagesAction(savedPages).handle(state);
    },
  };
};

export const removeSavedPageAction = (savedPage: SavedPage): AppAction => {
  return {
    handle(state: AppState) {
      const savedPages = new Storable<SavedPage[]>(state.savedPages.value.filter((o) => o.id !== savedPage.id));
      const item = getNewestStorable(savedPages, state.savedPages);

      return {
        ...state,
        savedPagesLoaded: true,
        savedPages: item,
      };
    },
  };
};

export const moveSavedPageCardAction = (oldSavedPage: SavedPage, fromIndex: number, toIndex: number): AppAction => {
  return {
    handle(state: AppState) {
      const queries = moveItem(oldSavedPage.queries, fromIndex, toIndex);
      const savedPage = {
        ...oldSavedPage,
        queries, // update the queries.
      };

      return updateSavedPageAction(savedPage).handle(state);
    },
  };
};

export const addCardToSavedPageAction = (card: CardItem, pageId: string): AppAction => {
  return {
    handle(state: AppState) {
      const savedPages = new Storable([
        ...(state.savedPages ? state.savedPages.value : []).map((o) => {
          if (o.id.toString() === pageId) {
            let references = [] as DataReference[];
            if (state.settings.value.displaySettings.appendCardToBottom) {
              references = [...o.queries, card];
            } else {
              references = [card, ...o.queries];
            }
            return {
              ...o,
              queries: mergeCardList(references, state.settings.value.pageSettings.mergeStrategy),
            };
          }
          return o;
        }),
      ]);

      return updateSavedPagesAction(savedPages).handle(state);
    },
  };
};

//#endregion

//#region Cards

export const addCardsAction = (cardsToAdd: CardItem[], nextToItem: CardItem): AppAction => {
  return {
    handle(state: AppState) {
      let cards = [...state.currentCards.value];
      let cache = state.cardCache;

      for (let card of cardsToAdd) {
        if (nextToItem && state.settings.value.displaySettings.insertCardNextToItem) {
          const idx = cards.indexOf(nextToItem);

          if (state.settings.value.displaySettings.appendCardToBottom) {
            const before = cards.slice(0, idx + 1);
            const after = cards.slice(idx + 1);
            cards = [...before, card, ...after];
          } else {
            const before = cards.slice(0, idx);
            const after = cards.slice(idx);
            cards = [...before, card, ...after];
          }
        } else {
          if (state.settings.value.displaySettings.appendCardToBottom) {
            cards = [...cards, card];
          } else {
            cards = [card, ...cards];
          }
        }
        cache = updateInCardCache(card, state.cardCache);
      }
      return {
        ...state,
        currentCards: new Storable(cards),
        cardCache: cache,
      };
    },
  };
};

export const updateCardAction = (newCard: CardItem, oldCard: CardItem): AppAction => {
  return {
    handle(state: AppState) {
      return {
        ...state,
        currentCards: new Storable(
          state.currentCards.value.map((c) => {
            if (c.type === oldCard.type && c.qry === oldCard.qry) {
              return newCard;
            }
            return c;
          })
        ),
        cardCache: updateInCardCache(newCard, removeFromCardCache(oldCard, state.cardCache)),
      };
    },
  };
};

export const removeCardAction = (card: CardItem): AppAction => {
  return {
    handle(state: AppState) {
      // potentially remove card from a saved page.
      const currentSavedPage =
        state.currentSavedPage !== null
          ? {
              ...state.currentSavedPage,
              queries: state.currentSavedPage.queries.filter((q) => q !== card),
            }
          : null;

      const savedPages =
        state.currentSavedPage !== null
          ? new Storable(
              (state.savedPages ? state.savedPages.value : []).map((o) => {
                if (o === state.currentSavedPage) {
                  return currentSavedPage;
                }
                return o;
              })
            )
          : state.savedPages;

      return {
        ...state,
        currentSavedPage,
        savedPages,
        currentCards: new Storable([...state.currentCards.value.filter((c) => c !== card)]),
        cardCache: removeFromCardCache(card, state.cardCache),
      };
    },
  };
};

export const moveCardAction = (card: CardItem, direction: MoveDirection): AppAction => {
  return {
    handle(state: AppState) {
      const cards = moveItemUpOrDown(state.currentCards.value, card, direction);

      return {
        ...state,
        currentCards: new Storable(cards),
      };
    },
  };
};

export const updateCardsAction = (cards: IStorable<CardItem[]>): AppAction => {
  return {
    handle(state: AppState) {
      let cardCache = { ...state.cardCache };
      for (const card of cards.value) {
        cardCache = updateInCardCache(card, cardCache);
      }
      return {
        ...state,
        currentCards: cards,
        cardCache,
      };
    },
  };
};

export const clearCardsAction = (): AppAction => {
  return {
    handle(state: AppState) {
      return {
        ...state,
        currentCards: new Storable([]),
      };
    },
  };
};

//#endregion

//#region General

export const updateErrorAction = (error: Error): AppAction => {
  return {
    handle(state: AppState) {
      return {
        ...state,
        error: error,
      };
    },
  };
};

export const setUserAction = (user: User): AppAction => {
  return {
    handle(state: AppState) {
      return {
        ...state,
        user: user,
      };
    },
  };
};

export const updateAutoCompleteAction = (words: string[]): AppAction => {
  return {
    handle(state: AppState) {
      return {
        ...state,
        autocomplete: [...words],
      };
    },
  };
};

//#endregion

//#region Settings

export const updateSettingsAction = (settings: IStorable<Settings>): AppAction => {
  return {
    handle(state: AppState) {
      const item = getNewestStorable(settings, state.settings);

      return {
        ...state,
        settings: item,
      };
    },
  };
};

export const updateCardMergeStrategyAction = (mergeStrategy: Overlap): AppAction => {
  return {
    handle(state: AppState) {
      const settings = new Storable<Settings>({
        ...state.settings.value,
        pageSettings: {
          ...state.settings.value.pageSettings,
          mergeStrategy: mergeStrategy,
        },
      });
      return updateSettingsAction(settings).handle(state);
    },
  };
};

export const updateCardFontSizeAction = (cardFontSize: number): AppAction => {
  return {
    handle(state: AppState) {
      const settings = new Storable<Settings>({
        ...state.settings.value,
        displaySettings: {
          ...state.settings.value.displaySettings,
          cardFontSize: cardFontSize,
        },
      });

      return updateSettingsAction(settings).handle(state);
    },
  };
};

export const updateCardFontFamilyAction = (cardFontFamily: string): AppAction => {
  return {
    handle(state: AppState) {
      const settings = new Storable<Settings>({
        ...state.settings.value,
        displaySettings: {
          ...state.settings.value.displaySettings,
          cardFontFamily: cardFontFamily,
        },
      });

      return updateSettingsAction(settings).handle(state);
    },
  };
};

//#endregion

//#region Notes

export const findNotesAction = (qry: string, nextToItem: CardItem): AppAction => {
  return {
    handle(state: AppState) {
      const notes = state.notes.value
        .filter((o) => o.title.search(qry) > -1)
        .map((o) => {
          return {
            qry: o.id,
            type: CardType.Note,
            data: o,
          } as CardItem;
        });

      let cards = [] as DataReference[];

      if (nextToItem && state.settings.value.displaySettings.insertCardNextToItem) {
        const idx = state.currentCards.value.indexOf(nextToItem);

        if (state.settings.value.displaySettings.appendCardToBottom) {
          const before = state.currentCards.value.slice(0, idx + 1);
          const after = state.currentCards.value.slice(idx + 1);
          cards = [...before, ...notes, ...after];
        } else {
          const before = state.currentCards.value.slice(0, idx);
          const after = state.currentCards.value.slice(idx);
          cards = [...before, ...notes, ...after];
        }
      } else {
        if (state.settings.value.displaySettings.appendCardToBottom) {
          cards = [...state.currentCards.value, ...notes];
        } else {
          cards = [...notes, ...state.currentCards.value];
        }
      }

      let cache = { ...state.cardCache };
      for (const n of notes) {
        cache = updateInCardCache(n, cache);
      }

      return {
        ...state,
        currentCards: new Storable(cards),
        cardCache: cache,
      };
    },
  };
};

export const getNotesAction = (noteId: string, nextToItem: CardItem): AppAction => {
  return {
    handle(state: AppState) {
      const note = state.notes.value.find((o) => o.id === noteId);
      const card: CardItem = {
        qry: note.id,
        type: CardType.Note,
        data: note,
      };

      return addCardsAction([card], nextToItem).handle(state);
    },
  };
};

export const updateNotesAction = (notes: IStorable<readonly NoteItem[]>): AppAction => {
  return {
    handle(state: AppState) {
      return {
        ...state,
        notes: notes ? notes : new Storable([]),
      };
    },
  };
};

export const saveNoteAction = (note: NoteItem): AppAction => {
  return {
    handle(state: AppState) {
      // you may be creating a new note or updating an existing.
      //  if its an update, you need to update the note in the following:
      //  * card list could have it.
      //  * notes list could have it.
      //  * it could be in any of the saved pages lists...
      //  so iterate through all of them and if you find the note
      //   in any of them, swap it out

      const notes = new Storable<NoteItem[]>([...state.notes.value.filter((o) => o.id !== note.id), note]);

      return {
        ...state,
        // you want to trigger an update to the cards if a card update is different.
        currentCards: new Storable([...state.currentCards.value]),
        notes,
      };
    },
  };
};

export const deleteNoteAction = (note: NoteItem): AppAction => {
  return {
    handle(state: AppState) {
      //  the note may be in any of the following:
      //  * card list could have it.
      //  * notes list could have it.
      //  * it could be in any of the saved pages lists...
      //  so iterate through all of them and if you find the note
      //   in any of them, remove it

      const card = state.currentCards.value.find((o) => o.qry === note.id);

      const cards = card
        ? [
            ...state.currentCards.value.filter((o) => {
              return o.type !== CardType.Note || o.qry !== note.id;
            }),
          ]
        : state.currentCards.value; // if card is undefined, then it wasn't in the current card list.

      const notes = new Storable<NoteItem[]>([...state.notes.value.filter((o) => o.id !== note.id)]);

      const savedPages = new Storable<SavedPage[]>([
        ...(state.savedPages ? state.savedPages.value : []).map((sp) => {
          return {
            ...sp,
            queries: sp.queries.filter((o) => {
              return o.type !== CardType.Note || o.qry !== note.id;
            }),
          };
        }),
      ]);

      return {
        ...state,
        currentCards: new Storable(cards),
        notes,
        savedPages,
        cardCache: card
          ? removeFromCardCache(getFromCardCache(card, state.cardCache), state.cardCache)
          : state.cardCache,
      };
    },
  };
};

//#endregion

@Injectable({
  providedIn: 'root',
})
export class AppService extends createReducingService(initialState) {
  private wordToStem: Map<string, string>;
  private paragraphs: HashTable<Paragraph>;
  private searchIndexArray: string[];
  private autocomplete: string[];
  private excludedWords: Map<string, number> = new Map([
    ['us', 0],
    ['these', 0],
    ['her', 0],
    ['saith', 0],
    ['shalt', 0],
    ['let', 0],
    ['do', 0],
    ['your', 0],
    ['we', 0],
    ['no', 0],
    ['go', 0],
    ['if', 0],
    ['at', 0],
    ['an', 0],
    ['so', 0],
    ['before', 0],
    ['also', 0],
    ['on', 0],
    ['had', 0],
    ['you', 0],
    ['there', 0],
    ['then', 0],
    ['up', 0],
    ['by', 0],
    ['upon', 0],
    ['were', 0],
    ['are', 0],
    ['this', 0],
    ['when', 0],
    ['thee', 0],
    ['their', 0],
    ['ye', 0],
    ['will', 0],
    ['as', 0],
    ['thy', 0],
    ['my', 0],
    ['me', 0],
    ['have', 0],
    ['from', 0],
    ['was', 0],
    ['but', 0],
    ['which', 0],
    ['thou', 0],
    ['all', 0],
    ['it', 0],
    ['with', 0],
    ['them', 0],
    ['him', 0],
    ['they', 0],
    ['is', 0],
    ['be', 0],
    ['not', 0],
    ['his', 0],
    ['i', 0],
    ['shall', 0],
    ['a', 0],
    ['for', 0],
    ['unto', 0],
    ['he', 0],
    ['in', 0],
    ['to', 0],
    ['that', 0],
    ['of', 0],
    ['and', 0],
    ['the', 0],
  ]);
  private readonly dataPath = 'assets/data';

  constructor(private http: HttpClient) {
    super();

    this.searchIndexArray = this.buildIndexArray().sort();
  }

  //#region General

  dispatchError(msg: string) {
    this.dispatch(updateErrorAction({ msg }));
    console.log(msg);
  }

  setUser(user: User) {
    this.dispatch(setUserAction(user));
  }

  async getAutoComplete(keyword: string) {
    if (!this.autocomplete) {
      // if you have't populated the word list yet, do so...
      const data = await lastValueFrom(this.http.get<WordToStem[]>(`${this.dataPath}/index/word_to_stem_idx.json`));
      this.autocomplete = data.map((o) => o.w);
    }

    let qry = keyword;
    let prefix = '';
    const idx = qry.lastIndexOf(';');
    const words: string[] = [];

    if (idx > -1) {
      qry = keyword.substring(idx + 1).trim();
      prefix = keyword.substring(0, idx).trim() + '; ';
    }

    qry = qry.split('"').join(''); // remove any quotes, just in case

    if (qry.search(/\d/i) === -1) {
      // its a word
      for (const item of BibleReference.Books) {
        if (
          item.name !== 'Unknown' &&
          (item.name.toLowerCase().indexOf(qry.toLowerCase()) > -1 ||
            item.abbreviation.toLowerCase().indexOf(qry.toLowerCase()) > -1)
        ) {
          words.push(prefix + item.name);
          if (words.length > 2) {
            break;
          }
        }
      }

      for (const item of this.autocomplete) {
        if (item.toLowerCase().indexOf(qry.toLowerCase()) > -1) {
          words.push(prefix + item);
          if (words.length > 6) {
            break;
          }
        }
      }

      this.dispatch(updateAutoCompleteAction(words));
    } else if (qry.search(/(h|g)\d/i) !== -1) {
      // its a strongs lookup
      if (qry.substring(0, 1).toUpperCase() === 'H') {
        const num = parseInt(qry.substring(1), 10);
        for (let x = num; x < num + 10 && x < 8675; x++) {
          words.push('H' + x);
        }
      } else if (qry.substring(0, 1).toUpperCase() === 'G') {
        const num = parseInt(qry.substring(1), 10);
        for (let x = num; x < num + 10 && x < 5625; x++) {
          words.push('G' + x);
        }
      }

      this.dispatch(updateAutoCompleteAction(words));
    }
  }

  //#endregion

  //#region Cards

  removeCard(card: CardItem) {
    this.dispatch(removeCardAction(card));
  }

  moveCard(card: CardItem, direction: MoveDirection) {
    this.dispatch(moveCardAction(card, direction));
  }

  async addCard(qry: string, nextToItem: CardItem = null) {
    const card = await this.getCardByQuery(qry);
    if (!card) {
      return;
    }
    this.dispatch(addCardsAction([card], nextToItem));
  }

  async updateCards(queries: IStorable<readonly DataReference[]>) {
    const values = queries.value ?? [];
    const cards: CardItem[] = [];
    for (const q of values) {
      const card = await this.getCardByQuery(q.qry.trim(), q.type);
      cards.push(card);
    }
    this.dispatch(
      updateCardsAction({
        type: queries.type,
        createdOn: queries.createdOn,
        value: cards,
      })
    );
  }

  async updateCard(newCard: CardItem, oldCard: CardItem) {
    const card = await this.getCardByQuery(newCard.qry.trim());
    this.dispatch(updateCardAction(card, oldCard));
  }

  async clearCards() {
    this.dispatch(clearCardsAction());
  }

  private async getCardByQuery(qry: string, type?: CardType): Promise<CardItem> {
    if (qry.startsWith('note:')) {
      const q = qry.toLocaleLowerCase().trim().replace('note:', '');
      const data = this.getState().notes.value.find((o) => o.title.toLowerCase().indexOf(q) > -1);
      if (!data) {
        return;
      }
      return {
        qry,
        type: CardType.Note,
        data,
      } as CardItem;
    } else if (type !== undefined && type === CardType.Note) {
      const data = this.getState().notes.value.find((o) => o.id === qry);
      if (!data) {
        return;
      }
      return {
        qry,
        type: CardType.Note,
        data,
      } as CardItem;
    } else if (qry.search(/\d/i) === -1) {
      // // its a search term.
      const data = await this.getWordsFromApi(qry);
      if (!data) {
        return;
      }
      return {
        qry,
        type: CardType.Word,
        data,
      } as CardItem;
    } else if (qry.search(/(h|g)\d/i) !== -1) {
      // its a strongs lookup
      const dict = qry.substring(0, 1).search(/h/i) !== -1 ? 'heb' : 'grk';
      const strongsNumber = qry.substring(1, qry.length);
      return await this.getStrongsCard(strongsNumber, dict);
    } else {
      // its a verse reference.
      if (qry !== '') {
        const myref = new BibleReference(qry.trim());
        const data = await this.getPassageFromApi(myref.section);
        if (!data) {
          return;
        }
        return {
          qry,
          type: CardType.Passage,
          data,
        } as CardItem;
      }
    }
  }

  //#endregion

  //#region Saved Pages

  async getSavedPage(pageid: string) {
    const page = this.getState().savedPages.value.find((o) => o.id === pageid);
    const cards = [];

    for (const ref of page.queries) {
      cards.push(await this.getCardByQuery(ref.qry, ref.type));
    }

    this.dispatch(updateCardsAction(new Storable(cards)));
  }

  savePage(title: string) {
    this.dispatch(savePageAction(title));
  }

  updateCurrentSavedPage() {
    this.dispatch(updateCurrentPageAction());
  }

  updateSavedPages(savedPages: IStorable<readonly SavedPage[]>) {
    this.dispatch(updateSavedPagesAction(savedPages));
  }

  updateSavedPage(savedPage: SavedPage) {
    this.dispatch(updateSavedPageAction(savedPage));
  }

  removeSavedPage(savedPage: SavedPage) {
    this.dispatch(removeSavedPageAction(savedPage));
  }

  moveSavedPageCard(savedPage: SavedPage, fromIndex: number, toIndex: number) {
    this.dispatch(moveSavedPageCardAction(savedPage, fromIndex, toIndex));
  }

  addCardToSavedPage(pageId: string, card: CardItem) {
    this.dispatch(addCardToSavedPageAction(card, pageId));
  }

  //#endregion

  //#region Settings

  updateSettings(settings: IStorable<Settings>) {
    this.dispatch(updateSettingsAction(settings));
  }

  updateCardMergeStrategy(strategy: Overlap) {
    this.dispatch(updateCardMergeStrategyAction(strategy));
  }

  changeCardFontSize(size: number) {
    this.dispatch(updateCardFontSizeAction(size));
  }

  changeCardFontFamily(cardFont: string) {
    this.dispatch(updateCardFontFamilyAction(cardFont));
  }

  updateDisplaySettings(displaySettings: DisplaySettings) {
    const state = this.getState();

    this.dispatch(
      updateSettingsAction(
        new Storable<Settings>({
          ...state.settings.value,
          displaySettings,
        })
      )
    );
  }

  updatePageSettings(pageSettings: PageSettings) {
    const state = this.getState();

    this.dispatch(
      updateSettingsAction(
        new Storable<Settings>({
          ...state.settings.value,
          pageSettings,
        })
      )
    );
  }

  //#endregion

  //#region Notes

  findNotes(qry: string, nextToItem: CardItem = null) {
    this.dispatch(findNotesAction(qry, nextToItem));
  }

  async getNote(noteId: string, nextToItem: CardItem = null) {
    this.dispatch(getNotesAction(noteId, nextToItem));
  }

  updateNotes(notes: IStorable<readonly NoteItem[]>) {
    this.dispatch(updateNotesAction(notes));
  }

  saveNote(note: NoteItem) {
    this.dispatch(saveNoteAction(note));
  }

  deleteNote(note: NoteItem) {
    this.dispatch(deleteNoteAction(note));
  }

  //#endregion

  //#region Strongs

  async getStrongs(strongsNumber: string[], dict: StrongsDictionary, nextToItem: CardItem = null) {
    const cards = [];
    for (const sn of strongsNumber) {
      const card = await this.getStrongsCard(sn, dict);
      cards.push(card);
    }
    if (cards.length < 1) {
      return;
    }

    this.dispatch(addCardsAction(cards, nextToItem));
  }

  async getStrongsCard(strongsNumber: string, dict: StrongsDictionary) {
    const result = await this.getStrongsFromApi(strongsNumber, dict);
    if (!result) {
      return;
    }
    const d = dict === 'grk' ? 'G' : 'H';

    const card: CardItem = {
      qry: `${d}${strongsNumber}`,
      type: CardType.Strongs,
      data: result,
    };

    return card;
  }

  private async getStrongsFromApi(strongsNumber: string, dict: string): Promise<StrongsResult> {
    const sn = parseInt(strongsNumber, 10);
    const result = {
      prefix: '',
      sn,
      def: null,
      rmac: null,
      crossrefs: null,
      rmaccode: '',
    };

    if (dict === 'grk') {
      result.prefix = 'G';
      if (sn > 5624 || sn < 1) {
        this.dispatchError(
          `Strong's Number G${sn} is out of range. Strong's numbers range from 1 - 5624 in the New Testament.`
        );
        return;
      }
    } else {
      result.prefix = 'H';
      if (sn > 8674 || sn < 1) {
        this.dispatchError(
          `Strong's Number H${sn} is out of range. Strong's numbers range from 1 - 8674 in the Old Testament.`
        );
        return;
      }
    }

    let url = `${dict}${Math.ceil(sn / 100)}.json`;
    try {
      const d = await lastValueFrom(this.http.get<StrongsDefinition[]>(`${this.dataPath}/strongs/${url}`));
      result.def = d.find((el) => el.i === result.prefix + result.sn);
    } catch (err) {
      this.dispatchError(`Unable to retrieve Strong's Data for ${result.prefix}${result.sn}`);
      return;
    }

    try {
      const d = await lastValueFrom(this.http.get<StrongsCrossReference[]>(`${this.dataPath}/strongscr/cr${url}`));

      result.crossrefs = d.find((o) => o.id.toUpperCase() === result.prefix + result.sn);
    } catch (err) {
      this.dispatchError(`Unable to retrieve Strong\'s Cross References for ${result.prefix}${result.sn}`);

      return;
    }

    if (dict === 'grk') {
      url = `${this.dataPath}/rmac/rs${Math.ceil(sn / 1000)}.json`;

      // rmac is a two get process.
      // first, get the rmac code.
      try {
        const rmacCrossReferences = await lastValueFrom(this.http.get<RMACCrossReference[]>(url));

        // deal with RMAC
        const referencesForThisStrongsNumber = rmacCrossReferences.filter((el) => {
          return el.i === sn.toString();
        });

        if (referencesForThisStrongsNumber.length === 0) {
          return result as StrongsResult;
        }
        result.rmaccode = referencesForThisStrongsNumber[0].r;
      } catch (err) {
        this.dispatchError('Unable to retrieve RMAC');
        return;
      }

      // if you were able to get the rmac code, then get the related definition.
      if (result.rmaccode !== undefined) {
        url = `${this.dataPath}/rmac/r-${result.rmaccode.substring(0, 1)}.json`;

        try {
          const rmacDefinitions = await lastValueFrom(this.http.get<RMACDefinition[]>(url));
          result.rmac = rmacDefinitions.find((o) => o.id.toLowerCase() === result.rmaccode);
        } catch (err) {
          this.dispatchError('Unable to retrieve RMAC');
          return;
        }
      }
    }
    return result as StrongsResult;
  }

  //#endregion

  //#region Bible Passages

  async getPassage(ref: BibleReference, nextToItem: CardItem = null) {
    const card = await this.composeBiblePassageCardItem(ref);
    if (!card) {
      return; // nothing was returned. so an error occurred.
    }

    this.dispatch(addCardsAction([card], nextToItem));
  }

  async updatePassage(oldCard: CardItem, ref: BibleReference) {
    const newCard = await this.composeBiblePassageCardItem(ref);

    this.dispatch(updateCardAction(newCard, oldCard));
  }

  private async composeBiblePassageCardItem(ref: BibleReference) {
    const result = await this.getPassageFromApi(ref.section);
    return {
      qry: ref.toString(),
      type: CardType.Passage,
      data: result,
    };
  }

  private async getPassageFromApi(section: Section) {
    try {
      const chapters = []; // the verses from the chapter.

      if (Number(section.start.chapter) > section.book.lastChapter) {
        this.dispatchError(
          `The requested chapter ${section.book.name} is out of range.  Please pick a chapter between 1 and ${section.book.lastChapter}.`
        );
        return;
      }

      if (Number(section.end.chapter) > section.book.lastChapter) {
        this.dispatchError(
          `The requested chapter ${section.book.name} is out of range.  Please pick a chapter between 1 and ${section.book.lastChapter}.`
        );
        return;
      }

      for (let i = Number(section.start.chapter); i <= Number(section.end.chapter); i++) {
        try {
          const d = await lastValueFrom(
            this.http.get<BiblePassage>(`${this.dataPath}/bibles/kjv_strongs/${section.book.bookNumber}-${i}.json`)
          );
          chapters.push(d);
        } catch (err) {
          this.dispatchError(`Unable to retrieve bible passage ${BibleReference.toString(section)}.`);
          return;
        }
      }

      const passages: BiblePassage[] = [];

      // prep the passages
      for (let j = 0; j < chapters.length; j++) {
        const vss: BibleVerse[] = [];
        let start: number;
        let end: number;

        // figure out the start verse.
        if (j === 0) {
          start = section.start.verse;
        } else {
          start = 1;
        }

        // clamp the start to a valid range.
        if (start < 1) {
          start = 1;
        }

        if (start > chapters[j].vss.length) {
          start = chapters[j].vss.length;
        }

        // figure out the end verse
        if (j + 1 === chapters.length) {
          end = section.end.verse;
        } else {
          // go to the end of the chapter
          end = chapters[j].vss.length;
        }

        // get the verses requested.
        const tvs = chapters[j].vss.length;
        if (end === 0 || end > tvs) {
          end = tvs;
        }

        // we're using c based indexes here, so the index is 1 less than the verse #.
        for (let i = start; i <= end; i++) {
          vss.push(chapters[j].vss[i - 1]);
        }

        passages.push({
          ch: chapters[j].ch,
          vss,
        });
      }

      // convert into paragraphs.
      const cs = await this.convertToParagraphPassages(passages, section);

      const result = {
        cs,
        dict: section.book.bookNumber > 39 ? 'grk' : 'heb',
        ref: BibleReference.toString(section),
      } as BiblePassageResult;

      return result;
    } catch (error) {
      this.dispatchError(`An unknown error occurred:  ${error}.`);
    }
  }

  private async convertToParagraphPassages(chapters: BiblePassage[], section: Section) {
    // get the paragraphs the first time if you haven't already.
    if (!this.paragraphs) {
      this.paragraphs = await lastValueFrom(this.http.get<HashTable<Paragraph>>(`${this.dataPath}/bibles/paras.json`));
    }

    const passages: BibleParagraphPassage[] = [];
    for (const ch of chapters) {
      const passage = {
        ch: ch.ch,
        paras: this.convertToParagraphs(ch, section, this.paragraphs),
      };

      passages.push(passage);
    }

    return passages;
  }

  private convertToParagraphs(
    ch: BiblePassage,
    section: Section,
    paragraphMarkers: HashTable<Paragraph>
  ): BibleParagraph[] {
    // group the verses into paragraphs.

    // create an initial paragraph to hold verses that might come before a paragraph.
    let para = { p: { h: '', p: 0 }, vss: [] };
    const paras = [];

    // for each verse in the chapter, break them into paragraphs.
    for (const v of ch.vss) {
      if (this.getRefKey(v, section) in paragraphMarkers) {
        paras.push(para);
        para = {
          p: paragraphMarkers[this.getRefKey(v, section)],
          vss: [v],
        };

        if (para.p === undefined) {
          para.p = { h: '', p: 0 };
        } // just in case you can't find a paragraph.
      } else {
        para.vss.push(v);
      }
    }

    // add the final paragraph if it has verses.
    if (para.vss.length > 0) {
      paras.push(para);
    }

    return paras;
  }

  private getRefKey(vs: BibleVerse, section: Section) {
    return BibleReference.formatReferenceKey(section.book.bookNumber, section.start.chapter, vs.v);
  }

  //#endregion

  //#region Word Search

  async getWords(qry: string, nextToItem: CardItem = null) {
    const result = await this.getWordsFromApi(qry);

    if (!result) {
      return; // nothing was returned. so an error occurred.
    }

    const card = {
      qry,
      type: CardType.Word,
      data: result,
    } as CardItem;

    this.dispatch(addCardsAction([card], nextToItem));
  }

  private async getWordsFromApi(qry: string): Promise<WordLookupResult> {
    // its possible that this might get called before the word to stem map is build.  first time, check and populate...
    if (!this.wordToStem) {
      await this.getStemWordIndex();
    }

    const excluded: string[] = [];

    // now carry on...
    const qs = this.normalizeQueryString(qry);
    const results: (readonly string[])[] = [];

    // Loop through each query term.
    for (const q of qs) {
      if (this.excludedWords.has(q)) {
        excluded.push(q);
        continue; // skip this word.
      }

      if (!this.wordToStem.has(q)) {
        this.dispatch(
          updateErrorAction({
            msg: `Unable to find the word "${q}" in any passages.`,
          })
        );
        return;
      }

      const stem =
        q.startsWith('"') && q.endsWith('"')
          ? q.split('"').join('') // if its quoted, search the exact term.
          : this.wordToStem.get(q); // otherwise use the stem to get a broader set of results.

      // handle the first case.
      if (stem <= this.searchIndexArray[0]) {
        results.unshift(
          await this.getSearchReferences(`${this.dataPath}/index/${this.searchIndexArray[0]}idx.json`, stem)
        );
        break;
      }

      // For each query term, figure out which file it is in, and get it.
      for (let w = 1; w < this.searchIndexArray.length; w++) {
        // If we are at the end of the array, we want to use a different test.
        if (stem <= this.searchIndexArray[w] && stem > this.searchIndexArray[w - 1]) {
          results.unshift(
            await this.getSearchReferences(`${this.dataPath}/index/${this.searchIndexArray[w]}idx.json`, stem)
          );
        }
      }
    } // End of loop through query terms

    // Now we need to test results.  If there is more than one item in the array, we need to find the set
    //  that is shared by all of them.  IF not, we can just return those refs.
    if (results.length === 0) {
      if (excluded.length > 0) {
        this.dispatch(
          updateErrorAction({
            msg: `The ${excluded.length > 1 ? 'words' : 'word'} "${excluded.reduce((prev, curr) => {
              return `${prev}, ${curr}`;
            })}" ${excluded.length > 1 ? 'have' : 'has'} been excluded from search because it is too common.`,
          })
        );
        return;
      }

      this.dispatch(
        updateErrorAction({
          msg: `No passages found for query: ${qry}.`,
        })
      );
      return;
    }

    // // in this case, let the user know, but continue on because a result was found.
    // if (excluded.length > 0) {
    //   this.dispatch({
    //     type: 'UPDATE_ERROR',
    //     error: {
    //       msg: `The ${excluded.length > 1 ? 'words' : 'word'} "${excluded.reduce((prev, curr, idx, arr) => {
    //         return `${prev}, ${curr}`;
    //       })}" ${excluded.length > 1 ? 'have' : 'has'} been excluded from search because it is too common.`,
    //     },
    //   });
    // }

    if (results.length === 1) {
      // we go down this path if only one word was searched for
      return {
        refs: results[0],
        word: qry,
      };
    } else {
      // we go down this path if more than one word was searched for
      return {
        refs: this.findSharedSet(results),
        word: qry,
      };
    }
  }

  /**
   * Gets the references a given word is found in.
   * Returns a string[].
   * @param url - The url of the word index
   * @param query - The word to lookup.
   */
  private async getStemWordIndex() {
    this.wordToStem = new Map<string, string>();
    try {
      const r = await lastValueFrom(this.http.get<WordToStem[]>(`${this.dataPath}/index/word_to_stem_idx.json`));

      // find the right word
      for (const i of r) {
        this.wordToStem.set(i.w, i.s);
      }
    } catch (err) {
      this.dispatch(
        updateErrorAction({
          msg: err,
        })
      );
      return;
    }
  }

  /**
   * Gets the references a given word is found in.
   * Returns a string[].
   * @param url - The url of the word index
   * @param query - The word to lookup.
   */
  private async getSearchReferences(url: string, query: string) {
    // getSearchRefs takes a url and uses ajax to retrieve the references and returns an array of references.
    let r: IndexResult[];

    try {
      r = await lastValueFrom(this.http.get<IndexResult[]>(url));
    } catch (err) {
      this.dispatch(
        updateErrorAction({
          msg: err,
        })
      );
      return;
    }
    // find the right word
    const refs = r.filter((o) => o.w === query);
    if (refs.length > 0) {
      return refs[0].r;
    } else {
      return [];
    }
  }

  private buildIndexArray() {
    const words: string[] = [];
    words.unshift('abishur');
    words.unshift('achor');
    words.unshift('adoni');
    words.unshift('afterward');
    words.unshift('ahishahar');
    words.unshift('alleg');
    words.unshift('ambush');
    words.unshift('ancestor');
    words.unshift('aphik');
    words.unshift('arbah');
    words.unshift('arodi');
    words.unshift('ashkenaz');
    words.unshift('ate');
    words.unshift('azaniah');
    words.unshift('backbiteth');
    words.unshift('barbarian');
    words.unshift('beard');
    words.unshift('begettest');
    words.unshift('beneath');
    words.unshift('bethabara');
    words.unshift('bilshan');
    words.unshift('blindeth');
    words.unshift('booti');
    words.unshift('breaketh');
    words.unshift('bucket');
    words.unshift('cabbon');
    words.unshift('caphtor');
    words.unshift('causeless');
    words.unshift('chapmen');
    words.unshift('chese');
    words.unshift('chrysoprasus');
    words.unshift('cloth');
    words.unshift('common');
    words.unshift('confer');
    words.unshift('contendest');
    words.unshift('couch');
    words.unshift('creepeth');
    words.unshift('cursest');
    words.unshift('dare');
    words.unshift('deckest');
    words.unshift('delus');
    words.unshift('devic');
    words.unshift('direct');
    words.unshift('dispos');
    words.unshift('dote');
    words.unshift('drown');
    words.unshift('ebal');
    words.unshift('eldest');
    words.unshift('elkanah');
    words.unshift('encount');
    words.unshift('entranc');
    words.unshift('escapeth');
    words.unshift('eventid');
    words.unshift('experi');
    words.unshift('fallen');
    words.unshift('feedest');
    words.unshift('filth');
    words.unshift('fleec');
    words.unshift('forbor');
    words.unshift('forsook');
    words.unshift('fret');
    words.unshift('gaham');
    words.unshift('gazit');
    words.unshift('gibbethon');
    words.unshift('glede');
    words.unshift('gospel');
    words.unshift('groaneth');
    words.unshift('hadid');
    words.unshift('hammoleketh');
    words.unshift('haraseth');
    words.unshift('hashupha');
    words.unshift('hazeroth');
    words.unshift('height');
    words.unshift('hereaft');
    words.unshift('higher');
    words.unshift('holdest');
    words.unshift('hosah');
    words.unshift('huram');
    words.unshift('ilai');
    words.unshift('indit');
    words.unshift('intellig');
    words.unshift('ishuai');
    words.unshift('jaasiel');
    words.unshift('jamin');
    words.unshift('jechoniah');
    words.unshift('jemuel');
    words.unshift('jethlah');
    words.unshift('joiakim');
    words.unshift('jucal');
    words.unshift('keilah');
    words.unshift('kishi');
    words.unshift('lade');
    words.unshift('laugheth');
    words.unshift('lehem');
    words.unshift('lifetim');
    words.unshift('loath');
    words.unshift('lucif');
    words.unshift('madmannah');
    words.unshift('maktesh');
    words.unshift('marcaboth');
    words.unshift('mattock');
    words.unshift('melchiah');
    words.unshift('merri');
    words.unshift('midian');
    words.unshift('mire');
    words.unshift('moder');
    words.unshift('movabl');
    words.unshift('naarath');
    words.unshift('nazaren');
    words.unshift('nephish');
    words.unshift('ninth');
    words.unshift('oath');
    words.unshift('olivet');
    words.unshift('oregim');
    words.unshift('overran');
    words.unshift('palet');
    words.unshift('parvaim');
    words.unshift('pedigre');
    words.unshift('permit');
    words.unshift('philetus');
    words.unshift('pisidia');
    words.unshift('pluck');
    words.unshift('pound');
    words.unshift('prey');
    words.unshift('propiti');
    words.unshift('purer');
    words.unshift('rabbith');
    words.unshift('ravin');
    words.unshift('redeem');
    words.unshift('remaind');
    words.unshift('reproveth');
    words.unshift('reverend');
    words.unshift('rishathaim');
    words.unshift('rudder');
    words.unshift('sahadutha');
    words.unshift('saphir');
    words.unshift('scent');
    words.unshift('sect');
    words.unshift('seorim');
    words.unshift('shabbethai');
    words.unshift('sharaim');
    words.unshift('sheepmast');
    words.unshift('shethar');
    words.unshift('shisha');
    words.unshift('shrine');
    words.unshift('signif');
    words.unshift('sitnah');
    words.unshift('sloth');
    words.unshift('soever');
    words.unshift('sow');
    words.unshift('spittl');
    words.unshift('state');
    words.unshift('stool');
    words.unshift('stronger');
    words.unshift('sukkiim');
    words.unshift('sweat');
    words.unshift('tahanit');
    words.unshift('taskmast');
    words.unshift('tempteth');
    words.unshift('thereabout');
    words.unshift('thread');
    words.unshift('timber');
    words.unshift('tong');
    words.unshift('travail');
    words.unshift('trumpet');
    words.unshift('unchang');
    words.unshift('unperfect');
    words.unshift('urban');
    words.unshift('vashti');
    words.unshift('visitest');
    words.unshift('warrior');
    words.unshift('web');
    words.unshift('whereupon');
    words.unshift('winebibb');
    words.unshift('womenserv');
    words.unshift('writeth');
    words.unshift('zalmon');
    words.unshift('zena');
    words.unshift('ziphah');
    words.unshift('zuzim');

    return words;
  }

  /*
   *  Returns a list of references in string form as a string[] that are shared
   *  given a list of lists of references in string form.
   */
  private findSharedSet(referenceSet: (readonly string[])[]) {
    const results = [];
    // FindSharedSet takes an array of reference arrays, and figures out
    // which references are shared by all arrays/sets, then returns a single
    // array of references.
    for (let j = 0; j < referenceSet.length; j++) {
      const refs = referenceSet[j];
      results[j] = []; // initialize inner array
      if (refs != null) {
        for (let i = 0; i < refs.length; i++) {
          const r = refs[i].split(':');
          // convert references to single integers.
          //  Book * 100000000, Chapter * 10000, Verse remains same, add all together.
          results[j][i] = this.toReferenceNumber(r);
        }
      } else {
        return null;
      }
    }

    // get the first result
    let first = results[0];

    // for each additional result, get the shared set
    for (let i = 1; i < results.length; i++) {
      first = this.returnSharedSet(results[i], first);
    }

    // convert the references back into book, chapter and verse.
    for (let i = 0; i < first.length; i++) {
      const ref = first[i];
      first[i] = this.toReferenceString(ref);
    }

    return first;
  }

  private toReferenceString(ref: number) {
    return BibleReference.formatReferenceKey(
      Math.floor(ref / 100000000),
      Math.floor((ref % 100000000) / 10000),
      Math.floor((ref % 100000000) % 10000)
    );
  }

  private toReferenceNumber(r: string[]) {
    let ref = parseInt(r[0], 10) * 100000000;
    ref = ref + parseInt(r[1], 10) * 10000;
    ref = ref + parseInt(r[2], 10) * 1;
    return ref;
  }

  private returnSharedSet(x, y) {
    /// <summary>
    ///  Takes two javascript arrays and returns an array
    ///  containing a set of values shared by arrays.
    /// </summary>
    // declare iterator
    let i = 0;
    // declare terminator
    let t = x.length < y.length ? x.length : y.length;
    // sort the arrays
    x.sort((a, b) => a - b);
    y.sort((a, b) => a - b);
    // in this loop, we remove from the arrays, the
    //  values that aren't shared between them.
    while (i < t) {
      if (x[i] === y[i]) {
        i++;
      }

      if (x[i] < y[i]) {
        x.splice(i, 1);
      }

      if (x[i] > y[i]) {
        y.splice(i, 1);
      }

      t = x.length < y.length ? x.length : y.length;
      // we have to make sure to remove any extra values
      // at the end of an array when we reach the end of
      // the other.
      if (t === i && t < x.length) {
        x.splice(i, x.length - i);
      }

      if (t === i && t < y.length) {
        y.splice(i, x.length - i);
      }
    }
    // we could return y, because at this time, both arrays
    // are identical.
    return x;
  }

  private normalizeQueryString(qry: string): string[] {
    qry = qry.toLowerCase();
    return qry.replace(/'/g, '').replace(/\s+/g, ' ').split(' ');
  }

  //#endregion
}

﻿namespace DynamicBibleUtility
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCreateIndex = new System.Windows.Forms.Button();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.btnCreateText = new System.Windows.Forms.Button();
            this.btnCreateStrongs = new System.Windows.Forms.Button();
            this.btnCreateStrongsDict = new System.Windows.Forms.Button();
            this.btnCreateRMAC = new System.Windows.Forms.Button();
            this.btnRmacCrossRefs = new System.Windows.Forms.Button();
            this.btnCreateGeolocationJson = new System.Windows.Forms.Button();
            this.createGeolocationJsonTooltip = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // btnCreateIndex
            // 
            this.btnCreateIndex.Location = new System.Drawing.Point(12, 12);
            this.btnCreateIndex.Name = "btnCreateIndex";
            this.btnCreateIndex.Size = new System.Drawing.Size(75, 23);
            this.btnCreateIndex.TabIndex = 0;
            this.btnCreateIndex.Text = "Create Index";
            this.btnCreateIndex.UseVisualStyleBackColor = true;
            this.btnCreateIndex.Click += new System.EventHandler(this.CreateIndex_Click);
            // 
            // txtStatus
            // 
            this.txtStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatus.Location = new System.Drawing.Point(13, 42);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(860, 507);
            this.txtStatus.TabIndex = 1;
            // 
            // btnCreateText
            // 
            this.btnCreateText.Location = new System.Drawing.Point(93, 12);
            this.btnCreateText.Name = "btnCreateText";
            this.btnCreateText.Size = new System.Drawing.Size(75, 23);
            this.btnCreateText.TabIndex = 2;
            this.btnCreateText.Text = "CreateText";
            this.btnCreateText.UseVisualStyleBackColor = true;
            this.btnCreateText.Click += new System.EventHandler(this.btnCreateText_Click);
            // 
            // btnCreateStrongs
            // 
            this.btnCreateStrongs.Location = new System.Drawing.Point(174, 12);
            this.btnCreateStrongs.Name = "btnCreateStrongs";
            this.btnCreateStrongs.Size = new System.Drawing.Size(144, 23);
            this.btnCreateStrongs.TabIndex = 3;
            this.btnCreateStrongs.Text = "Create Strongs Cross Refs";
            this.btnCreateStrongs.UseVisualStyleBackColor = true;
            this.btnCreateStrongs.Click += new System.EventHandler(this.btnCreateStrongs_Click);
            // 
            // btnCreateStrongsDict
            // 
            this.btnCreateStrongsDict.Location = new System.Drawing.Point(324, 12);
            this.btnCreateStrongsDict.Name = "btnCreateStrongsDict";
            this.btnCreateStrongsDict.Size = new System.Drawing.Size(126, 23);
            this.btnCreateStrongsDict.TabIndex = 4;
            this.btnCreateStrongsDict.Text = "Create Strongs Dict";
            this.btnCreateStrongsDict.UseVisualStyleBackColor = true;
            this.btnCreateStrongsDict.Click += new System.EventHandler(this.btnCreateStrongsDict_Click);
            // 
            // btnCreateRMAC
            // 
            this.btnCreateRMAC.Location = new System.Drawing.Point(456, 12);
            this.btnCreateRMAC.Name = "btnCreateRMAC";
            this.btnCreateRMAC.Size = new System.Drawing.Size(101, 23);
            this.btnCreateRMAC.TabIndex = 5;
            this.btnCreateRMAC.Text = "Create RMAC";
            this.btnCreateRMAC.UseVisualStyleBackColor = true;
            this.btnCreateRMAC.Click += new System.EventHandler(this.btnCreateRMAC_Click);
            // 
            // btnRmacCrossRefs
            // 
            this.btnRmacCrossRefs.Location = new System.Drawing.Point(563, 12);
            this.btnRmacCrossRefs.Name = "btnRmacCrossRefs";
            this.btnRmacCrossRefs.Size = new System.Drawing.Size(149, 23);
            this.btnRmacCrossRefs.TabIndex = 6;
            this.btnRmacCrossRefs.Text = "Create RMAC Cross Refs";
            this.btnRmacCrossRefs.UseVisualStyleBackColor = true;
            this.btnRmacCrossRefs.Click += new System.EventHandler(this.btnRmacCrossRefs_Click);
            // 
            // btnCreateGeolocationJson
            // 
            this.btnCreateGeolocationJson.Location = new System.Drawing.Point(718, 12);
            this.btnCreateGeolocationJson.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCreateGeolocationJson.Name = "btnCreateGeolocationJson";
            this.btnCreateGeolocationJson.Size = new System.Drawing.Size(155, 23);
            this.btnCreateGeolocationJson.TabIndex = 7;
            this.btnCreateGeolocationJson.Text = "Create Geolocation JSON";
            this.createGeolocationJsonTooltip.SetToolTip(this.btnCreateGeolocationJson, "Creates JSON files for geolocation data from https://www.openbible.info/geo/data/" +
        "merged.txt");
            this.btnCreateGeolocationJson.UseVisualStyleBackColor = true;
            this.btnCreateGeolocationJson.Click += new System.EventHandler(this.btnCreateGeolocationJson_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 561);
            this.Controls.Add(this.btnCreateGeolocationJson);
            this.Controls.Add(this.btnRmacCrossRefs);
            this.Controls.Add(this.btnCreateRMAC);
            this.Controls.Add(this.btnCreateStrongsDict);
            this.Controls.Add(this.btnCreateStrongs);
            this.Controls.Add(this.btnCreateText);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.btnCreateIndex);
            this.Name = "frmMain";
            this.Text = "Dynamic Bible Utility";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreateIndex;
        private System.Windows.Forms.TextBox txtStatus;
        private System.Windows.Forms.Button btnCreateText;
        private System.Windows.Forms.Button btnCreateStrongs;
        private System.Windows.Forms.Button btnCreateStrongsDict;
        private System.Windows.Forms.Button btnCreateRMAC;
        private System.Windows.Forms.Button btnRmacCrossRefs;
        private System.Windows.Forms.Button btnCreateGeolocationJson;
        private System.Windows.Forms.ToolTip createGeolocationJsonTooltip;
    }
}


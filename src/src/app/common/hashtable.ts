export interface HashTable<T> {
  readonly [key: string]: T;
}

import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-ok-cancel-modal',
  templateUrl: 'ok-cancel-modal.component.html',
  styleUrls: ['./ok-cancel-modal.component.scss'],
})
export class OkCancelModalComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: OkCancelData,
    public dialogRef: MatDialogRef<OkCancelModalComponent>
  ) {}

  cancel() {
    this.dialogRef.close({
      ok: false,
    });
  }

  ok() {
    this.dialogRef.close({
      ok: true,
    });
  }
}

export interface OkCancelData {
  title: string;
  content: string;
}
export interface OkCancelResult {
  ok: boolean;
  data: any | undefined;
}

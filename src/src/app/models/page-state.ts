import { DataReference } from './card-state';

export interface SavedPage {
  readonly queries: readonly DataReference[];
  readonly title: string;
  readonly id: string;
}

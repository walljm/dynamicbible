import { BehaviorSubject, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

/**
 * Creates a base class from which to extend a service to provide predictable state to components.
 *
 * **If you're looking at this function because calling it yields an error that looks like this:**
 *
 * ```text
 * Type 'YourStateTypeNeedsToBeImmutable<State>' is not a constructor function type.
 * ```
 *
 * **That error means that the type you specified as `TState` (most likely the first parameter and return type of your
 * reducer) isn't fully immutable.** The Redux pattern requires that you treat your state as completely immutable—that
 * means that every property in your state hierarchy must be marked as `readonly` and you must use immutable collection
 * types. You can either do this by hand, or use the {@link Immutable} helper type also exported by this module to
 * quickly make an entire type hierarchy immutable.
 *
 * @param reducer A function that takes the previous state and the dispatched action and returns the new state.
 * @param initialState The initial state of the service.
 */
export function createReducingService<TState>(initialState: TState) {
  const stateServiceClass = class extends ReducingService<TState> {
    constructor() {
      super(initialState);
    }
  };

  return stateServiceClass as IfImmutable<TState, typeof stateServiceClass, YourStateTypeNeedsToBeImmutable<TState>>;
}

export interface IReducingAction<TState> {
  handle: (state: TState) => TState;
}

/**
 * Creates a deeply-immutable type from the provided type. Objects' properties will be marked as `readonly`, array types
 * will be replaced with {@link ReadonlyArray}, {@link Map} types will be replaced with {@link ReadonlyMap}, and
 * {@link Set} types will be replaced with {@link ReadonlySet}.
 */
export type Immutable<T> = T extends undefined | null | boolean | string | number
  ? T
  : T extends Array<infer U>
  ? ReadonlyArray<Immutable<U>>
  : T extends Map<infer K, infer V>
  ? ReadonlyMap<Immutable<K>, Immutable<V>>
  : T extends Set<infer M>
  ? ReadonlySet<Immutable<M>>
  : { readonly [N in keyof T]: Immutable<T[N]> };

type IfEquals<X, Y, A = X, B = never> = (<T>() => T extends X ? 1 : 2) extends <T>() => T extends Y ? 1 : 2 ? A : B;
type IfImmutable<TState extends {}, TImmutable = TState, TMutable = never> = IfEquals<
  TState,
  { readonly [K in keyof TState]: Immutable<TState[K]> },
  TImmutable,
  TMutable
>;

// eslint-disable-next-line
type YourStateTypeNeedsToBeImmutable<TState> = {};

class ReducingService<TState> {
  private internalState$: BehaviorSubject<TState>;

  constructor(private initialState: TState) {
    this.internalState$ = new BehaviorSubject(initialState);
  }

  // expose as observable.
  public get state$() {
    return this.internalState$.asObservable();
  }

  protected dispatch(action: IReducingAction<TState>) {
    this.internalState$.next(action.handle(this.internalState$.value));
  }

  /**
   * Gets the current state managed by the service. **You should only use the current state to validate operations or
   * help to construct actions to be dispatched.** Try not to expose any state retrieved using this method outside the
   * derived service class.
   */
  protected getState(): TState {
    return this.internalState$.value;
  }
  /**
   * Creates an observable that provides data derived from the state managed by the service.
   * @param selector A selector that maps the state to the derived data.
   */
  public select<TDerived>(selector: (state: TState) => TDerived): Observable<TDerived> {
    return this.internalState$.pipe(map(selector), distinctUntilChanged());
  }
}

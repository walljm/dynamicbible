﻿namespace DynamicBibleUtility.Geolocation;

/// <summary>
/// An index of Bible location data by location name.
/// </summary>
public class BibleLocationIndexByName
{
    /// <summary>A mapping of location names to full location data (in the JSON format).</summary>
    public readonly IDictionary<string, dynamic> NameToLocationLookup = new Dictionary<string, dynamic>();

    /// <summary>
    /// Creates the index from the provided locations.
    /// </summary>
    /// <param name="locations">The locations to put in the index.</param>
    /// <exception cref="System.NullReferenceException">Thrown if the locations are null.</exception>
    public BibleLocationIndexByName(IEnumerable<BibleLocationReference> locations)
    {
        // INDEX THE LOCATIONS BY NAME.
        foreach (var location in locations)
        {
            // ONLY INDEX THE LOCATION IF IT HAS GEOGRAPHIC COORDINATES.
            // The information currently isn't useful without these coordinates.
            if (location.HasGeographicCoordinates)
            {
                // Since the location data needs to be converted to JSON in different
                // scenarios with different properties serialized, there's not an easy
                // way to simply mark which fields should/shouldn't be serialized in
                // all situations.  While a custom JSON converter could be used,
                // creating a dynamic object here seemed simpler.  In this scenario,
                // the name doesn't need to be included in the member data since
                // it already serves as the key in the lookup.
                NameToLocationLookup[location.Name] = new
                {
                    sn = location.StrongsNumbers,
                    lat = location.Latitude,
                    lon = location.Longitude,
                    vss = location.VerseReferenceStrings,
                };
            }
        }
    }
}
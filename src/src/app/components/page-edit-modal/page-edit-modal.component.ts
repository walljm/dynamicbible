import { Component, Inject } from '@angular/core';
import { UntypedFormBuilder,UntypedFormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA,MatDialogRef } from '@angular/material/dialog';
import { SavedPage } from 'src/app/models/page-state';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-page-edit-modal',
  templateUrl: 'page-edit-modal.component.html',
  styleUrls: ['./page-edit-modal.component.scss'],
})
export class PageEditModalComponent {
  form: UntypedFormGroup;
  dialogTitle = 'Save Page using Current Cards';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: PageEditModalData,
    public dialogRef: MatDialogRef<PageEditModalComponent>,
    private appService: AppService,
    private fb: UntypedFormBuilder
  ) {
    if (data) {
      this.dialogTitle = 'Edit Page Name';
    } else {
      data = {
        title: '',
        savedPage: null,
      };
    }

    this.form = this.fb.group(data);
  }

  cancel() {
    this.dialogRef.close();
  }

  save() {
    if (this.dialogTitle === 'Edit Page Name') {
      this.appService.updateSavedPage({
        ...this.data.savedPage,
        title: this.form.get('title').value,
      });
    } else {
      this.appService.savePage(this.form.get('title').value);
    }
    this.dialogRef.close();
  }
}

export interface PageEditModalData {
  title: string;
  savedPage: SavedPage;
}

import { CardItem,CardType } from '../models/card-state';
import { NoteItem } from '../models/note-state';
import { getCardCacheKey, removeFromCardCache,updateInCardCache } from './card-cache-operations';
import { HashTable } from './hashtable';

describe('Card Cache', () => {
  it('updateCache', () => {
    const card1: CardItem = {
      qry: 'jason',
      type: CardType.Passage,
      data: null,
    };
    const card2: CardItem = {
      qry: 'jason',
      type: CardType.Passage,
      data: {
        id: 'adsf',
        xref: '',
        title: 'adsf',
        content: '',
      } as NoteItem,
    };
    const card3: CardItem = {
      qry: 'jason3',
      type: CardType.Passage,
      data: null,
    };

    const cache: HashTable<CardItem> = {};
    let newCache = updateInCardCache(card1, cache);

    expect(newCache[getCardCacheKey(card1)].qry).toBe('jason', 'Should have added the card');
    expect(newCache[getCardCacheKey(card1)].data).toBe(null, 'Should have null data');

    newCache = updateInCardCache(card2, newCache);
    expect(newCache[getCardCacheKey(card2)].qry).toBe('jason', 'Should have added the card');
    expect((newCache[getCardCacheKey(card1)].data as NoteItem).title).toBe('adsf', 'Should have added the card');
    expect(Object.keys(newCache).length).toBe(1, 'Should still have only 1 item.');

    newCache = updateInCardCache(card3, newCache);
    expect(newCache[getCardCacheKey(card3)].qry).toBe('jason3', 'Should have added the card');
    expect(Object.keys(newCache).length).toBe(2, 'Should have 2 items.');
  });

  it('removeFromCache', () => {
    const card1: CardItem = {
      qry: 'jason',
      type: CardType.Passage,
      data: null,
    };
    const card3: CardItem = {
      qry: 'jason3',
      type: CardType.Passage,
      data: null,
    };

    const cache: HashTable<CardItem> = {};
    let newCache = updateInCardCache(card1, cache);
    newCache = updateInCardCache(card3, newCache);
    expect(Object.keys(newCache).length).toBe(2, 'Should have 2 items.');

    newCache = removeFromCardCache(card1, newCache);
    expect(Object.keys(newCache).length).toBe(1, 'Should remove 1 item');
  });
});

﻿using System.Xml.Serialization;

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

namespace DynamicBible.Schemas;

[Serializable()]
[XmlType(AnonymousType = true)]
[XmlRoot(Namespace = "", IsNullable = false)]
public class XMLBIBLE
{
    [XmlElement("BIBLEBOOK")]
    public XMLBIBLE_BOOK[] BIBLEBOOKS;
}

[Serializable()]
[XmlType(AnonymousType = true)]
public  class XMLBIBLE_BOOK
{
    [XmlElement("CHAPTER")]
    public XMLBIBLE_CHAPTER[] CHAPTERS;


    [XmlAttribute()]
    public byte bnumber;


    [XmlAttribute()]
    public string bname;


    [XmlAttribute()]
    public string bsname;
}

[Serializable()]
[XmlType(AnonymousType = true)]
public  class XMLBIBLE_CHAPTER
{
    [XmlElement("VERS")]
    public XMLBIBLE_VERSES[] VERSES;


    [XmlAttribute()]
    public byte cnumber;
}

[Serializable()]
[XmlType(AnonymousType = true)]
public  class XMLBIBLE_VERSES
{
    [XmlElement("STYLE", typeof(XMLBIBLE_STYLE_ITEM))]
    [XmlElement("gr", typeof(XMLBIBLE_GR))]
    public object[] Items;

    /// <remarks />
    [XmlText()]
    public string[] Text;


    [XmlAttribute()]
    public byte vnumber;
}

[Serializable()]
[XmlType(AnonymousType = true)]
public  class XMLBIBLE_STYLE_ITEM
{
    [XmlElement("STYLE")]
    public XMLBIBLE_STYLE_STYLE[] STYLE;


    public XMLBIBLE_GR gr;


    [XmlText()]
    public string[] Text;


    [XmlAttribute()]
    public string css;
}

[Serializable()]
[XmlType(AnonymousType = true)]
public  class XMLBIBLE_STYLE_STYLE
{
    public XMLBIBLE_STYLE_STYLE_GR gr;


    [XmlText()]
    public string[] Text;


    [XmlAttribute()]
    public string css;
}

[Serializable()]
[XmlType(AnonymousType = true)]
public  class XMLBIBLE_STYLE_STYLE_GR
{
    [XmlAttribute()]
    public string str;


    [XmlText()]
    public string Value;
}

[Serializable()]
[XmlType(AnonymousType = true)]
public  class XMLBIBLE_GR
{
    public XMLBIBLE_GR_GR gr;


    [XmlText()]
    public string[] Text;


    [XmlAttribute()]
    public string str;
}

[Serializable()]
[XmlType(AnonymousType = true)]
public  class XMLBIBLE_GR_GR
{
    [XmlAttribute()]
    public string str;
}
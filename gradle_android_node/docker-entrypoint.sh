#!/usr/bin/env bash
set -eux
pwd
cd /src/android
gradle signingReport
gradle assembleRelease
gradle bundleReleaseBundle
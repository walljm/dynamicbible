using System.Text.Json.Serialization;

namespace DynamicBible.DataPreparation.Models;

public class Bible
{
    [JsonPropertyName("bk")]
    public int BookNumber { get; set; } = -1;

    [JsonPropertyName("chs")]
    public List<Chapter> Chapters { get; set; } = [];
}

public class Chapter
{
    [JsonPropertyName("ch")]
    public int ChapterId { get; set; } = -1;

    [JsonPropertyName("vss")]
    public List<Verse> Verses { get; set; } = [];
}

public class Verse
{
    [JsonPropertyName("v")]
    public int VerseId { get; set; } = -1;

    [JsonPropertyName("w")]
    public List<Text> Word { get; set; } = [];
}

public class Text
{
    public Text(string word, string strongs)
    {
        Word = word;
        StrongsId = strongs;
    }

    public Text(string word)
    {
        Word = word;
    }

    [JsonPropertyName("t")]
    public string Word { get; set; }

    [JsonPropertyName("s")]
    public string StrongsId { get; set; } = string.Empty;
}
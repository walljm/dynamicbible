﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace DynamicBibleUtility.Geolocation
{
    /// <summary>
    /// A location mentioned in the Bible, with geographic coordinates and verse references.
    /// </summary>
    public class BibleLocationReference
    {
        /// <summary>The name of the location.</summary>
        public string Name = "";
        /// <summary>
        /// Any Strong's numbers for the location name, if available.
        /// Includes an 'H' prefix for Hebrew and a 'G' prefix for Greek before the actual numbers.
        /// </summary>
        public IEnumerable<string> StrongsNumbers = new List<string>();
        /// <summary>The latitude of the location, if available.</summary>
        public double? Latitude = null;
        /// <summary>The longitude of the location, if available.</summary>
        public double? Longitude = null;
        /// <summary>References to verses that mention the location.</summary>
        public IEnumerable<BibleVerseReference> VerseReferences = new List<BibleVerseReference>();

        /// <summary>
        /// Gets references to verses that mention the location, in their short string form
        /// as used by the Dynamic Bible app.
        /// </summary>
        public IEnumerable<string> VerseReferenceStrings
        {
            get
            {
                var verse_reference_strings = VerseReferences.Select(verse_reference => verse_reference.ToString());
                return verse_reference_strings;
            }
        }

        /// <summary>
        /// True if this location information has full geographic coordinates; false otherwise.
        /// </summary>
        public bool HasGeographicCoordinates
        {
            get
            {
                bool has_geographic_coordinates = Latitude.HasValue && Longitude.HasValue;
                return has_geographic_coordinates;
            }
        }
    }
}

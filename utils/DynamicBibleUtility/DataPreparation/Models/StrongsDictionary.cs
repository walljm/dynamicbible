using System.Text.Json.Serialization;

namespace DynamicBible.DataPreparation.Models;

//"G208":{"lemma":"ἀκυρόω","kjv_def":"disannul, make of none effect","strongs_def":" to invalidate","derivation":"from G1 (Α) (as a negative particle) and G2964 (κυρόω);"},

public class StrongDictEntry
{
    // number
    public int n { get; set; }

    [JsonPropertyName("i")]
    public string? Dict { get; set; } // G1/H1

    [JsonPropertyName("p")]
    public string? Pronounciation { get; set; } // pronounciation

    [JsonPropertyName("lemma")]
    public string? Lemma { get; set; } // greek/hebrew

    [JsonPropertyName("tr")]
    public string? Translation { get; set; } // translation

    [JsonPropertyName("de")]
    public List<StrongsDictEntryPart> Description { get; set; } = []; // Description
}

public class StrongsDictEntryPart
{
    [JsonPropertyName("sn")]
    public string? StrongsNumber { get; set; }

    [JsonPropertyName("w")]
    public string? Word { get; set; }
}

public class Strongs
{
    [JsonPropertyName("id")]
    public string Id { get; set; } = string.Empty;

    [JsonPropertyName("t")]
    public string Testament { get; set; } = string.Empty;

    [JsonPropertyName("d")]
    public string Definition { get; set; } = string.Empty;

    [JsonPropertyName("ss")]
    public List<StrongRef> StrongsReferences { get; set; } = [];
}

public class StrongRef
{
    [JsonPropertyName("w")]
    public string? Word { get; set; } = string.Empty;

    [JsonPropertyName("rs")]
    public List<BibleRef> BibleReferences { get; set; } = [];
}

public class BibleRef
{
    [JsonPropertyName("r")]
    public string Reference { get; set; } = string.Empty;
}
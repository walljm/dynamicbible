import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { MatDialog } from '@angular/material/dialog';
import { MatSelectChange } from '@angular/material/select';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Capacitor } from '@capacitor/core';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth'
import firebase from '@firebase/app-compat';
import { Overlap } from 'src/app/common/bible-reference';
import { NoteEditModalComponent } from 'src/app/components/note/edit-modal/note-edit-modal.component';
import { PageEditModalComponent } from 'src/app/components/page-edit-modal/page-edit-modal.component';
import { CardFonts } from 'src/app/constants';
import { DisplaySettings } from 'src/app/models/app-state';
import { SavedPage } from 'src/app/models/page-state';
import { AppService } from 'src/app/services/app.service';
import { NavService } from 'src/app/services/nav.service';

import { SubscriberBase } from '../../common/subscriber-base';
import { OkCancelModalComponent, OkCancelResult } from '../ok-cancel-modal/ok-cancel-modal.component';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent extends SubscriberBase {
  displaySettings: DisplaySettings;
  fonts: string[];
  cardFontFamily = '';
  cardFontSize = 10;
  cardMergeStrategy = Overlap.Equal;
  cardMergeStrategies = [];

  currentSavedPage$ = this.appService.select((state) => state.currentSavedPage);
  user$ = this.appService.select((state) => state.user);

  constructor(
    public appService: AppService,
    public authService: AngularFireAuth,
    private navService: NavService,
    private dialog: MatDialog,
    private snackBar: MatSnackBar
  ) {
    super();
    this.fonts = CardFonts;
    // eslint-disable-next-line guard-for-in
    for (const enumIdx in Overlap) {
      this.cardMergeStrategies.push(Overlap[enumIdx]);
    }
    this.addSubscription(
      this.appService.state$.subscribe((state) => {
        this.displaySettings = state.settings.value.displaySettings;
        this.cardFontFamily = state.settings.value.displaySettings.cardFontFamily;
        this.cardFontSize = state.settings.value.displaySettings.cardFontSize;
        this.cardMergeStrategy = state.settings.value.pageSettings.mergeStrategy;
      })
    );

    this.addSubscription(
      this.authService.authState.subscribe((user) => {
        if (user) {
          this.appService.setUser({
            uid: user.uid,
            displayName: user.displayName,
            providerId: user.providerId,
            email: user.email,
          });
        }
      })
    );
  }

  async login() {
    if (Capacitor.isNativePlatform()) {
      GoogleAuth.initialize();

      const googleUser = await GoogleAuth.signIn();
      const credential = firebase.auth.GoogleAuthProvider.credential(googleUser.authentication.idToken);
      (await this.authService.app).auth().signInWithCredential(credential);
    } else {
      this.authService.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(() => {
        console.log('Authenticated.');
      });
    }
  }

  async logout() {
    if (Capacitor.isNativePlatform()) {
      await GoogleAuth.signOut();
    }
    await this.authService.signOut();
    this.appService.setUser(null); // clear the user.
  }

  //#region Page Settings

  savePage() {
    this.navService.closeSettings();
    this.dialog.open(PageEditModalComponent, {
      autoFocus: 'content',
    });
  }

  updatePage(page: SavedPage) {
    this.dialog
      .open(OkCancelModalComponent, {
        data: {
          title: 'Update Saved Page',
          content: `Do you want to update saved page: ${page.title}?`,
        },
      })
      .afterClosed()
      .subscribe((ds: OkCancelResult) => {
        console.log(ds);
        if (ds.ok) {
          this.appService.updateCurrentSavedPage();
          this.navService.closeSettings();
          this.snackBar.open(`${page.title} has been updated!`, '', {
            duration: 3 * 1000,
          });
        }
      });
  }

  createNote() {
    this.navService.closeSettings();
    this.dialog.open(NoteEditModalComponent, {
      autoFocus: 'note-content',
    });
  }

  clearCurrentCard(){
    this.appService.clearCards();
    this.navService.closeSettings();
  }

  //#endregion

  //#region Saved Page Settings

  pageCardMergeSettingsSelected(evt: MatSelectChange) {
    this.appService.updateCardMergeStrategy(evt.value);
  }

  //#endregion

  //#region  Font Settings

  cardFontSelected(evt: MatSelectChange) {
    this.appService.changeCardFontFamily(evt.value);
  }

  cardFontSizeChanged(evt: number) {
    this.appService.changeCardFontSize(evt);
  }

  //#endregion

  //#region Search Settings

  toggleStrongsAsModal(toggle: MatSlideToggleChange) {
    this.appService.updateDisplaySettings({
      ...this.displaySettings,
      showStrongsAsModal: toggle.checked,
    });
  }

  toggleAppendCardToBottom(toggle: MatSlideToggleChange) {
    this.appService.updateDisplaySettings({
      ...this.displaySettings,
      appendCardToBottom: toggle.checked,
    });
  }

  toggleInsertCardNextToItem(toggle: MatSlideToggleChange) {
    this.appService.updateDisplaySettings({
      ...this.displaySettings,
      insertCardNextToItem: toggle.checked,
    });
  }

  toggleClearSearchAfterQuery(toggle: MatSlideToggleChange) {
    this.appService.updateDisplaySettings({
      ...this.displaySettings,
      clearSearchAfterQuery: toggle.checked,
    });
  }
  toggleSyncCardsAcrossDevices(toggle: MatSlideToggleChange) {
    this.appService.updateDisplaySettings({
      ...this.displaySettings,
      syncCardsAcrossDevices: toggle.checked,
    });
  }
  //#endregion

  //#region Passage Settings

  toggleParagraphHeadings(toggle: MatSlideToggleChange) {
    this.appService.updateDisplaySettings({
      ...this.displaySettings,
      showParagraphHeadings: toggle.checked,
    });
  }

  toggleParagraphs(toggle: MatSlideToggleChange) {
    this.appService.updateDisplaySettings({
      ...this.displaySettings,
      showParagraphs: toggle.checked,
    });
  }

  toggleVerseNumbers(toggle: MatSlideToggleChange) {
    this.appService.updateDisplaySettings({
      ...this.displaySettings,
      showVerseNumbers: toggle.checked,
    });
  }

  toggleVersesOnNewLine(toggle: MatSlideToggleChange) {
    this.appService.updateDisplaySettings({
      ...this.displaySettings,
      showVersesOnNewLine: toggle.checked,
    });
  }

  //#endregion
}

using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace VAE.Common.Serialization
{
    public static class JSON
    {
        public static bool SaveData(object o, string path)
        {
            File.WriteAllText(path, Serialize(o));
            return true;
        }

        public static bool SaveData(object o)
        {
            return SaveData(o, o.GetType().Name + ".json");
        }

        public static object GetData(Type t, string path)
        {
            return File.Exists(path)
                ? Deserialize(File.ReadAllText(path), t)
                : Activator.CreateInstance(t);
        }

        public static object GetData(Type t)
        {
            return GetData(t, t.Name + ".json");
        }

        public static object GetData(Type t, string path, List<JsonConverter> lst)
        {
            return File.Exists(path)
                ? Deserialize(File.ReadAllText(path), t, lst)
                : Activator.CreateInstance(t);
        }

        public static object GetData(Type t, List<JsonConverter> lst)
        {
            return GetData(t, t.Name + ".json", lst);
        }

        public static object Deserialize(string s, System.Type t)
        {
            return FromJSON(s, t);
        }

        public static object Deserialize(string s, System.Type t, List<JsonConverter> lst)
        {
            return FromJSON(s, t, lst);
        }

        public static string Serialize(object o)
        {
            return ToJSON(o);
        }

        public static object FromJSON<T>(string json)
        {
            var s = new JsonSerializerSettings();
            s.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            s.NullValueHandling = NullValueHandling.Ignore;
            s.ObjectCreationHandling = ObjectCreationHandling.Replace;
            return JsonConvert.DeserializeObject<T>(json, s);
        }

        public static object FromJSON(string json, Type t)
        {
            return FromJSON(json, t, new List<JsonConverter>());
        }

        public static object FromJSON(string json, Type t, List<JsonConverter> lst)
        {
            var s = new JsonSerializerSettings();
            s.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            foreach (JsonConverter c in lst) s.Converters.Add(c);
            s.NullValueHandling = NullValueHandling.Ignore;
            s.ObjectCreationHandling = ObjectCreationHandling.Replace;
            return JsonConvert.DeserializeObject(json, t, s);
        }

        public static string ToJSON(object o)
        {
            var s = new JsonSerializerSettings();
            s.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            s.Converters.Add(new Newtonsoft.Json.Converters.KeyValuePairConverter());
            s.NullValueHandling = NullValueHandling.Ignore;
            s.ObjectCreationHandling = ObjectCreationHandling.Replace;
            return JsonConvert.SerializeObject(o, s);
        }
    }
}
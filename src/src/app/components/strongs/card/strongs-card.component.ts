import { Clipboard } from '@angular/cdk/clipboard';
import { ChangeDetectionStrategy, Component, ElementRef, Input,ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BibleReference } from 'src/app/common/bible-reference';
import { CardComponent } from 'src/app/components/card.component';
import { StrongsResult } from 'src/app/models/strongs-state';
import { AppService } from 'src/app/services/app.service';

import { StrongsModalComponent } from '../modal/strongs-modal.component';

@Component({
  selector: 'app-strongs-card',
  templateUrl: 'strongs-card.component.html',
  styleUrls: ['./strongs-card.component.scss'],
  preserveWhitespaces: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrongsCardComponent extends CardComponent {
  asModal = false;
  @ViewChild('strongs') strongsElement: ElementRef;

  @Input()
  data: StrongsResult;

  get strongsResult() {
    return this.cardItem.data as StrongsResult;
  }

  constructor(
    protected elementRef: ElementRef,
    protected appService: AppService,
    protected clipboard: Clipboard,
    protected dialog: MatDialog
  ) {
    super(elementRef, dialog, appService, clipboard);
    this.icon$ = appService.select((state) => state.settings.value.cardIcons.strongs);
    this.addSubscription(
      this.appService.state$.subscribe((state) => {
        this.asModal = state.settings.value.displaySettings.showStrongsAsModal;
      })
    );
  }

  copy() {
    const text = this.strongsElement.nativeElement.innerText;
    this.copyToClip(text);
  }

  async openStrongs(q: string) {
    const dict = q.substring(0, 1) === 'H' ? 'heb' : 'grk';
    const sn = q.substring(1);
    if (this.asModal) {
      const card = await this.appService.getStrongsCard(sn, dict);
      this.dialog.open(StrongsModalComponent, {
        data: [card],
        autoFocus: 'content',
      });
    } else {
      this.appService.getStrongs([sn], dict, this.cardItem);
    }
  }

  openPassage(p: string) {
    const ref = BibleReference.makePassageFromReferenceKey(p);
    this.appService.getPassage(ref, this.cardItem);
  }
}

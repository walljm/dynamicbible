package walljm.dynamicbible;

import com.getcapacitor.BridgeActivity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class MainActivity extends BridgeActivity {

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Grab WebView
    WebView webView = findViewById(R.id.webview);
    WebView.setWebContentsDebuggingEnabled(true);

    // The Magic
    WebSettings webSettings = webView.getSettings();
    webSettings.setUseWideViewPort(true);

    // The user might have set a text zoom, but that screws
    //  with the icon sizes.  So set a specific zoom here
    //  first.
    webView.getSettings().setTextZoom(110);
  }
}

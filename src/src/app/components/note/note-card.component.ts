import { Clipboard } from '@angular/cdk/clipboard';
import { Component, ElementRef, Input,ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BibleReference } from 'src/app/common/bible-reference';
import { NoteItem } from 'src/app/models/note-state';
import { AppService } from 'src/app/services/app.service';

import { CardComponent } from '../card.component';
import { OkCancelModalComponent, OkCancelResult } from '../ok-cancel-modal/ok-cancel-modal.component';
import { NoteEditModalComponent } from './edit-modal/note-edit-modal.component';

@Component({
  selector: 'app-note-card',
  templateUrl: './note-card.component.html',
  styleUrls: ['./note-card.component.scss'],
})
export class NoteCardComponent extends CardComponent {
  @ViewChild('note') noteElement: ElementRef;

  @Input()
  inSearch = true;

  get data(): NoteItem {
    return this.cardItem.data as NoteItem;
  }

  constructor(
    protected elementRef: ElementRef,
    protected appService: AppService,
    protected clipboard: Clipboard,
    public dialog: MatDialog
  ) {
    super(elementRef, dialog, appService, clipboard);

    this.icon$ = appService.select((state) => state.settings.value.cardIcons.note);
  }

  copy() {
    const text = this.noteElement.nativeElement.innerText;
    this.copyToClip(text);
  }

  private xrefs: BibleReference[];
  prepXref(xref: string) {
    if (this.xrefs === undefined) {
      this.xrefs = xref.split(';').map((o) => new BibleReference(o));
    }
    return this.xrefs;
  }

  openPassage(ref: BibleReference) {
    this.appService.getPassage(ref, this.cardItem);
  }

  edit() {
    this.dialog.open(NoteEditModalComponent, {
      data: this.cardItem,
      autoFocus: 'note-content',
    });
  }

  delete() {
    this.dialog
      .open(OkCancelModalComponent, {
        data: {
          title: 'Delete Note?',
          content: `Do you want to permanently delete the note '${(this.cardItem.data as NoteItem).title}'?`,
        },
      })
      .afterClosed()
      .subscribe((ds: OkCancelResult) => {
        if (ds.ok) {
          this.appService.deleteNote(this.cardItem.data as NoteItem);
        }
      });
  }
}

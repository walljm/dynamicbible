import { Component, EventEmitter,Input, Output } from '@angular/core';
import { BibleReference } from 'src/app/common/bible-reference';

import { StrongsResult } from '../../models/strongs-state';

@Component({
  selector: 'app-strongs',
  templateUrl: 'strongs.component.html',
  styleUrls: ['./strongs.component.scss'],
  preserveWhitespaces: true,
})
export class StrongsComponent {
  @Input()
  data: StrongsResult;

  @Input()
  isCard = false;

  @Output()
  openPassage = new EventEmitter<string>();

  @Output()
  openStrongs = new EventEmitter<string>();

  constructor() {}

  triggerOpenPassage(p: string) {
    this.openPassage.emit(p);
  }

  triggerOpenStrongs(q: string) {
    this.openStrongs.emit(q);
  }

  makePassage(p: string) {
    return BibleReference.makePassageFromReferenceKey(p);
  }
}

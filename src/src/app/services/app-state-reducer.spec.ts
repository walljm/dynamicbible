import { TestBed } from '@angular/core/testing';

import { Overlap } from '../common/bible-reference';
import { getCardCacheKey } from '../common/card-cache-operations';
import { MoveDirection } from '../common/move-direction';
import { Storable, StorableType } from '../common/storable';
import { AppState, User } from '../models/app-state';
import { CardItem,CardType } from '../models/card-state';
import { NoteItem } from '../models/note-state';
import { SavedPage } from '../models/page-state';
import {
  addCardsAction,
  addCardToSavedPageAction,
  deleteNoteAction,
  findNotesAction,
  getNewestStorable,
  getNotesAction,
  moveCardAction,
  moveSavedPageCardAction,
  removeCardAction,
  removeSavedPageAction,
  saveNoteAction,
  savePageAction,
  setUserAction,
  updateAutoCompleteAction,
  updateCardAction,
  updateCardFontFamilyAction,
  updateCardFontSizeAction,
  updateCardMergeStrategyAction,
  updateCurrentPageAction,
  updateNotesAction,
  updateSavedPageAction,
  updateSavedPagesAction,
  updateSettingsAction,
} from './app.service';

describe('getNewestStorable', () => {
  it('maybeMutateStorable', () => {
    const s1 = {
      createdOn: new Date(2019, 1, 1, 0, 0, 0, 0).toISOString(),
      type: StorableType.initial,
      value: 'test1',
    };

    const s2 = {
      createdOn: new Date(2020, 1, 1, 0, 0, 0, 0).toISOString(),
      type: StorableType.initial,
      value: 'test1',
    };

    expect(getNewestStorable(s1, s2)).toBe(s2, 'Should be the second item');
    expect(getNewestStorable(null, s2)).toBe(s2, 'Should be the second item');
    expect(getNewestStorable(s1, null)).toBe(s1, 'Should be the first item');
    expect(getNewestStorable(null, null)).toBe(null, 'Should be null');
  });
});

describe('AppService Reducer', () => {
  const preState = {
    user: null,
    currentCards: {
      createdOn: new Date(0).toISOString(),
      type: StorableType.initial,
      value: [],
    },
    cardCache: {},
    autocomplete: [],
    currentSavedPage: {
      queries: [],
      title: 'page1',
      id: 'myid1',
    },
    savedPages: {
      createdOn: new Date(0).toISOString(),
      type: StorableType.initial,
      value: [
        {
          queries: [
            {
              qry: 'H1',
              data: null,
              type: CardType.Strongs,
            } as CardItem,
          ],
          title: 'page1',
          id: 'myid1',
        },
        {
          queries: [
            {
              qry: 'H2',
              data: null,
              type: CardType.Strongs,
            } as CardItem,
            {
              qry: 'G1',
              data: null,
              type: CardType.Strongs,
            } as CardItem,
          ],
          title: 'page2',
          id: 'myid2',
        },
      ],
    },
    notes: {
      createdOn: new Date(0).toISOString(),
      type: StorableType.initial,
      value: [],
    },
    savedPagesLoaded: false,
    mainPages: [],
    error: null,
    settings: {
      createdOn: new Date(0).toISOString(),
      type: StorableType.initial,
      value: {
        displaySettings: {
          showStrongsAsModal: false,
          appendCardToBottom: true,
          insertCardNextToItem: true,
          clearSearchAfterQuery: true,
          cardFontSize: 12,
          cardFontFamily: 'PT Serif',
          showVersesOnNewLine: false,
          showVerseNumbers: false,
          showParagraphs: true,
          showParagraphHeadings: true,
          syncCardsAcrossDevices: false,
        },
        pageSettings: {
          mergeStrategy: Overlap.Subset,
        },
        cardIcons: {
          words: 'font_download',
          passage: 'menu_book',
          strongs: 'speaker_notes',
          note: 'text_snippet',
          savedPage: 'saved page',
        },
      },
    },
  } as AppState;

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  //#region  Settings

  it('UPDATE_CARD_MERGE_STRATEGY', () => {
    for (const strategy of [Overlap.None, Overlap.Equal, Overlap.Subset, Overlap.Intersect]) {
      const testState = updateCardMergeStrategyAction(strategy).handle(preState);
      expect(testState.settings.value.pageSettings.mergeStrategy).toEqual(strategy);
    }
  });

  it('UPDATE_SETTINGS', () => {
    const settings = {
      createdOn: new Date(2020, 1, 1, 0, 0, 0, 0).toISOString(),
      type: StorableType.initial,
      value: {
        displaySettings: {
          showStrongsAsModal: true,
          appendCardToBottom: false,
          insertCardNextToItem: false,
          clearSearchAfterQuery: false,
          cardFontSize: 100,
          cardFontFamily: 'Jason',
          showVersesOnNewLine: true,
          showVerseNumbers: true,
          showParagraphs: false,
          showParagraphHeadings: false,
          syncCardsAcrossDevices: true,
        },
        pageSettings: {
          mergeStrategy: Overlap.Subset,
        },
        cardIcons: {
          words: 'font_download',
          passage: 'menu_book',
          strongs: 'speaker_notes',
          note: 'text_snippet',
          savedPage: 'saved page',
        },
      },
    };

    const testState = updateSettingsAction(settings).handle(preState);
    expect(testState.settings).toBe(settings, 'Failed to update the display settings');
  });

  it('UPDATE_CARD_FONT_SIZE', () => {
    const testState = updateCardFontSizeAction(32).handle(preState);
    expect(testState.settings.value.displaySettings.cardFontSize).toBe(32, 'Failed to change card font size to 32');
  });

  it('UPDATE_CARD_FONT_FAMILY', () => {
    const testState = updateCardFontFamilyAction('Jason').handle(preState);
    expect(testState.settings.value.displaySettings.cardFontFamily).toBe(
      'Jason',
      'Failed to change card font family to "Jason"'
    );
  });

  //#endregion

  it('UPDATE_AUTOCOMPLETE', () => {
    const words = ['word1', 'word2', 'word3'];

    const testState = updateAutoCompleteAction(words).handle(preState);
    expect(testState.autocomplete).toEqual(words, 'Failed to update the autocomplete array');
  });

  //#region  Saved Pages

  it('UPDATE_SAVED_PAGES', () => {
    const savedPages = new Storable<readonly SavedPage[]>([
      {
        queries: [
          {
            qry: 'H12',
            data: null,
            type: CardType.Strongs,
          } as CardItem,
        ],
        title: 'testpage',
        id: 'myid',
      },
    ]);

    const testState = updateSavedPagesAction(savedPages).handle(preState);
    expect(testState.savedPages).toBe(savedPages, 'Failed to update the savedPages array');
    expect(testState.savedPages.value.length).toBe(1, 'Updated savedPages is the wrong length');
    expect(testState.savedPages.value[0].queries.length).toBe(
      1,
      'Updated savedPages first object has the wrong number of queries'
    );
  });

  it('UPDATE_SAVED_PAGE', () => {
    const sp: SavedPage = {
      queries: [],
      title: 'test page',
      id: 'myid2',
    };

    const testState = updateSavedPageAction(sp).handle(preState);

    expect(testState.savedPages.value[0].queries.length).toBe(
      1,
      'Updated savedPages first object has the wrong number of queries'
    );
    expect(testState.savedPages.value[0].title).toBe('page1');
    expect(testState.savedPages.value[1].queries.length).toBe(
      0,
      'Updated savedPages first object has the wrong number of queries'
    );
    expect(testState.savedPages.value[1].title).toBe('test page');
  });

  it('REMOVE_SAVED_PAGE', () => {
    const sp = preState.savedPages.value[0];
    const testState = removeSavedPageAction(sp).handle(preState);

    expect(testState.savedPages.value.length).toBe(1, 'Updated savedPages should be 1');
    expect(testState.savedPages.value[0].title).toBe('page2');
  });

  it('UPDATE_CURRENT_PAGE', () => {
    const card1: CardItem = {
      qry: 'H123',
      data: null,
      type: CardType.Strongs,
    };

    const testState = addCardsAction([card1], null).handle(preState);
    expect(testState.currentCards.value[0]).toBe(card1, 'Failed to add first card to empty list');

    const testState2 = updateCurrentPageAction().handle(testState);

    expect(testState2.currentSavedPage.queries.length).toBe(1);
    expect(preState.currentSavedPage.queries.length).toBe(0);
  });

  it('SAVE_PAGE', () => {
    const card1: CardItem = {
      qry: 'jn 3:16',
      data: null,
      type: CardType.Passage,
    };

    const preState2 = addCardsAction([card1], null).handle(preState);

    const testState = savePageAction('my saved page').handle(preState2);

    expect(testState.savedPages.value[2].queries.length).toBe(
      1,
      'Updated savedPages first object has the wrong number of queries'
    );
    expect(testState.savedPages.value[2].title).toBe('my saved page');
  });

  it('MOVE_SAVED_PAGE_CARD', () => {
    const page = preState.savedPages.value[1];

    const testState = moveSavedPageCardAction(page, 1, 0).handle(preState);
    expect(testState.savedPages.value[1].queries[0].qry).toBe('G1', 'Failed to move card in saved page');
  });

  it('ADD_CARD_TO_SAVED_PAGE', () => {
    const card1: CardItem = {
      qry: 'jn 3:16',
      data: null,
      type: CardType.Passage,
    };
    const testState = addCardToSavedPageAction(card1, 'myid1').handle(preState);
    expect(testState.savedPages.value[0].queries[1].qry).toBe('jn 3:16', 'Failed to add card to saved page');
  });

  //#endregion

  //#region  Cards

  it('ADD_CARD', () => {
    const card1: CardItem = {
      qry: 'H123',
      data: null,
      type: CardType.Strongs,
    };

    const testState = addCardsAction([card1], null).handle(preState);
    expect(testState.currentCards.value[0]).toBe(card1, 'Failed to add first card to empty list');

    const card2: CardItem = {
      qry: 'G123',
      data: null,
      type: CardType.Strongs,
    };

    const testState2 = addCardsAction([card2], null).handle(testState);
    expect(testState2.currentCards.value.length).toBe(2, 'Failed to add second card to list with 1 item');
    expect(testState2.currentCards.value[1]).toBe(card2);

    const card3: CardItem = {
      qry: 'G1234',
      data: null,
      type: CardType.Strongs,
    };

    // append to top, insert next to card

    let setState = updateSettingsAction({
      createdOn: new Date(2020, 1, 1, 0, 0, 0, 0).toISOString(),
      type: StorableType.initial,
      value: {
        ...testState2.settings.value,
        displaySettings: {
          showStrongsAsModal: true,
          appendCardToBottom: false,
          insertCardNextToItem: true,
          clearSearchAfterQuery: false,
          cardFontSize: 100,
          cardFontFamily: 'Jason',
          showVersesOnNewLine: true,
          showVerseNumbers: true,
          showParagraphs: false,
          showParagraphHeadings: false,
          syncCardsAcrossDevices: true,
        },
      },
    }).handle(testState2);

    const testState3 = addCardsAction([card3], card2).handle(setState);
    expect(testState3.currentCards.value.length).toBe(3, 'Failed to add third card');
    expect(testState3.currentCards.value[1]).toBe(card3, 'Failed to insert card above the second card');

    // append to bottom, insert next to card

    setState = updateSettingsAction({
      createdOn: new Date(2020, 1, 1, 0, 0, 0, 0).toISOString(),
      type: StorableType.initial,
      value: {
        ...testState2.settings.value,
        displaySettings: {
          showStrongsAsModal: true,
          appendCardToBottom: true,
          insertCardNextToItem: true,
          clearSearchAfterQuery: false,
          cardFontSize: 100,
          cardFontFamily: 'Jason',
          showVersesOnNewLine: true,
          showVerseNumbers: true,
          showParagraphs: false,
          showParagraphHeadings: false,
          syncCardsAcrossDevices: true,
        },
      },
    }).handle(testState2);

    const testState4 = addCardsAction([card3], card1).handle(setState);
    expect(testState4.currentCards.value.length).toBe(3, 'Failed to add third card');
    expect(testState4.currentCards.value[1]).toBe(card3, 'Failed to insert card below the first card');

    // append to bottom, do not insert next to card

    setState = updateSettingsAction({
      createdOn: new Date(2020, 1, 1, 0, 0, 0, 0).toISOString(),
      type: StorableType.initial,
      value: {
        ...testState2.settings.value,
        displaySettings: {
          showStrongsAsModal: true,
          appendCardToBottom: true,
          insertCardNextToItem: false,
          clearSearchAfterQuery: false,
          cardFontSize: 100,
          cardFontFamily: 'Jason',
          showVersesOnNewLine: true,
          showVerseNumbers: true,
          showParagraphs: false,
          showParagraphHeadings: false,
          syncCardsAcrossDevices: true,
        },
      },
    }).handle(testState2);

    const testState5 = addCardsAction([card3], card1).handle(setState);
    expect(testState5.currentCards.value.length).toBe(3, 'Failed to add third card');
    expect(testState5.currentCards.value[2]).toBe(card3, 'Failed to insert card at end of the list');

    // append to top, do not insert next to card

    setState = updateSettingsAction({
      createdOn: new Date(2020, 1, 1, 0, 0, 0, 0).toISOString(),
      type: StorableType.initial,
      value: {
        ...testState2.settings.value,
        displaySettings: {
          showStrongsAsModal: true,
          appendCardToBottom: false,
          insertCardNextToItem: false,
          clearSearchAfterQuery: false,
          cardFontSize: 100,
          cardFontFamily: 'Jason',
          showVersesOnNewLine: true,
          showVerseNumbers: true,
          showParagraphs: false,
          showParagraphHeadings: false,
          syncCardsAcrossDevices: true,
        },
      },
    }).handle(testState2);

    const testState6 = addCardsAction([card3], card1).handle(setState);
    expect(testState6.currentCards.value.length).toBe(3, 'Failed to add third card');
    expect(testState6.currentCards.value[0]).toBe(card3, 'Failed to insert card at start of the list');
  });

  it('UPDATE_CARD', () => {
    const oldCard: CardItem = {
      qry: 'G123',
      data: null,
      type: CardType.Strongs,
    };

    const preState1 = addCardsAction([oldCard], null).handle(preState);

    const newCard: CardItem = {
      qry: 'H88',
      data: null,
      type: CardType.Strongs,
    };

    const testState = updateCardAction(newCard, oldCard).handle(preState1);

    expect(testState.currentCards.value[0].qry).toBe('H88', 'Should update the card');
    expect(testState.cardCache[getCardCacheKey(newCard)].qry).toBe('H88', 'Should exist in card cache');
    expect(testState.cardCache[getCardCacheKey(oldCard)]).toBeUndefined(
      'Should be undefined, having been removed from card cache'
    );
  });

  it('REMOVE_CARD', () => {
    const card: CardItem = {
      qry: 'G123',
      data: null,
      type: CardType.Strongs,
    };

    const preState1 = addCardsAction([card], null).handle(preState);

    const testState = removeCardAction(card).handle(preState1);

    expect(preState1.currentCards.value.length).toBe(
      1,
      'Should have added the card in preparation for removing the card'
    );
    expect(testState.currentCards.value.length).toBe(0, 'Should have removed the card');
    expect(testState.cardCache[getCardCacheKey(card)]).toBeUndefined(
      'Should be undefined, having been removed from card cache'
    );
  });

  it('MOVE_CARD', () => {
    const card1: CardItem = {
      qry: 'G123',
      data: null,
      type: CardType.Strongs,
    };

    const preState1 = addCardsAction([card1], null).handle(preState);

    const card2: CardItem = {
      qry: 'H88',
      data: null,
      type: CardType.Strongs,
    };

    const preState2 = addCardsAction([card2], null).handle(preState1);
    expect(preState2.currentCards.value.length).toBe(2, 'Should have two cards');
    expect(preState2.currentCards.value[0].qry).toBe('G123');
    expect(preState2.currentCards.value[1].qry).toBe('H88');

    const testState1 = moveCardAction(card2, MoveDirection.Up).handle(preState2);
    expect(testState1.currentCards.value[0].qry).toBe('H88');
    expect(testState1.currentCards.value[1].qry).toBe('G123');
    const testState4 = moveCardAction(card2, MoveDirection.Up).handle(preState2);
    expect(testState4.currentCards.value[0].qry).toBe('H88');
    expect(testState4.currentCards.value[1].qry).toBe('G123');

    const testState2 = moveCardAction(card1, MoveDirection.Down).handle(preState2);
    expect(testState2.currentCards.value[0].qry).toBe('H88');
    expect(testState2.currentCards.value[1].qry).toBe('G123');
    const testState3 = moveCardAction(card1, MoveDirection.Down).handle(preState2);
    expect(testState3.currentCards.value[0].qry).toBe('H88');
    expect(testState3.currentCards.value[1].qry).toBe('G123');
  });

  //#endregion

  it('SET_USER', () => {
    const user: User = {
      uid: '1234',
      displayName: 'Jason',
      email: 'jason@walljm.com',
      providerId: 'asdfasf',
    };

    const testState = setUserAction(user).handle(preState);

    expect(testState.user).toBe(user, 'Should set the user');
  });

  //#region Notes

  it('NOTES: Save, Get, ', () => {
    const note1: NoteItem = {
      id: '123456789',
      title: 'Test note 1',
      content: 'some content',
      xref: 'jn 3:16',
    };
    const note2: NoteItem = {
      id: '1234567890',
      title: 'Test note 2',
      content: 'some content',
      xref: 'jn 3:16',
    };

    const preState1 = saveNoteAction(note1).handle(preState);
    const preState2 = saveNoteAction(note2).handle(preState1);
    expect(preState2.notes.value.length).toBe(2, 'Should have two notes');

    const preState3 = getNotesAction('123456789', null).handle(preState2);
    expect(preState3.currentCards.value.length).toBe(1, 'Should have added the note card');

    const preState4 = getNotesAction('1234567890', null).handle(preState3);
    expect(preState4.currentCards.value.length).toBe(2, 'Should have added the note card');
  });

  it('NOTES: Update Notes, Delete Notes, Find Notes, ', () => {
    const note1: NoteItem = {
      id: '123456789',
      title: 'Test note 1',
      content: 'some content',
      xref: 'jn 3:16',
    };
    const note2: NoteItem = {
      id: '1234567890',
      title: 'Test note 2',
      content: 'some content',
      xref: 'jn 3:16',
    };

    const preState1 = updateNotesAction(new Storable([note1, note2])).handle(preState);
    expect(preState1.notes.value.length).toBe(2, 'Should have added the notes');

    const preState2 = findNotesAction('note', null).handle(preState1);
    expect(preState2.currentCards.value.length).toBe(2, 'Should have found two notes card');

    const preState3 = deleteNoteAction(note1).handle(preState2);
    expect(preState3.currentCards.value.length).toBe(1, 'Should have deleted the note card');
    expect(preState3.notes.value.length).toBe(1, 'Should have added the notes');
  });

  //#endregion
});


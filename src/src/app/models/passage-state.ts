import { StrongsDictionary } from './strongs-state';

export interface BiblePassageResult {
  readonly cs: readonly BibleParagraphPassage[];
  readonly dict: StrongsDictionary;
  readonly ref: string;
}

export interface BibleParagraph {
  readonly p: Paragraph;
  readonly vss: readonly BibleVerse[];
}

export interface BibleParagraphPassage {
  readonly ch: number;
  readonly paras: readonly BibleParagraph[];
}

export interface BiblePassage {
  readonly ch: number;
  readonly vss: readonly BibleVerse[];
}

export interface BibleVerse {
  readonly v: number;
  readonly w: readonly [
    {
      readonly t: string;
      readonly s: string;
    }
  ];
}

export interface Paragraph {
  readonly h: string;
  readonly p: number;
}

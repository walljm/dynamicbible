import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/compat/database';
import { DataSnapshot } from '@angular/fire/compat/database/interfaces';
import { StorageMap } from '@ngx-pwa/local-storage';
import { createSelector } from 'reselect';
import { lastValueFrom } from 'rxjs';

import { isNullOrUndefined } from '../common/helpers';
import { IStorable, StorableType, UserVersion } from '../common/storable';
import { SubscriberBase } from '../common/subscriber-base';
import { AppState,Settings, User } from '../models/app-state';
import { DataReference } from '../models/card-state';
import { NoteItem } from '../models/note-state';
import { SavedPage } from '../models/page-state';
import { AppService } from './app.service';
import { MigrationVersion0to1 } from './migration0to1.service';

/**
 * This class handles all the storage needs of the application.  It handles both
 *   local and remote storage and syncing between the two.
 *
 * There are three components to the system that are important.
 *
 * 1)  Listening for changes to the local data store (see the Local //#region)
 *
 *     When data is written to the local data store, that data needs to be sent to
 *     the application state so the running storage reflects the local storage.
 *
 *     The local storage is initialized in the app.component.ts constructor.
 *
 * 2)  Listening for changes to the remote data store (see the Remote //#region)
 *
 *     When data is written to the remote data store, that data needs to be sent to
 *     the application state so the running storage reflects the remote storage.
 *
 *     The remote storage is initialized in the app.component.ts constructor once a user
 *     is available indicating that a connection has been established with the remote data
 *     store.
 *
 * 3)  Listening for changes from the application state.
 *
 *     When something happens in the application state, that state needs to be stored
 *     locally so the changes aren't lost, and remotely so the changes can be shared
 *     and persisten across devices and different installs.
 *
 *     The listeners to the app state are initialized after local state is retrieved at startup
 */
@Injectable({
  providedIn: 'root',
})
export class StorageService extends SubscriberBase {
  private version = 1;
  private storeVersion = `v${this.version}/`;
  private userVersionPath = `userVersion`;
  private userVersionRemoteObject: AngularFireObject<UserVersion>;

  private settingsState$ = this.appService.select((state) => state.settings);
  private settingsPath = `${this.storeVersion}settings`;
  private settingsRemoteObject: AngularFireObject<IStorable<Settings>>;

  private savedPagesState$ = this.appService.select((state) => state.savedPages);
  private savedPagesPath = `${this.storeVersion}savedPages`;
  private savedPagesRemoteObject: AngularFireObject<IStorable<readonly SavedPage[]>>;

  private noteItemsState$ = this.appService.select((state) => state.notes);
  private noteItemsPath = `${this.storeVersion}noteItems`;
  private noteItemsRemoteObject: AngularFireObject<IStorable<readonly NoteItem[]>>;

  private cardsPath = `${this.storeVersion}currentCards`;
  private cardsRemoteObject: AngularFireObject<IStorable<readonly DataReference[]>>;

  private syncCurrentItems = false;
  private syncCurrentItems$ = this.appService.select(
    createSelector(
      (state: AppState) => state.settings.value.displaySettings.syncCardsAcrossDevices,
      (state: AppState) => state.currentCards,
      (sync, cards) => ({
        syncCardsAcrossDevices: sync,
        currentCards: {
          type: cards.type,
          createdOn: cards.createdOn,
          value: cards.value.map((o) => {
            return {
              qry: o.qry,
              type: o.type,
            } as DataReference;
          }),
        } as IStorable<DataReference[]>,
      })
    )
  );

  constructor(
    private local: StorageMap,
    private remote: AngularFireDatabase,
    private appService: AppService,
    private v0to1: MigrationVersion0to1
  ) {
    super();
  }

  initRemote(user: User) {
    // do the necessary migration steps.
    this.handleMigration(user);

    // initialize the remote store monitoring.  This is where we listen for changes on the remote location.
    this.settingsRemoteObject = this.remote.object<IStorable<Settings>>(`/${this.settingsPath}/${user.uid}`);
    this.addSubscription(
      this.settingsRemoteObject
        .valueChanges() // when the value changes
        .subscribe((remoteData) => {
          if (!isNullOrUndefined(remoteData) && !isNullOrUndefined(remoteData.value)) {
            // update the app state with remote data if it isn't null
            // console.log('Data recieved from remote store', remoteData);
            this.appService.updateSettings(remoteData);
          }
        })
    );

    this.savedPagesRemoteObject = this.remote.object<IStorable<SavedPage[]>>(`/${this.savedPagesPath}/${user.uid}`);
    this.addSubscription(
      this.savedPagesRemoteObject
        .valueChanges() // when the value changes
        .subscribe((remoteData) => {
          if (!isNullOrUndefined(remoteData)) {
            // update the app state with remote data if it isn't null
            // console.log('Data recieved from remote store', remoteData);
            this.appService.updateSavedPages(remoteData);
          }
        })
    );

    this.noteItemsRemoteObject = this.remote.object<IStorable<NoteItem[]>>(`/${this.noteItemsPath}/${user.uid}`);
    this.addSubscription(
      this.noteItemsRemoteObject
        .valueChanges() // when the value changes
        .subscribe((remoteData) => {
          if (!isNullOrUndefined(remoteData)) {
            // update the app state with remote data if it isn't null
            // console.log('Data recieved from remote store', remoteData);
            this.appService.updateNotes(remoteData);
          }
        })
    );

    // handle cards differently, because there is a setting that changes the behavior of how you handle
    //  new data from the remote.
    this.cardsRemoteObject = this.remote.object<IStorable<DataReference[]>>(`/${this.cardsPath}/${user.uid}`);
    this.addSubscription(
      this.cardsRemoteObject
        .valueChanges() // when the value changes
        .subscribe((remoteData) => {
          if (!this.syncCurrentItems) {
            return;
          }

          if (!isNullOrUndefined(remoteData)) {
            // update the app state with remote data, but only if syncing is turned on.
            this.appService.updateCards(remoteData);
          }
        })
    );
  }

  //#region Local

  /*
    initialize the local stores
  */
  async initLocal() {
    this.getFromLocal<Settings>(this.settingsPath, (data) => {
      this.appService.updateSettings(data);
    });
    this.getFromLocal<SavedPage[]>(this.savedPagesPath, (data) => {
      this.appService.updateSavedPages(data);
    });
    this.getFromLocal<NoteItem[]>(this.noteItemsPath, (data) => {
      this.appService.updateNotes(data);
    });
    this.getFromLocal<DataReference[]>(this.cardsPath, (data) => {
      this.appService.updateCards(data);
    });

    // we start listening after we get the local storage, to avoid overwriting local storage with the initial app state.
    this.addAppStateListeners();
  }

  private async getFromLocal<T>(path: string, action: (storable: IStorable<T>) => void) {
    const hasStorable = await lastValueFrom(this.local.has(path));
    if (hasStorable) {
      const localData = (await lastValueFrom(this.local.get(path))) as IStorable<T>;
      // console.log('Data recieved from local store', localData);
      action(localData);
    }
  }

  private addAppStateListeners() {
    // handle remote and local storage
    //  when the specific items change in the state,
    //  store them locally and remotely, if you're logged in.
    this.addSubscription(
      this.settingsState$.subscribe((data) => {
        if (!data || data.type === StorableType.initial) {
          return;
        }

        // console.log('Data saved to local store', data);
        // update local
        this.addSubscription(
          this.local.set(this.settingsPath, data).subscribe({
            next: () => {},
            complete: () => {},
            error: () => {
              this.appService.dispatchError(`Something went wrong and the Settings weren't saved. :(`);
            },
          })
        );

        // update remote
        if (this.settingsRemoteObject) {
          // console.log('Data sent to remote store', data);
          this.settingsRemoteObject.set(data);
        }
      })
    );

    this.addSubscription(
      this.savedPagesState$.subscribe((data) => {
        if (!data || data.type === StorableType.initial) {
          return;
        }

        // console.log('Data saved to local store', data);
        // update local
        this.addSubscription(
          this.local.set(this.savedPagesPath, data).subscribe({
            next: () => {},
            complete: () => {},
            error: () => {
              this.appService.dispatchError(`Something went wrong and the Page wasn't saved. :(`);
            },
          })
        );

        // update remote
        if (this.savedPagesRemoteObject) {
          // console.log('Data sent to remote store', data);
          this.savedPagesRemoteObject.set(data);
        }
      })
    );

    this.addSubscription(
      this.noteItemsState$.subscribe((data) => {
        if (!data || data.type === StorableType.initial) {
          return;
        }

        // console.log('Data saved to local store', data);
        // update local
        this.addSubscription(
          this.local.set(this.noteItemsPath, data).subscribe({
            next: () => {},
            complete: () => {},
            error: () => {
              this.appService.dispatchError(`Something went wrong and the Note wasn't saved. :(`);
            },
          })
        );

        // update remote
        if (this.noteItemsRemoteObject) {
          // console.log('Data sent to remote store', data);
          this.noteItemsRemoteObject.set(data);
        }
      })
    );

    // whenever the user setting or the current cards change, save the cards
    //   and if syncing is turned on, send them to the remote store.
    this.addSubscription(
      this.syncCurrentItems$.subscribe((v) => {
        // set a local sync variable, so that the remote events know whether to
        //   accept remote state changes.
        this.syncCurrentItems = v.syncCardsAcrossDevices;

        if (!v.currentCards || v.currentCards.type === StorableType.initial) {
          return;
        }

        // update local
        this.addSubscription(
          this.local.set(this.cardsPath, v.currentCards).subscribe({
            next: () => {},
            complete: () => {},
            error: () => {
              this.appService.dispatchError(`Something went wrong and the current cards weren't saved. :(`);
            },
          })
        );

        // since you updated the local variable above, this remote update will
        //  get picked up by the remote subscription below.
        if (this.cardsRemoteObject && v.syncCardsAcrossDevices) {
          this.cardsRemoteObject.set(v.currentCards);
        }
      })
    );
  }

  //#endregion

  //#region Migrations

  private handleMigration(user: User) {
    this.userVersionRemoteObject = this.remote.object<UserVersion>(`/${this.userVersionPath}/${user.uid}`);
    this.userVersionRemoteObject.query.once('value').then((data: DataSnapshot) => {
      const v: UserVersion = data.val();
      if (v === null || v === undefined || v.version < this.version) {
        // perform the migration.
        this.v0to1.migrate(user, this);

        // update the version so the migration doesn't happen again.
        this.userVersionRemoteObject.set({ version: 1 });
      }
    });
  }

  public setRemoteData(
    settings: IStorable<Settings>,
    savedPages: IStorable<SavedPage[]>,
    notes: IStorable<NoteItem[]>,
    cards: IStorable<DataReference[]>
  ) {
    this.settingsRemoteObject.set(settings);
    this.savedPagesRemoteObject.set(savedPages);
    this.noteItemsRemoteObject.set(notes);
    this.cardsRemoteObject.set(cards);
  }

  //#endregion
}

import { BreakpointObserver,Breakpoints } from '@angular/cdk/layout';
import { AfterViewInit, ChangeDetectorRef,Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

import { SubscriberBase } from './common/subscriber-base';
import { HelpModalComponent } from './components/help-modal/help-modal.component';
import { AppService } from './services/app.service';
import { NavService } from './services/nav.service';
import { StorageService } from './services/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent extends SubscriberBase implements AfterViewInit {
  savedPages$ = this.appService.select((state) => (state.savedPages === null ? null : state.savedPages.value));
  fontSize$ = this.appService.select((state) => state.settings.value.displaySettings.cardFontSize + 'pt');
  cardFont$ = this.appService.select((state) => state.settings.value.displaySettings.cardFontFamily);
  error$ = this.appService.select((state) => state.error);

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset).pipe(
    map((result) => result.matches),
    shareReplay()
  );

  disableAnimation = false;

  @ViewChild('drawer') public sidenav: MatSidenav;
  @ViewChild('settings') public settings: MatSidenav;

  constructor(
    private appService: AppService,
    private storageService: StorageService,
    private breakpointObserver: BreakpointObserver,
    private snackBar: MatSnackBar,
    private cd: ChangeDetectorRef,
    public navService: NavService,
    public dialog: MatDialog
  ) {
    super();

    this.storageService.initLocal();

    this.addSubscription(
      this.error$.subscribe((err) => {
        if (err) {
          this.snackBar.open(`Oh no! ${err.msg}`, 'Dismiss  Error');
          setTimeout(() => {
            this.snackBar.dismiss(); // dismiss if its still there after 25 seconds.
          }, 25 * 1000);
        }
      })
    );

    //#region  Handle recieving updates from firebase

    this.addSubscription(
      // when the user object changes, respond
      this.appService
        // this should trigger only once, when the user logs in.
        .select((state) => state.user)
        .subscribe((user) => {
          if (!user) {
            return; // if the user is null, avoid this.
          }
          this.storageService.initRemote(user);
        })
    );

    //#endregion

    //#region handle font settings

    this.addSubscription(
      this.fontSize$.subscribe((size) => {
        if (size) {
          document.documentElement.style.setProperty('--card-font-size', size);
        }
      })
    );
    this.addSubscription(
      this.cardFont$.subscribe((family) => {
        if (family) {
          document.documentElement.style.setProperty('--card-font-family', family);
        }
      })
    );

    //#endregion
  }

  showHelp() {
    this.dialog.open(HelpModalComponent, {
      autoFocus: 'content',
    });
  }

  ngAfterViewInit(): void {
    this.navService.afterViewInit(this.sidenav, this.settings);
  }
}

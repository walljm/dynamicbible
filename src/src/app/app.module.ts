import { ClipboardModule } from '@angular/cdk/clipboard';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { APP_ID, NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AngularFireDatabaseModule } from '@angular/fire/compat/database';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatBadgeModule } from '@angular/material/badge';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatChipsModule } from '@angular/material/chips';
import { MatNativeDateModule, MatRippleModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatSliderModule } from '@angular/material/slider';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MarkdownModule,MARKED_OPTIONS } from 'ngx-markdown';
import { MarkedOptions, MarkedRenderer } from 'ngx-markdown';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AddToPageModalComponent } from './components/add-to-page-modal/add-to-page-modal.component';
import { CardEditModalComponent } from './components/card-edit-modal/card-edit-modal.component';
import { HelpModalComponent } from './components/help-modal/help-modal.component';
import { NoteEditModalComponent } from './components/note/edit-modal/note-edit-modal.component';
import { NoteCardComponent } from './components/note/note-card.component';
import { OkCancelModalComponent } from './components/ok-cancel-modal/ok-cancel-modal.component';
import { PageEditModalComponent } from './components/page-edit-modal/page-edit-modal.component';
import { PassageCardComponent } from './components/passage/passage-card.component';
import { SavedPageCardComponent } from './components/saved-page-card/saved-page-card.component';
import { SettingsComponent } from './components/settings/settings.component';
import { StrongsCardComponent } from './components/strongs/card/strongs-card.component';
import { StrongsModalComponent } from './components/strongs/modal/strongs-modal.component';
import { StrongsComponent } from './components/strongs/strongs.component';
import { VersePickerModalComponent } from './components/verse-picker-modal/verse-picker-modal.component';
import { WordsCardComponent } from './components/words/words-card.component';
import { FirebaseConfig } from './constants';
import { NotesAdminPageComponent } from './pages/notes-admin/notes-admin.page';
import { SavedPagesAdminPageComponent } from './pages/saved-pages-admin/saved-pages-admin.page';
import { SearchPageComponent } from './pages/search/search.page';

// function that returns `MarkedOptions` with renderer override
export function markedOptionsFactory(): MarkedOptions {
  const renderer = new MarkedRenderer();

  renderer.heading = (text: string, level: number) => {
    return `<h${level + 1}>${text}</h${level + 1}>\n`;
  };

  return {
    renderer: renderer,
    gfm: true,
    breaks: false,
    pedantic: false,
  };
}

@NgModule({ declarations: [
        AppComponent,
        NotesAdminPageComponent,
        SavedPagesAdminPageComponent,
        SavedPageCardComponent,
        HelpModalComponent,
        SearchPageComponent,
        PassageCardComponent,
        StrongsComponent,
        StrongsCardComponent,
        StrongsModalComponent,
        WordsCardComponent,
        NoteCardComponent,
        PageEditModalComponent,
        NoteEditModalComponent,
        AddToPageModalComponent,
        VersePickerModalComponent,
        SettingsComponent,
        OkCancelModalComponent,
        CardEditModalComponent,
    ],
    bootstrap: [AppComponent], imports: [BrowserModule,
        ReactiveFormsModule,
        FormsModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MarkdownModule.forRoot({
            markedOptions: {
                provide: MARKED_OPTIONS,
                useFactory: markedOptionsFactory,
            },
        }),
        AngularFireModule.initializeApp(FirebaseConfig),
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        DragDropModule,
        MatSidenavModule,
        MatChipsModule,
        MatToolbarModule,
        MatIconModule,
        MatAutocompleteModule,
        MatBadgeModule,
        MatBottomSheetModule,
        MatButtonModule,
        MatButtonToggleModule,
        MatCardModule,
        MatCheckboxModule,
        MatStepperModule,
        MatDialogModule,
        MatDividerModule,
        MatExpansionModule,
        MatInputModule,
        MatListModule,
        MatMenuModule,
        MatNativeDateModule,
        MatRadioModule,
        MatRippleModule,
        MatSelectModule,
        MatSliderModule,
        MatSlideToggleModule,
        MatSnackBarModule,
        MatTooltipModule,
        MatFormFieldModule,
        MatTabsModule,
        ClipboardModule], providers: [{ provide: APP_ID, useValue: 'ng-cli-universal' }, provideHttpClient(withInterceptorsFromDi())] })
export class AppModule {}

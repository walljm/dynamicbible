﻿using System.Collections.Generic;

namespace DynamicBibleUtility
{
    public class Index : List<IndexItem>
    {
        public IndexItem GetItem(string word)
        {
            return this.Find(i => i.w == word);
        }
    }

    public class IndexItem
    {
        public string w = "";
        public List<string> r = new List<string>();
    }
}
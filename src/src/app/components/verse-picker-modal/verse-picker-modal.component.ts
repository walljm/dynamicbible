import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { BibleReference,Book } from 'src/app/common/bible-reference';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-verse-picker',
  templateUrl: 'verse-picker-modal.component.html',
  styleUrls: ['./verse-picker-modal.component.scss'],
})
export class VersePickerModalComponent {
  otBooks: Array<Book>;
  ntBooks: Array<Book>;
  hasBook = false;
  book: Book;

  constructor(public appService: AppService, public dialogRef: MatDialogRef<VersePickerModalComponent>) {
    this.hasBook = false;
    this.otBooks = BibleReference.Books.slice(1, 40);
    this.ntBooks = BibleReference.Books.slice(40, 67);
  }

  toBooks() {
    this.hasBook = false;
    this.book = null;
  }

  setBook(book: Book) {
    this.hasBook = true;
    this.book = book;
  }

  setChapter(chapter: number) {
    // close the control, trigger the passage event.
    this.appService.getPassage(new BibleReference(this.book.name + ' ' + chapter));
    this.dialogRef.close();
  }
}

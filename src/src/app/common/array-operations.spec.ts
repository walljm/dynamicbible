import { moveItem,moveItemUpOrDown } from './array-operations';
import { MoveDirection } from './move-direction';

describe('Array Movement', () => {
  it('Should move an item up', () => {
    const a1: number[] = [1, 2, 3, 4];
    const a2 = moveItemUpOrDown(a1, 2, MoveDirection.Up);

    expect(a2[0]).toBe(2);
  });

  it('Should not move an item up if at start of array', () => {
    const a1: number[] = [1, 2, 3, 4];
    const a2 = moveItemUpOrDown(a1, 1, MoveDirection.Up);

    expect(a2).toBe(a1);
  });

  it('Should move an item down', () => {
    const a1: number[] = [1, 2, 3, 4];
    const a2 = moveItemUpOrDown(a1, 2, MoveDirection.Down);

    expect(a2[2]).toBe(2);
  });

  it('Should not move an item down if at the end of a list', () => {
    const a1: number[] = [1, 2, 3, 4];
    const a2 = moveItemUpOrDown(a1, 4, MoveDirection.Down);

    expect(a2).toBe(a1);
  });

  it('Should move an item from one index to another', () => {
    const a1: number[] = [1, 2, 3, 4];
    const a2 = moveItem(a1, 1, 3);

    expect(a2[3]).toBe(2);
  });
});

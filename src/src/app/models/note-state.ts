export type NoteItem = {
  // The Note id
  readonly id: string;
  // A note title.
  readonly title: string;
  // An optional cross reference to a bible passage.
  readonly xref: string | null;
  // The content of the note styled as markdown.
  readonly content: string;
};

import { NoteItem } from './note-state';
import { BiblePassageResult } from './passage-state';
import { StrongsResult } from './strongs-state';
import { WordLookupResult } from './words-state';

export type CardData = BiblePassageResult | StrongsResult | WordLookupResult | NoteItem;

export enum CardType {
  Passage,
  Note,
  Word,
  Strongs,
  Error,
}

export interface CardIcons {
  readonly words: string;
  readonly passage: string;
  readonly strongs: string;
  readonly note: string;
  readonly savedPage: string;
}

export interface DataReference {
  readonly qry: string;
  readonly type: CardType;
}

export interface CardItem extends DataReference {
  readonly data: CardData;
}

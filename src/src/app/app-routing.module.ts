import { NgModule } from '@angular/core';
import { RouterModule,Routes } from '@angular/router';

import { NotesAdminPageComponent } from './pages/notes-admin/notes-admin.page';
import { SavedPagesAdminPageComponent } from './pages/saved-pages-admin/saved-pages-admin.page';
import { SearchPageComponent } from './pages/search/search.page';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'search',
  },
  {
    path: 'search',
    pathMatch: 'prefix',
    component: SearchPageComponent,
  },
  {
    path: 'search/:term',
    pathMatch: 'prefix',
    component: SearchPageComponent,
  },
  {
    path: 'page/:id',
    pathMatch: 'prefix',
    component: SearchPageComponent,
  },
  {
    path: 'saved/admin',
    pathMatch: 'prefix',
    component: SavedPagesAdminPageComponent,
  },
  {
    path: 'notes/admin',
    pathMatch: 'prefix',
    component: NotesAdminPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}

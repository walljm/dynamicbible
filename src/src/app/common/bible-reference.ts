// This code was written by Jeremy and Jason Wall.
// Feel free to use, and if you can, include a link back to www.walljm.com
// Jason@walljm.com // www.walljm.com
// Jeremy@marzhillstudios.com // jeremy.marzhillstudios.com

export class BibleReference {
  static Books: Array<Book> = [
    {
      name: 'Unknown',
      abbreviation: 'Unk',
      longName: 'Unknown',
      bookNumber: 0,
      lastChapter: 0,
      chapters: [0],
    },
    {
      name: 'Genesis',
      abbreviation: 'Gen',
      longName: 'Book of Genesis',
      bookNumber: 1,
      lastChapter: 66,
      chapters: [0, 31, 25, 24, 26, 32, 22, 24, 22, 29, 32, 32, 20, 18, 24, 21, 16, 27, 33, 38, 18, 34, 24, 20, 67, 34, 35, 46, 22, 35, 43, 55, 32, 20, 31, 29, 43, 36, 30, 23, 23, 57, 38, 34, 34, 28, 34, 31, 22, 33, 26],
    },
    {
      bookNumber: 2,
      abbreviation: 'Exo',
      name: 'Exodus',
      longName: 'Book of Exodus',
      lastChapter: 40,
      chapters: [0, 22, 25, 22, 31, 23, 30, 25, 32, 35, 29, 10, 51, 22, 31, 27, 36, 16, 27, 25, 26, 36, 31, 33, 18, 40, 37, 21, 43, 46, 38, 18, 35, 23, 35, 35, 38, 29, 31, 43, 38],
    },
    {
      bookNumber: 3,
      name: 'Leviticus',
      abbreviation: 'Lev',
      longName: 'Book of Leviticus',
      lastChapter: 27,
      chapters: [0, 17, 16, 17, 35, 19, 30, 38, 36, 24, 20, 47, 8, 59, 57, 33, 34, 16, 30, 37, 27, 24, 33, 44, 23, 55, 46, 34],
    },
    {
      bookNumber: 4,
      name: 'Numbers',
      abbreviation: 'Num',
      longName: 'Book of Numbers',
      lastChapter: 36,
      chapters: [0, 54, 34, 51, 49, 31, 27, 89, 26, 23, 36, 35, 16, 33, 45, 41, 50, 13, 32, 22, 29, 35, 41, 30, 25, 18, 65, 23, 31, 40, 16, 54, 42, 56, 29, 34, 13],
    },
    {
      bookNumber: 5,
      name: 'Deuteronomy',
      abbreviation: 'Deut',
      longName: 'Book of Deuteronomy',
      lastChapter: 34,
      chapters: [0, 46, 37, 29, 49, 33, 25, 26, 20, 29, 22, 32, 32, 18, 29, 23, 22, 20, 22, 21, 20, 23, 30, 25, 22, 19, 19, 26, 68, 29, 20, 30, 52, 29, 12],
    },
    {
      bookNumber: 6,
      name: 'Joshua',
      abbreviation: 'Josh',
      longName: 'Book of Joshua',
      lastChapter: 24,
      chapters: [0, 18, 24, 17, 24, 15, 27, 26, 35, 27, 43, 23, 24, 33, 15, 63, 10, 18, 28, 51, 9, 45, 34, 16, 33],
    },
    {
      bookNumber: 7,
      name: 'Judges',
      abbreviation: 'Jud',
      longName: 'Book of Judges',
      lastChapter: 21,
      chapters: [0, 36, 23, 31, 24, 31, 40, 25, 35, 57, 18, 40, 15, 25, 20, 20, 31, 13, 31, 30, 48, 25],
    },
    {
      bookNumber: 8,
      name: 'Ruth',
      abbreviation: 'Ruth',
      longName: 'Book of Ruth',
      lastChapter: 4,
      chapters: [0, 22, 23, 18, 22],
    },
    {
      bookNumber: 9,
      name: '1 Samuel',
      abbreviation: '1 Sam',
      longName: 'First Book of Samuel',
      lastChapter: 31,
      chapters: [0, 28, 36, 21, 22, 12, 21, 17, 22, 27, 27, 15, 25, 23, 52, 35, 23, 58, 30, 24, 42, 15, 23, 29, 22, 44, 25, 12, 25, 11, 31, 13],
    },
    {
      bookNumber: 10,
      name: '2 Samuel',
      abbreviation: '2 Sam',
      longName: 'Second Book of Samuel',
      lastChapter: 24,
      chapters: [0, 27, 32, 39, 12, 25, 23, 29, 18, 13, 19, 27, 31, 39, 33, 37, 23, 29, 33, 43, 26, 22, 51, 39, 25],
    },
    {
      bookNumber: 11,
      name: '1 Kings',
      abbreviation: '1 Kng',
      longName: 'First Book of Kings',
      lastChapter: 22,
      chapters: [0, 53, 46, 28, 34, 18, 38, 51, 66, 28, 29, 43, 33, 34, 31, 34, 34, 24, 46, 21, 43, 29, 53],
    },
    {
      bookNumber: 12,
      name: '2 Kings',
      abbreviation: '2 Kng',
      longName: 'Second Book of Kings',
      lastChapter: 25,
      chapters: [0, 18, 25, 27, 44, 27, 33, 20, 29, 37, 36, 21, 21, 25, 29, 38, 20, 41, 37, 37, 21, 26, 20, 37, 20, 30],
    },
    {
      bookNumber: 13,
      name: '1 Chronicles',
      abbreviation: '1 Chr',
      longName: 'First Book of Chronicles',
      lastChapter: 29,
      chapters: [0, 54, 55, 24, 43, 26, 81, 40, 40, 44, 14, 47, 40, 14, 17, 29, 43, 27, 17, 19, 8, 30, 19, 32, 31, 31, 32, 34, 21, 30],
    },
    {
      bookNumber: 14,
      name: '2 Chronicles',
      abbreviation: '2 Chr',
      longName: 'Second Book of Chronicles',
      lastChapter: 36,
      chapters: [0, 17, 18, 17, 22, 14, 42, 22, 18, 31, 19, 23, 16, 22, 15, 19, 14, 19, 34, 11, 37, 20, 12, 21, 27, 28, 23, 9, 27, 36, 27, 21, 33, 25, 33, 27, 23],
    },
    {
      bookNumber: 15,
      name: 'Ezra',
      abbreviation: 'Ezra',
      longName: 'Book of Ezra',
      lastChapter: 10,
      chapters: [0, 11, 70, 13, 24, 17, 22, 28, 36, 15, 44],
    },
    {
      bookNumber: 16,
      name: 'Nehemiah',
      abbreviation: 'Neh',
      longName: 'Book of Nehemiah',
      lastChapter: 13,
      chapters: [0, 11, 20, 32, 23, 19, 19, 73, 18, 38, 39, 36, 47, 31],
    },
    {
      bookNumber: 17,
      name: 'Esther',
      abbreviation: 'Esther',
      longName: 'Book of Esther',
      lastChapter: 10,
      chapters: [0, 22, 23, 15, 17, 14, 14, 10, 17, 32, 3],
    },
    {
      bookNumber: 18,
      name: 'Job',
      abbreviation: 'Job',
      longName: 'Book of Job',
      lastChapter: 42,
      chapters: [0, 22, 13, 26, 21, 27, 30, 21, 22, 35, 22, 20, 25, 28, 22, 35, 22, 16, 21, 29, 29, 34, 30, 17, 25, 6, 14, 23, 28, 25, 31, 40, 22, 33, 37, 16, 33, 24, 41, 30, 24, 34, 17],
    },
    {
      bookNumber: 19,
      name: 'Psalm',
      abbreviation: 'Psalm',
      longName: 'Book of Psalms',
      lastChapter: 150,
      chapters: [0, 6, 12, 8, 8, 12, 10, 17, 9, 20, 18, 7, 8, 6, 7, 5, 11, 15, 50, 14, 9, 13, 31, 6, 10, 22, 12, 14, 9, 11, 12, 24, 11, 22, 22, 28, 12, 40, 22, 13, 17, 13, 11, 5, 26, 17, 11, 9, 14, 20, 23, 19, 9, 6, 7, 23, 13, 11, 11, 17, 12, 8, 12, 11, 10, 13, 20, 7, 35, 36, 5, 24, 20, 28, 23, 10, 12, 20, 72, 13, 19, 16, 8, 18, 12, 13, 17, 7, 18, 52, 17, 16, 15, 5, 23, 11, 13, 12, 9, 9, 5, 8, 28, 22, 35, 45, 48, 43, 13, 31, 7, 10, 10, 9, 8, 18, 19, 2, 29, 176, 7, 8, 9, 4, 8, 5, 6, 5, 6, 8, 8, 3, 18, 3, 3, 21, 26, 9, 8, 24, 13, 10, 7, 12, 15, 21, 10, 20, 14, 9, 6],
    },
    {
      bookNumber: 20,
      name: 'Proverbs',
      abbreviation: 'Prov',
      longName: 'Book of Proverbs',
      lastChapter: 31,
      chapters: [0, 33, 22, 35, 27, 23, 35, 27, 36, 18, 32, 31, 28, 25, 35, 33, 33, 28, 24, 29, 30, 31, 29, 35, 34, 28, 28, 27, 28, 27, 33, 31],
    },
    {
      bookNumber: 21,
      name: 'Ecclesiastes',
      abbreviation: 'Eccl',
      longName: 'Book of Ecclesiastes',
      lastChapter: 12,
      chapters: [0, 18, 26, 22, 16, 20, 12, 29, 17, 18, 20, 10, 14],
    },
    {
      bookNumber: 22,
      name: 'Song of Solomon',
      abbreviation: 'Song',
      longName: 'Book of the Song of Solomon',
      lastChapter: 8,
      chapters: [0, 17, 17, 11, 16, 16, 13, 13, 14],
    },
    {
      bookNumber: 23,
      name: 'Isaiah',
      abbreviation: 'Isa',
      longName: 'Book of Isaiah',
      lastChapter: 66,
      chapters: [0, 31, 22, 26, 6, 30, 13, 25, 22, 21, 34, 16, 6, 22, 32, 9, 14, 14, 7, 25, 6, 17, 25, 18, 23, 12, 21, 13, 29, 24, 33, 9, 20, 24, 17, 10, 22, 38, 22, 8, 31, 29, 25, 28, 28, 25, 13, 15, 22, 26, 11, 23, 15, 12, 17, 13, 12, 21, 14, 21, 22, 11, 12, 19, 12, 25, 24],
    },
    {
      bookNumber: 24,
      name: 'Jeremiah',
      abbreviation: 'Jer',
      longName: 'Book of Jeremiah',
      lastChapter: 52,
      chapters: [0, 19, 37, 25, 31, 31, 30, 34, 22, 26, 25, 23, 17, 27, 22, 21, 21, 27, 23, 15, 18, 14, 30, 40, 10, 38, 24, 22, 17, 32, 24, 40, 44, 26, 22, 19, 32, 21, 28, 18, 16, 18, 22, 13, 30, 5, 28, 7, 47, 39, 46, 64, 34],
    },
    {
      bookNumber: 25,
      name: 'Lamentations',
      abbreviation: 'Lam',
      longName: 'Book of Lamentations',
      lastChapter: 5,
      chapters: [0, 22, 22, 66, 22, 22],
    },
    {
      bookNumber: 26,
      name: 'Ezekiel',
      abbreviation: 'Eze',
      longName: 'Book of Ezekiel',
      lastChapter: 48,
      chapters: [0, 28, 10, 27, 17, 17, 14, 27, 18, 11, 22, 25, 28, 23, 23, 8, 63, 24, 32, 14, 49, 32, 31, 49, 27, 17, 21, 36, 26, 21, 26, 18, 32, 33, 31, 15, 38, 28, 23, 29, 49, 26, 20, 27, 31, 25, 24, 23, 35],
    },
    {
      bookNumber: 27,
      name: 'Daniel',
      abbreviation: 'Dan',
      longName: 'Book of Daniel',
      lastChapter: 12,
      chapters: [0, 21, 49, 30, 37, 31, 28, 28, 27, 27, 21, 45, 13],
    },
    {
      bookNumber: 28,
      name: 'Hosea',
      abbreviation: 'Hosea',
      longName: 'Book of Hosea',
      lastChapter: 14,
      chapters: [0, 11, 23, 5, 19, 15, 11, 16, 14, 17, 15, 12, 14, 16, 9],
    },
    {
      bookNumber: 29,
      name: 'Joel',
      abbreviation: 'Joel',
      longName: 'Book of Joel',
      lastChapter: 3,
      chapters: [0, 20, 32, 21],
    },
    {
      bookNumber: 30,
      name: 'Amos',
      abbreviation: 'Amos',
      longName: 'Book of Amos',
      lastChapter: 9,
      chapters: [0, 15, 16, 15, 13, 27, 14, 17, 14, 15],
    },
    {
      bookNumber: 31,
      name: 'Obadiah',
      abbreviation: 'Oba',
      longName: 'Book of Obadiah',
      lastChapter: 1,
      chapters: [0, 21],
    },
    {
      bookNumber: 32,
      name: 'Jonah',
      abbreviation: 'Jonah',
      longName: 'Book of Jonah',
      lastChapter: 4,
      chapters: [0, 17, 10, 10, 11],
    },
    {
      bookNumber: 33,
      name: 'Micah',
      abbreviation: 'Micah',
      longName: 'Book of Micah',
      lastChapter: 7,
      chapters: [0, 16, 13, 12, 13, 15, 16, 20],
    },
    {
      bookNumber: 34,
      name: 'Nahum',
      abbreviation: 'Nahum',
      longName: 'Book of Nahum',
      lastChapter: 3,
      chapters: [0, 15, 13, 19],
    },
    {
      bookNumber: 35,
      name: 'Habakkuk',
      abbreviation: 'Hab',
      longName: 'Book of Habakkuk',
      lastChapter: 3,
      chapters: [0, 17, 20, 19],
    },
    {
      bookNumber: 36,
      name: 'Zephaniah',
      abbreviation: 'Zeph',
      longName: 'Book of Zephaniah',
      lastChapter: 3,
      chapters: [0, 18, 15, 20],
    },
    {
      bookNumber: 37,
      name: 'Haggai',
      abbreviation: 'Hag',
      longName: 'Book of Haggai',
      lastChapter: 2,
      chapters: [0, 15, 23],
    },
    {
      bookNumber: 38,
      name: 'Zechariah',
      abbreviation: 'Zech',
      longName: 'Book of Zechariah',
      lastChapter: 14,
      chapters: [0, 21, 13, 10, 14, 11, 15, 14, 23, 17, 12, 17, 14, 9, 21],
    },
    {
      bookNumber: 39,
      name: 'Malachi',
      abbreviation: 'Mal',
      longName: 'Book of Malachi',
      lastChapter: 4,
      chapters: [0, 14, 17, 18, 6],
    },
    {
      bookNumber: 40,
      name: 'Matthew',
      abbreviation: 'Mat',
      longName: 'Gospel of Matthew',
      lastChapter: 28,
      chapters: [0, 25, 23, 17, 25, 48, 34, 29, 34, 38, 42, 30, 50, 58, 36, 39, 28, 27, 35, 30, 34, 46, 46, 39, 51, 46, 75, 66, 20],
    },
    {
      bookNumber: 41,
      name: 'Mark',
      abbreviation: 'Mk',
      longName: 'Gospel of Mark',
      lastChapter: 16,
      chapters: [0, 45, 28, 35, 41, 43, 56, 37, 38, 50, 52, 33, 44, 37, 72, 47, 20],
    },
    {
      bookNumber: 42,
      name: 'Luke',
      abbreviation: 'Lk',
      longName: 'Gospel of Luke',
      lastChapter: 24,
      chapters: [0, 80, 52, 38, 44, 39, 49, 50, 56, 62, 42, 54, 59, 35, 35, 32, 31, 37, 43, 48, 47, 38, 71, 56, 53],
    },
    {
      bookNumber: 43,
      name: 'John',
      abbreviation: 'Jn',
      longName: 'Gospel of John',
      lastChapter: 21,
      chapters: [0, 51, 25, 36, 54, 47, 71, 53, 59, 41, 42, 57, 50, 38, 31, 27, 33, 26, 40, 42, 31, 25],
    },
    {
      bookNumber: 44,
      name: 'Acts',
      abbreviation: 'Acts',
      longName: 'The Acts of the Apostles',
      lastChapter: 28,
      chapters: [0, 26, 47, 26, 37, 42, 15, 60, 40, 43, 48, 30, 25, 52, 28, 41, 40, 34, 28, 41, 38, 40, 30, 35, 27, 27, 32, 44, 31],
    },
    {
      bookNumber: 45,
      name: 'Romans',
      abbreviation: 'Rom',
      longName: 'Epistle to the Romans',
      lastChapter: 16,
      chapters: [0, 32, 29, 31, 25, 21, 23, 25, 39, 33, 21, 36, 21, 14, 23, 33, 27],
    },
    {
      bookNumber: 46,
      name: '1 Corinthians',
      abbreviation: '1 Cor',
      longName: 'First Epistle to the Corinthians',
      lastChapter: 16,
      chapters: [0, 31, 16, 23, 21, 13, 20, 40, 13, 27, 33, 34, 31, 13, 40, 58, 24],
    },
    {
      bookNumber: 47,
      name: '2 Corinthians',
      abbreviation: '2 Cor',
      longName: 'Second Epistle to the Corinthians',
      lastChapter: 13,
      chapters: [0, 24, 17, 18, 18, 21, 18, 16, 24, 15, 18, 33, 21, 14],
    },
    {
      bookNumber: 48,
      name: 'Galatians',
      abbreviation: 'Gal',
      longName: 'Epistle to the Galatians',
      lastChapter: 6,
      chapters: [0, 24, 21, 29, 31, 26, 18],
    },
    {
      bookNumber: 49,
      name: 'Ephesians',
      abbreviation: 'Eph',
      longName: 'Epistle to the Ephesians',
      lastChapter: 6,
      chapters: [0, 23, 22, 21, 32, 33, 24],
    },
    {
      bookNumber: 50,
      name: 'Philippians',
      abbreviation: 'Phil',
      longName: 'Epistle to the Philippians',
      lastChapter: 4,
      chapters: [0, 30, 30, 21, 23],
    },
    {
      bookNumber: 51,
      name: 'Colossians',
      abbreviation: 'Col',
      longName: 'Epistle to the Colossians',
      lastChapter: 4,
      chapters: [0, 29, 23, 25, 18],
    },
    {
      bookNumber: 52,
      name: '1 Thessalonians',
      abbreviation: '1 Thes',
      longName: 'First Epistle to the Thessalonians',
      lastChapter: 5,
      chapters: [0, 10, 20, 13, 18, 28],
    },
    {
      bookNumber: 53,
      name: '2 Thessalonians',
      abbreviation: '2 Thes',
      longName: 'Second Epistle to the Thessalonians',
      lastChapter: 3,
      chapters: [0, 12, 17, 18],
    },
    {
      bookNumber: 54,
      name: '1 Timothy',
      abbreviation: '1 Tim',
      longName: 'First Epistle to Timothy',
      lastChapter: 6,
      chapters: [0, 20, 15, 16, 16, 25, 21],
    },
    {
      bookNumber: 55,
      name: '2 Timothy',
      abbreviation: '2 Tim',
      longName: 'Second Epistle to Timothy',
      lastChapter: 4,
      chapters: [0, 18, 26, 17, 22],
    },
    {
      bookNumber: 56,
      name: 'Titus',
      abbreviation: 'Titus',
      longName: 'Epistle to Titus',
      lastChapter: 3,
      chapters: [0, 16, 15, 15],
    },
    {
      bookNumber: 57,
      name: 'Philemon',
      abbreviation: 'Phi',
      longName: 'Epistle to Philemon',
      lastChapter: 1,
      chapters: [0, 25],
    },
    {
      bookNumber: 58,
      name: 'Hebrews',
      abbreviation: 'Heb',
      longName: 'Epistle to the Hebrews',
      lastChapter: 13,
      chapters: [0, 14, 18, 19, 16, 14, 20, 28, 13, 28, 39, 40, 29, 25],
    },
    {
      bookNumber: 59,
      name: 'James',
      abbreviation: 'James',
      longName: 'Epistle of James',
      lastChapter: 5,
      chapters: [0, 27, 26, 18, 17, 20],
    },
    {
      bookNumber: 60,
      name: '1 Peter',
      abbreviation: '1 Pe',
      longName: 'First Epistle of Peter',
      lastChapter: 5,
      chapters: [0, 25, 25, 22, 19, 14],
    },
    {
      bookNumber: 61,
      name: '2 Peter',
      abbreviation: '2 Pe',
      longName: 'Second Epistle of Peter',
      lastChapter: 3,
      chapters: [0, 21, 22, 18],
    },
    {
      bookNumber: 62,
      name: '1 John',
      abbreviation: '1 Jn',
      longName: 'First Epistle of John',
      lastChapter: 5,
      chapters: [0, 10, 29, 24, 21, 21],
    },
    {
      bookNumber: 63,
      name: '2 John',
      abbreviation: '2 Jn',
      longName: 'Second Epistle of John',
      lastChapter: 1,
      chapters: [0, 13],
    },
    {
      bookNumber: 64,
      name: '3 John',
      abbreviation: '3 Jn',
      longName: 'Third Epistle of John',
      lastChapter: 1,
      chapters: [0, 14],
    },
    {
      bookNumber: 65,
      name: 'Jude',
      abbreviation: 'Jude',
      longName: 'Epistle to Jude',
      lastChapter: 1,
      chapters: [0, 25],
    },
    {
      bookNumber: 66,
      name: 'Revelation',
      abbreviation: 'Rev',
      longName: 'Book of Revelations',
      lastChapter: 22,
      chapters: [0, 20, 29, 22, 11, 14, 17, 17, 13, 21, 11, 19, 17, 18, 20, 8, 21, 18, 24, 21, 15, 27, 21],
    },
  ];

  private ref: string;
  section: Section;
  errAcc: string;

  constructor(reference: string) {
    this.section = {
      book: null,
      start: {
        chapter: 0,
        verse: 0,
      },
      end: {
        chapter: 0,
        verse: 0,
      },
    };
    this.ref = reference.toLowerCase().trim();
    this.parseReference();

    if (this.section.end.chapter === 0) {
      this.section.end.chapter = this.section.start.chapter;
    }
    if (this.section.start.verse > this.section.end.verse && this.section.start.chapter === this.section.end.chapter) {
      this.section.end.verse = this.section.start.verse;
    }
    if (this.section.start.verse === 0) {
      this.section.start.verse = 1;
    }
    if (this.section.end.verse === 0) {
      this.section.end.verse = this.section.book.chapters[this.section.end.chapter];
    }
  }

  //#region Public Static Functions

  static parseBook(fbook: string): Book {
    if (fbook.search(/\b(genesis|gen|ge|gn)\b/i) !== -1) {
      return this.bookName(1);
    }
    if (fbook.search(/\b(exodus|ex|exo|exod|exd)\b/i) !== -1) {
      return this.bookName(2);
    }
    if (fbook.search(/\b(leviticus|lev|le|levi|lv)\b/i) !== -1) {
      return this.bookName(3);
    }
    if (fbook.search(/\b(numbers|num|nu|numb|number)\b/i) !== -1) {
      return this.bookName(4);
    }
    if (fbook.search(/\b(deuteronomy|deut|de|dt|deu)\b/i) !== -1) {
      return this.bookName(5);
    }
    if (fbook.search(/\b(joshua|josh|jos)\b/i) !== -1) {
      return this.bookName(6);
    }
    if (fbook.search(/\b(judges|jud|jdg|judg)\b/i) !== -1) {
      return this.bookName(7);
    }
    if (fbook.search(/\b(ruth|ru)\b/i) !== -1) {
      return this.bookName(8);
    }
    if (fbook.search(/\b(1|i|1st|first)\s*(samuel|sa|sam|sml)\b/i) !== -1) {
      return this.bookName(9);
    }
    if (fbook.search(/\b(2|ii|2nd|second|sec)\s*(samuel|sa|sam|sml)\b/i) !== -1) {
      return this.bookName(10);
    }
    if (fbook.search(/\b(1|i|1st|first)\s*(kings|king|kgs|kn|k|ki)\b/i) !== -1) {
      return this.bookName(11);
    }
    if (fbook.search(/\b(2|ii|2nd|second|sec)\s*(kings|king|kgs|kn|k|ki)\b/i) !== -1) {
      return this.bookName(12);
    }
    if (fbook.search(/\b(1|i|1st|first)\s*(chronicles|chron|ch|chr)\b/i) !== -1) {
      return this.bookName(13);
    }
    if (fbook.search(/\b(2|ii|2nd|second|sec)\s*(chronicles|chron|ch|chr)\b/i) !== -1) {
      return this.bookName(14);
    }
    if (fbook.search(/\b(ezra|ezr)\b/i) !== -1) {
      return this.bookName(15);
    }
    if (fbook.search(/\b(nehemiah|neh|ne|nehamiah)\b/i) !== -1) {
      return this.bookName(16);
    }
    if (fbook.search(/\b(esther|est|es|esth)\b/i) !== -1) {
      return this.bookName(17);
    }
    if (fbook.search(/\b(job|jo|jb)\b/i) !== -1) {
      return this.bookName(18);
    }
    if (fbook.search(/\b(psalms|ps|psa|psalm|psm)\b/i) !== -1) {
      return this.bookName(19);
    }
    if (fbook.search(/\b(proverbs|prov|pr|pro|proverb|prv|prvbs)\b/i) !== -1) {
      return this.bookName(20);
    }
    if (fbook.search(/\b(ecclesiastes|eccl|ecc|eccles|ec|ecl|ecclesiaste)\b/i) !== -1) {
      return this.bookName(21);
    }
    if (fbook.search(/\b(song\sof\ssolomon|song\sof\ssongs|sos|ss|son|so|song|songs)\b/i) !== -1) {
      return this.bookName(22);
    }
    if (fbook.search(/\b(isaiah|is|isah|isai|ia)\b/i) !== -1) {
      return this.bookName(23);
    }
    if (fbook.search(/\b(jerimiah|jeremiah|jer|je|jere)\b/i) !== -1) {
      return this.bookName(24);
    }
    if (fbook.search(/\b(lamentations|lam|la|lamentation)\b/i) !== -1) {
      return this.bookName(25);
    }
    if (fbook.search(/\b(ezekiel|eze|ezk|ezek)\b/i) !== -1) {
      return this.bookName(26);
    }
    if (fbook.search(/\b(daniel|dan|dn|dl|da)\b/i) !== -1) {
      return this.bookName(27);
    }
    if (fbook.search(/\b(hosea|hos|ho)\b/i) !== -1) {
      return this.bookName(28);
    }
    if (fbook.search(/\b(joel|joe|jl)\b/i) !== -1) {
      return this.bookName(29);
    }
    if (fbook.search(/\b(amos|am|amo)\b/i) !== -1) {
      return this.bookName(30);
    }
    if (fbook.search(/\b(obadiah|oba|ob|obad)\b/i) !== -1) {
      return this.bookName(31);
    }
    if (fbook.search(/\b(jonah|jnh|jon)\b/i) !== -1) {
      return this.bookName(32);
    }
    if (fbook.search(/\b(micah|mic|mi)\b/i) !== -1) {
      return this.bookName(33);
    }
    if (fbook.search(/\b(nahum|nah|na)\b/i) !== -1) {
      return this.bookName(34);
    }
    if (fbook.search(/\b(habakkuk|hab|ha|habakuk)\b/i) !== -1) {
      return this.bookName(35);
    }
    if (fbook.search(/\b(zephaniah|zeph|zep)\b/i) !== -1) {
      return this.bookName(36);
    }
    if (fbook.search(/\b(haggia|hag|hg|haggai)\b/i) !== -1) {
      return this.bookName(37);
    }
    if (fbook.search(/\b(zechariah|zech|zch|zec)\b/i) !== -1) {
      return this.bookName(38);
    }
    if (fbook.search(/\b(malachi|mal)\b/i) !== -1) {
      return this.bookName(39);
    }
    if (fbook.search(/\b(matthew|mt|matt|mat)\b/i) !== -1) {
      return this.bookName(40);
    }
    if (fbook.search(/\b(mark|mrk|mk|mr)\b/i) !== -1) {
      return this.bookName(41);
    }
    if (fbook.search(/\b(luke|lu|lke|luk|lk)\b/i) !== -1) {
      return this.bookName(42);
    }
    // must come before the Gospel of John
    if (fbook.search(/\b(1|i|1st|first)\s*(john|jn|jo)\b/i) !== -1) {
      return this.bookName(62);
    }
    if (fbook.search(/\b(2|ii|2nd|second|sec)\s*(john|jn|jo)\b/i) !== -1) {
      return this.bookName(63);
    }
    if (fbook.search(/\b(3|iii|3rd|third)\s*(john|jn|jo)\b/i) !== -1) {
      return this.bookName(64);
    }
    // 1, 2, and 3rd John need to come before this one, else this will catch those book references.
    if (fbook.search(/\b(john|jn|jhn)\b/i) !== -1) {
      return this.bookName(43);
    }
    if (fbook.search(/\b(acts|ac|act)\b/i) !== -1) {
      return this.bookName(44);
    }
    if (fbook.search(/\b(romans|rom|ro|rm|roman)\b/i) !== -1) {
      return this.bookName(45);
    }
    if (fbook.search(/\b(1|i|1st|first)\s*(corinthian|cor|corinthians|corinth|corin|corth|corint)\b/i) !== -1) {
      return this.bookName(46);
    }
    if (fbook.search(/\b(2|ii|2nd|second|sec)\s*(corinthian|cor|corinthians|corinth|corin|corth|corint)\b/i) !== -1) {
      return this.bookName(47);
    }
    if (fbook.search(/\b(galatians|galatian|galations|gal|ga|gala|galation|galat)\b/i) !== -1) {
      return this.bookName(48);
    }
    if (fbook.search(/\b(ephesians|eph|ep|ephes|ephe|ephs)\b/i) !== -1) {
      return this.bookName(49);
    }
    if (fbook.search(/\b(philippians|phi|phil|ph|philip)\b/i) !== -1) {
      return this.bookName(50);
    }
    if (fbook.search(/\b(colossians|col|co|colossian|colos|coloss)\b/i) !== -1) {
      return this.bookName(51);
    }
    if (fbook.search(/\b(1|i|1st|first)\s*(thessalonians|the|thessa|thessalonian|thes|thess|th)\b/i) !== -1) {
      return this.bookName(52);
    }
    if (fbook.search(/\b(2|ii|2nd|second|sec)\s*(thessalonians|the|thessa|thessalonian|thes|thess|th)\b/i) !== -1) {
      return this.bookName(53);
    }
    if (fbook.search(/\b(1|i|1st|first)\s*(timothy|tim|ti|timoth|tm)\b/i) !== -1) {
      return this.bookName(54);
    }
    if (fbook.search(/\b(2|ii|2nd|second|sec)\s*(timothy|tim|timoth|tm)\b/i) !== -1) {
      return this.bookName(55);
    }
    if (fbook.search(/\b(titus|tit)\b/i) !== -1) {
      return this.bookName(56);
    }
    if (fbook.search(/\b(philemon|phlmn|phl|phm|phile|philem)\b/i) !== -1) {
      return this.bookName(57);
    }
    if (fbook.search(/\b(hebrews|heb|he|hebrew)\b/i) !== -1) {
      return this.bookName(58);
    }
    if (fbook.search(/\b(james|jam|ja|jas|jms|jame|jm)\b/i) !== -1) {
      return this.bookName(59);
    }
    if (fbook.search(/\b(1|i|1st|first)\s*(peter|pe|pet|pete|pt|p)\b/i) !== -1) {
      return this.bookName(60);
    }
    if (fbook.search(/\b(2|ii|2nd|second|sec)\s*(peter|pe|pet|pete|pt|p)\b/i) !== -1) {
      return this.bookName(61);
    }
    if (fbook.search(/\b(jude|ju)\b/i) !== -1) {
      return this.bookName(65);
    }
    if (fbook.search(/\b(revelation|rev|re|revelations|rv)\b/i) !== -1) {
      return this.bookName(66);
    }

    return this.bookName(0);
  }

  static toString(section: Section) {
    // get the starting book, chapter, verse
    let ref = section.book.name
      .concat(' ') //
      .concat(section.start.chapter.toString()) //
      .concat(':') //
      .concat(section.start.verse.toString()); //

    if (section.start.chapter === section.end.chapter && section.start.verse === section.end.verse) {
      return ref;
    }

    if (section.start.chapter === section.end.chapter && section.start.verse !== section.end.verse) {
      return ref.concat(' - ').concat(section.end.verse.toString());
    }

    ref = ref.concat(' - ');

    ref = ref.concat(section.end.chapter.toString()).concat(':');

    return ref.concat(section.end.verse.toString());
  }

  static bookName(booknum: number): Book {
    return this.Books[booknum];
  }

  static locationToIndex(book: number, loc: Location): number {
    let ref = book * 100000000;
    ref = ref + loc.chapter * 10000;
    ref = ref + loc.verse;
    return ref;
  }

  static formatReferenceKey(book: number | string, chapter: number | string, vs: number | string) {
    return `${book}:${chapter}:${vs}`;
  }

  static makePassageFromReferenceKey(referenceKey: string) {
    const keyArray = referenceKey.split(':');
    const book = BibleReference.bookName(parseInt(keyArray[0], 10)).name;
    return new BibleReference(`${book} ${keyArray[1]}:${keyArray[2]}`);
  }

  static overlap(left: BibleReference, right: BibleReference): Overlap {
    if (left.section.book !== right.section.book) {
      // either of the above means we are not overlapping
      return Overlap.None;
    }

    if (left.toString() === right.toString()) {
      return Overlap.Equal;
    }

    if (
      // left is subset of right
      (left.startIndex >= right.startIndex && left.endIndex <= right.endIndex) ||
      // right is subset of left
      (right.startIndex >= left.startIndex && right.endIndex <= left.endIndex)
    ) {
      return Overlap.Subset;
    }
    if (
      // left overlaps the right
      (left.endIndex >= right.startIndex && left.endIndex <= right.endIndex) ||
      // right overlaps the left
      (right.endIndex >= left.startIndex && right.endIndex <= left.endIndex)
    ) {
      return Overlap.Intersect;
    }
    return Overlap.None;
  }

  public static mergeReference(ref1: BibleReference, ref2: BibleReference, strategy: Overlap): BibleReference | null {
    if (strategy === Overlap.None) {
      return null;
    }
    if (ref1.toString() === ref2.toString() && strategy === Overlap.Equal) {
      return ref1;
    }
    const overlapType = BibleReference.overlap(ref1, ref2);
    switch (overlapType) {
      case Overlap.Subset:
        if (strategy !== Overlap.Subset && strategy !== Overlap.Intersect) {
          return null;
        }
        break;
      case Overlap.Intersect:
        if (strategy !== Overlap.Intersect) {
          return null;
        }
        break;
      case Overlap.None:
        return null;
    }

    // Now it's safe to merge
    const mergedRef = new BibleReference(ref1.toString());
    mergedRef.section.start.chapter =
      ref1.section.start.chapter <= ref2.section.start.chapter
        ? ref1.section.start.chapter //
        : ref2.section.start.chapter; //

    if (ref1.section.start.chapter < ref2.section.start.chapter) {
      mergedRef.section.start.verse = ref1.section.start.verse;
    } else if (ref2.section.start.chapter < ref1.section.start.chapter) {
      mergedRef.section.start.verse = ref2.section.start.verse;
    } else {
      mergedRef.section.start.verse =
        ref1.section.start.verse <= ref2.section.start.verse
          ? ref1.section.start.verse //
          : ref2.section.start.verse; //
    }

    mergedRef.section.end.chapter =
      ref1.section.end.chapter >= ref2.section.end.chapter
        ? ref1.section.end.chapter //
        : ref2.section.end.chapter; //

    if (ref1.section.end.chapter > ref2.section.end.chapter) {
      mergedRef.section.end.verse = ref1.section.end.verse;
    } else if (ref2.section.end.chapter > ref1.section.end.chapter) {
      mergedRef.section.end.verse = ref2.section.end.verse;
    } else {
      mergedRef.section.end.verse =
        ref1.section.end.verse >= ref2.section.end.verse
          ? ref1.section.end.verse //
          : ref2.section.end.verse; //
    }
    return mergedRef;
  }

  //#endregion

  //#region  Private Parsing Functions

  private parseReference() {
    this.parseKeyReference();
    if (this.ref.length === 0) {
      return;
    }
    this.parseBook();
    this.parseChapter(false);

    const foundFirstVerse = this.ref.search(/:.*-/) !== -1;
    this.maybeParseVerse(false);
    this.maybeParseRangeSep();
    const foundSecondBook = this.ref.search(/\w\s+\d/i) !== -1;

    this.maybeParseBook();
    this.maybeParseChapterOrVerse(foundSecondBook, foundFirstVerse, true);
    this.maybeParseVerse(true);
  }

  // we're trying to parse references in the form of 41:2:3
  private parseKeyReference() {
    this.ref = this.ref.toLowerCase().trim();

    const parts = this.ref.split(':');
    if (this.ref.match(/\d+:\d+:\d+/) !== null) {
      const fbook = BibleReference.bookName(parseInt(parts[0], 10));
      this.section.book = fbook;

      const fch = parts[1];
      this.section.end.chapter = parseInt(fch, 10);
      this.section.start.chapter = parseInt(fch, 10);

      const fv = parts[2];
      this.section.end.verse = parseInt(fv, 10);
      this.section.start.verse = parseInt(fv, 10);

      this.ref = '';
    }
  }

  private parseBook() {
    this.ref = this.ref.toLowerCase().trim();

    let fbook = this.ref.substring(0, this.ref.search(/\w\s+\d/i) + 1);
    if (!fbook) {
      fbook = this.ref;
    }
    this.ref = this.ref.slice(this.ref.search(/\w\s+\d/i) + 1);
    this.section.book = BibleReference.parseBook(fbook);
  }

  private parseChapter(isEnd: boolean) {
    let thing = this.section.start;
    if (isEnd) {
      thing = this.section.end;
    }

    this.ref = StringUtils.ltrim(this.ref);
    let found = false;
    let chapter = '';
    for (let i = 0; i <= this.ref.length; i++) {
      const c = this.ref.charAt(i);
      // Grab characters until we hit a non digit.
      if ('0'.charAt(0) <= c && c <= '9'.charAt(0)) {
        found = true;
        chapter = chapter.concat(c);
      } else {
        // if the chapter is longer than 3 digits it's an error
        if (chapter.length > 3) {
          this.errAcc = 'Chapter too long"' + chapter + '".';
          return;
        } else if (!found) {
          this.errAcc = 'No chapter found' + this.ref;
        }
        this.ref = this.ref.slice(i);
        thing.chapter = parseInt(chapter, 10);
        return;
      }
    }
  }

  private parseVerse(skipColon?: boolean, isEnd?: boolean) {
    let thing = this.section.start;
    if (isEnd) {
      thing = this.section.end;
    }

    this.ref = StringUtils.ltrim(this.ref.toLowerCase());
    if (!skipColon) {
      if (this.ref[0] !== ':') {
        return;
      }
      this.ref = this.ref.slice(1);
    }
    this.ref = StringUtils.ltrim(this.ref.toLowerCase());
    let verse = '';
    if (this.ref[0] === '*') {
      if (isEnd) {
        thing.verse = this.section.book.chapters[thing.chapter];
      } else {
        thing.verse = 0;
      }
      this.ref = this.ref.slice(1);
      return;
    }
    for (let i = 0; i <= this.ref.length; i++) {
      const c = this.ref.charAt(i);
      if ('0'.charAt(0) <= c && c <= '9'.charAt(0)) {
        verse = verse.concat(c);
      } else {
        if (verse.length > 3) {
          this.errAcc = 'Verse too long "' + thing.verse + '".';
          return;
        }
        this.ref = this.ref.slice(i);
        thing.verse = verse === '' ? 0 : parseInt(verse, 10);
        return;
      }
    }
  }

  private maybeParseBook() {
    return this.maybeDo(() => {
      if (this.ref.search(/\w\s+\d/i) !== -1) {
        this.parseBook();
      }
    });
  }

  private maybeParseVerse(isEnd?: boolean) {
    return this.maybeDo(() => {
      this.parseVerse(false, isEnd);
    });
  }

  private maybeParseChapterOrVerse(foundSecondBook: boolean, foundFirstVerse: boolean, isEnd: boolean) {
    const self = this;
    return this.maybeDo(() => {
      if (self.ref.search(/:/) !== -1 || foundSecondBook || !foundFirstVerse) {
        self.parseChapter(isEnd);
      }
      if (this.section.end.chapter === 0) {
        this.section.end.chapter = this.section.start.chapter;
      }
      // Fixup chapter here probably
      self.parseVerse(true, isEnd);
    });
  }

  private maybeParseRangeSep() {
    const self = this;
    return this.maybeDo(() => {
      if (self.ref[0] === '-') {
        self.ref = StringUtils.ltrim(self.ref.slice(1));
      }
    });
  }

  private maybeDo(f) {
    const func = f;
    this.ref = StringUtils.ltrim(this.ref.toLowerCase());
    if (this.ref !== '') {
      func();
    }
  }

  //#endregion

  public toString() {
    // get the starting book, chapter, verse
    return BibleReference.toString(this.section);
  }

  get startIndex(): number {
    return BibleReference.locationToIndex(this.section.book.bookNumber, this.section.start);
  }

  get endIndex(): number {
    return BibleReference.locationToIndex(this.section.book.bookNumber, this.section.end);
  }
}

class StringUtils {
  public static trim(str: string): string {
    return str.replace(/^\s+|\s+$/g, '');
  }

  public static ltrim(str: string): string {
    return str.replace(/^\s+/, '');
  }

  public static rtrim(str: string): string {
    return str.replace(/\s+$/, '');
  }
}

export enum Overlap {
  Intersect = 'Is Overlapping',
  Subset = 'Is Contained In',
  Equal = 'Is Equal',
  None = 'None',
}

export interface Book {
  name: string;
  abbreviation: string;
  longName: string;
  bookNumber: number;
  lastChapter: number;
  chapters: Array<number>;
}

export interface Section {
  book: Book;
  start: Location;
  end: Location;
}

export interface Location {
  chapter: number;
  verse: number;
}

﻿using DynamicBibleUtility.Geolocation;

namespace DynamicBible.DataPreparation.Models.Geolocation;

/// <summary>
/// A reference to a Bible verse including a book, chapter, and verse.
/// </summary>
public class BibleVerseReference
{
    /// <summary>The book of the Bible.</summary>
    public BibleBook? Book = null;

    /// <summary>The chapter number within the book.</summary>
    public int Chapter = 0;

    /// <summary>The verse number within the book.</summary>
    public int Verse = 0;

    /// <summary>
    /// Converts the verse reference to the canonical string form
    /// for the Dynamic Bible app.  This form has the numeric book,
    /// chapter, and verse in a single colon-separated string.
    /// </summary>
    /// <returns>The canonical short string form of the verse reference.</returns>
    public override string ToString()
    {
        var book_number = Convert.ToInt32(Book?.Id);
        var short_reference_string = $"{book_number}:{Chapter}:{Verse}";
        return short_reference_string;
    }
}
import { HashTable } from '../common/hashtable';
import { CardItem, DataReference } from '../models/card-state';

export function updateInCardCache(card: CardItem, cardCache: HashTable<CardItem>): HashTable<CardItem> {
  const cache = { ...cardCache };
  cache[getCardCacheKey(card)] = card;
  return cache;
}

export function removeFromCardCache(card: CardItem, cardCache: HashTable<CardItem>): HashTable<CardItem> {
  const cache = { ...cardCache };
  delete cache[getCardCacheKey(card)];
  return cache;
}

export function getFromCardCache(ref: DataReference, cardCache: HashTable<CardItem>) {
  const key = getCardCacheKey(ref);
  return cardCache[key];
}

export function getCardCacheKey(card: DataReference) {
  return `${card.qry}:${card.type}`;
}

import { Clipboard } from '@angular/cdk/clipboard';
import { ChangeDetectionStrategy,Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BibleReference, Overlap } from 'src/app/common/bible-reference';
import { isNullOrUndefined } from 'src/app/common/helpers';
import { CardComponent } from 'src/app/components/card.component';
import { CardItem } from 'src/app/models/card-state';
import { NoteItem } from 'src/app/models/note-state';
import { BiblePassageResult,Paragraph } from 'src/app/models/passage-state';
import { AppService } from 'src/app/services/app.service';

import { StrongsModalComponent } from '../strongs/modal/strongs-modal.component';

@Component({
  selector: 'app-passage-card',
  templateUrl: 'passage-card.component.html',
  styleUrls: ['./passage-card.component.scss'],
  preserveWhitespaces: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PassageCardComponent extends CardComponent implements OnInit {
  ref: BibleReference;
  displaySettings$ = this.appService.select((state) => state.settings.value.displaySettings);

  // whenever the notes changes, look for any notes that reference this passage.
  notes$ = this.appService.select((state) =>
    !isNullOrUndefined(state.notes) && !isNullOrUndefined(state.notes.value) && !isNullOrUndefined(this.ref)
      ? state.notes.value.filter((o) => {
          const refs = o.xref
            .split(';')
            .map((r) => new BibleReference(r))
            .filter((r) => BibleReference.overlap(this.ref, r) !== Overlap.None);
          return refs.length > 0;
        })
      : []
  );
  hasNotes = false;

  @ViewChild('passage') passageElement: ElementRef;

  get data(): BiblePassageResult {
    return this.cardItem.data as BiblePassageResult;
  }

  constructor(
    protected elementRef: ElementRef,
    protected appService: AppService,
    protected clipboard: Clipboard,
    public dialog: MatDialog
  ) {
    super(elementRef, dialog, appService, clipboard);
    this.icon$ = appService.select((state) => state.settings.value.cardIcons.passage);
  }

  ngOnInit(): void {
    this.ref = new BibleReference(this.cardItem.qry);
  }

  getDict(item: CardItem) {
    return (item.data as BiblePassageResult).dict === 'grk' ? 'G' : 'H';
  }

  copy() {
    const text = this.passageElement.nativeElement.innerText + ` \n - ${this.ref.toString()}`;
    this.copyToClip(text);
  }

  next() {
    const newRef = new BibleReference(this.ref.toString());

    const lastVerseForEnd = newRef.section.book.chapters[newRef.section.end.chapter];

    if (newRef.section.end.verse !== 0 && newRef.section.end.verse !== lastVerseForEnd) {
      newRef.section.end.chapter = newRef.section.end.chapter;
    } else {
      newRef.section.end.chapter = newRef.section.end.chapter + 1;
    }

    newRef.section.start.chapter = newRef.section.end.chapter;
    newRef.section.start.verse = 1;
    newRef.section.end.verse = newRef.section.book.chapters[newRef.section.end.chapter];

    this.appService.updatePassage(this.cardItem, newRef);
  }

  prev() {
    const newRef = new BibleReference(this.ref.toString());

    if (newRef.section.start.verse !== 1) {
      newRef.section.start.chapter = newRef.section.start.chapter;
    } else {
      newRef.section.start.chapter = newRef.section.start.chapter - 1;
    }

    newRef.section.end.chapter = newRef.section.start.chapter;
    newRef.section.start.verse = 1;
    newRef.section.end.verse = newRef.section.book.chapters[newRef.section.end.chapter];

    this.appService.updatePassage(this.cardItem, newRef);
  }

  expand() {
    const newRef = new BibleReference(this.ref.toString());

    const lastVerseForEnd = newRef.section.book.chapters[newRef.section.end.chapter];

    // if your verse is at the beginning, to go the prev chapter and add 3 verses from that
    if (newRef.section.start.verse < 4) {
      newRef.section.start.chapter = newRef.section.start.chapter - 1;
      newRef.section.start.verse =
        newRef.section.book.chapters[newRef.section.start.chapter] - (3 - newRef.section.start.verse);
      if (newRef.section.start.chapter === 0) {
        newRef.section.start.chapter = 1;
        newRef.section.start.verse = 1;
      }
    } else {
      // or go back 3 verses
      newRef.section.start.verse = newRef.section.start.verse - 3;
    }

    // if your verse is at the end, go to the next chapter
    if (newRef.section.end.verse === 0 || newRef.section.end.verse + 3 > lastVerseForEnd) {
      newRef.section.end.chapter = newRef.section.end.chapter + 1;
      if (newRef.section.end.verse === 0) {
        newRef.section.end.verse = 3;
      } else {
        newRef.section.end.verse = newRef.section.end.verse + 3 - lastVerseForEnd;
      }

      if (newRef.section.end.chapter === newRef.section.book.lastChapter + 1) {
        newRef.section.end.chapter = newRef.section.book.lastChapter;
        newRef.section.end.verse = lastVerseForEnd;
      }
    } else {
      // or add 3 verses
      newRef.section.end.verse = newRef.section.end.verse + 3;
    }

    if (newRef.section.start.verse === 0) {
      newRef.section.start.verse = 1;
    }

    this.appService.updatePassage(this.cardItem, newRef);
  }

  openNote(note: NoteItem) {
    this.appService.getNote(note.id, this.cardItem);
  }

  async openStrongs(q: string, asModal = false) {
    const dict = (this.cardItem.data as BiblePassageResult).dict;
    const numbers = q.split(' ');

    if (asModal) {
      const cards: CardItem[] = [];
      for (const sn of numbers) {
        const card = await this.appService.getStrongsCard(sn, dict);
        cards.push(card);
      }

      this.dialog.open(StrongsModalComponent, {
        data: cards,
        autoFocus: 'content',
      });
    } else {
      this.appService.getStrongs(numbers, dict, this.cardItem);
    }
  }

  hasHeader(p: Paragraph) {
    if (p === undefined) {
      return false;
    }

    return p.h.length > 0;
  }
}

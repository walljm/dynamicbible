import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { Component, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { getFromCardCache } from 'src/app/common/card-cache-operations';
import { HashTable } from 'src/app/common/hashtable';
import { SubscriberBase } from 'src/app/common/subscriber-base';
import { CardIcons, CardItem, CardType, DataReference } from 'src/app/models/card-state';
import { NoteItem } from 'src/app/models/note-state';
import { SavedPage } from 'src/app/models/page-state';
import { AppService } from 'src/app/services/app.service';

import { OkCancelModalComponent, OkCancelResult } from '../ok-cancel-modal/ok-cancel-modal.component';
import { PageEditModalComponent } from '../page-edit-modal/page-edit-modal.component';

@Component({
  selector: 'app-saved-page-card',
  templateUrl: 'saved-page-card.component.html',
  styleUrls: ['./saved-page-card.component.scss'],
  preserveWhitespaces: true,
})
export class SavedPageCardComponent extends SubscriberBase {
  icon$: Observable<string>;
  cache: HashTable<CardItem>;
  cardIcons: CardIcons;

  @Input()
  savedPage: SavedPage;

  constructor(protected appService: AppService, public dialog: MatDialog, private snackBar: MatSnackBar) {
    super();

    this.icon$ = appService.select((state) => state.settings.value.cardIcons.savedPage);
    this.addSubscription(
      this.appService.state$.subscribe((state) => {
        this.cache = state.cardCache;
        this.cardIcons = state.settings.value.cardIcons;
      })
    );
  }

  class(item: DataReference) {
    if (item.type === CardType.Note) {
      return 'note';
    } else if (item.type === CardType.Passage) {
      return 'passage';
    } else if (item.type === CardType.Strongs) {
      return 'strongs';
    } else if (item.type === CardType.Word) {
      return 'words';
    }
    return '';
  }

  format(item: DataReference) {
    if (item.type === CardType.Note) {
      return `${(getFromCardCache(item, this.cache).data as NoteItem).title}`;
    } else if (item.type === CardType.Passage) {
      return `${item.qry}`;
    } else if (item.type === CardType.Strongs) {
      return `${item.qry}`;
    } else if (item.type === CardType.Word) {
      return `${item.qry}`;
    }
    return '';
  }

  icons(item: DataReference) {
    if (this.cardIcons === null || this.cardIcons === undefined) {
      return 'page';
    }

    if (item.type === CardType.Note) {
      return this.cardIcons.note;
    } else if (item.type === CardType.Passage) {
      return this.cardIcons.passage;
    } else if (item.type === CardType.Strongs) {
      return this.cardIcons.strongs;
    } else if (item.type === CardType.Word) {
      return this.cardIcons.words;
    }
    return '';
  }

  onRemoveCard(reference: DataReference) {
    this.dialog
      .open(OkCancelModalComponent, {
        data: {
          title: 'Delete Card from Saved Page',
          content: `Do you want to delete this card '${this.format(reference)}' from the saved page '${
            this.savedPage.title
          }?`,
        },
      })
      .afterClosed()
      .subscribe((ds: OkCancelResult) => {
        if (ds.ok) {
          this.appService.updateSavedPage({
            ...this.savedPage,
            queries: this.savedPage.queries.filter((o) => o !== reference),
          });
          this.snackBar.open(`${this.savedPage.title} has been updated!`, '', {
            duration: 3 * 1000,
          });
        }
      });
  }

  onRemovePage() {
    this.dialog
      .open(OkCancelModalComponent, {
        data: {
          title: 'Delete Saved Page',
          content: `Do you want to delete the saved page '${this.savedPage.title}?`,
        },
      })
      .afterClosed()
      .subscribe((ds: OkCancelResult) => {
        if (ds.ok) {
          this.appService.removeSavedPage(this.savedPage);
          this.snackBar.open(`${this.savedPage.title} has been deleted!`, '', {
            duration: 3 * 1000,
          });
        }
      });
  }

  onEditPageTitle() {
    this.dialog.open(PageEditModalComponent, {
      data: {
        title: this.savedPage.title,
        savedPage: this.savedPage,
        autoFocus: 'content',
      },
    });
  }

  moveSavedPageCard(event: CdkDragDrop<string[]>) {
    this.appService.moveSavedPageCard(this.savedPage, event.previousIndex, event.currentIndex);
  }
}

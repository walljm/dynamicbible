﻿namespace DynamicBibleUtility.Geolocation;

/// <summary>
/// An enum for uniquely identifying a book of the Bible.
/// An "invalid" enum value is first so that the numeric
/// value for actual books starts at 1, which helps maintain
/// consistency with how verse references are used in other
/// data for the app.
/// </summary>
public enum BibleBookId
{
    INVALID,
    GENESIS,
    EXODUS,
    LEVITICUS,
    NUMBERS,
    DEUTERONOMY,
    JOSHUA,
    JUDGES,
    RUTH,
    FIRST_SAMUEL,
    SECOND_SAMUEL,
    FIRST_KINGS,
    SECOND_KINGS,
    FIRST_CHRONICLES,
    SECOND_CHRONICLES,
    EZRA,
    NEHEMIAH,
    ESTHER,
    JOB,
    PSALMS,
    PROVERBS,
    ECCLESIASTES,
    SONG_OF_SOLOMON,
    ISAIAH,
    JEREMIAH,
    LAMENTATIONS,
    EZEKIEL,
    DANIEL,
    HOSEA,
    JOEL,
    AMOS,
    OBADIAH,
    JONAH,
    MICAH,
    NAHUM,
    HABAKKUK,
    ZEPHANIAH,
    HAGGAI,
    ZECHARIAH,
    MALACHI,
    MATTHEW,
    MARK,
    LUKE,
    JOHN,
    ACTS,
    ROMANS,
    FIRST_CORINTHIANS,
    SECOND_CORINTHIANS,
    GALATIANS,
    EPHESIANS,
    PHILIPPIANS,
    COLOSSIANS,
    FIRST_THESSALONIANS,
    SECOND_THESSALONIANS,
    FIRST_TIMOTHY,
    SECOND_TIMOTHY,
    TITUS,
    PHILEMON,
    HEBREWS,
    JAMES,
    FIRST_PETER,
    SECOND_PETER,
    FIRST_JOHN,
    SECOND_JOHN,
    THIRD_JOHN,
    JUDE,
    REVELATION,
};

/// <summary>
/// A book of the Bible.
/// </summary>
public class BibleBook
{
    /// <summary>The unique ID of book.</summary>
    public readonly BibleBookId Id;

    /// <summary>
    /// Constructs a book by parsing a string.
    /// </summary>
    /// <param name="bookString">A string representation of the book.</param>
    /// <exception cref="System.Exception">Thrown if a parsing error occurs.</exception>
    public BibleBook(string bookString)
    {
        // DEFINE A MAPPING OF BOOK STRINGS TO BOOK IDs.
        // This code currently only handles parsing a subset of book strings as
        // needed for geolocation parsing.  If needed, it could be made more
        // generic later and moved out of this namespace, but the structure
        // of this logic would need to be altered.
        var stringToBookIdLookup = new Dictionary<string, BibleBookId>
        {
            { "Gen", BibleBookId.GENESIS },
            { "Ex", BibleBookId.EXODUS },
            { "Lev", BibleBookId.LEVITICUS },
            { "Num", BibleBookId.NUMBERS },
            { "Deut", BibleBookId.DEUTERONOMY },
            { "Josh", BibleBookId.JOSHUA },
            { "Judg", BibleBookId.JUDGES },
            { "Ruth", BibleBookId.RUTH },
            { "1 Sam", BibleBookId.FIRST_SAMUEL },
            { "2 Sam", BibleBookId.SECOND_SAMUEL },
            { "1 Kgs", BibleBookId.FIRST_KINGS },
            { "2 Kgs", BibleBookId.SECOND_KINGS },
            { "1 Chr", BibleBookId.FIRST_CHRONICLES },
            { "2 Chr", BibleBookId.SECOND_CHRONICLES },
            { "Ezra", BibleBookId.EZRA },
            { "Neh", BibleBookId.NEHEMIAH },
            { "Est", BibleBookId.ESTHER },
            { "Job", BibleBookId.JOB },
            { "Ps", BibleBookId.PSALMS },
            // No locations exist for Proverbs. { "", BibleBookId.PROVERBS },
            { "Eccl", BibleBookId.ECCLESIASTES },
            { "Sng", BibleBookId.SONG_OF_SOLOMON },
            { "Isa", BibleBookId.ISAIAH },
            { "Jer", BibleBookId.JEREMIAH },
            { "Lam", BibleBookId.LAMENTATIONS },
            { "Ezek", BibleBookId.EZEKIEL },
            { "Dan", BibleBookId.DANIEL },
            { "Hos", BibleBookId.HOSEA },
            { "Joel", BibleBookId.JOEL },
            { "Amos", BibleBookId.AMOS },
            { "Obad", BibleBookId.OBADIAH },
            { "Jonah", BibleBookId.JONAH },
            { "Mic", BibleBookId.MICAH },
            { "Nahum", BibleBookId.NAHUM },
            { "Hab", BibleBookId.HABAKKUK },
            { "Zeph", BibleBookId.ZEPHANIAH },
            { "Hag", BibleBookId.HAGGAI },
            { "Zech", BibleBookId.ZECHARIAH },
            { "Mal", BibleBookId.MALACHI },
            { "Matt", BibleBookId.MATTHEW },
            { "Mark", BibleBookId.MARK },
            { "Luke", BibleBookId.LUKE },
            { "John", BibleBookId.JOHN },
            { "Acts", BibleBookId.ACTS },
            { "Rom", BibleBookId.ROMANS },
            { "1 Cor", BibleBookId.FIRST_CORINTHIANS },
            { "2 Cor", BibleBookId.SECOND_CORINTHIANS },
            { "Gal", BibleBookId.GALATIANS },
            { "Eph", BibleBookId.EPHESIANS },
            { "Phil", BibleBookId.PHILIPPIANS },
            { "Col", BibleBookId.COLOSSIANS },
            { "1 Thes", BibleBookId.FIRST_THESSALONIANS },
            { "2 Thes", BibleBookId.SECOND_THESSALONIANS },
            { "1 Tim", BibleBookId.FIRST_TIMOTHY },
            { "2 Tim", BibleBookId.SECOND_TIMOTHY },
            { "Titus", BibleBookId.TITUS },
            // No locations exist for Philemon. { "", BibleBookId.PHILEMON },
            { "Heb", BibleBookId.HEBREWS },
            // No locations exist for James. { "", BibleBookId.JAMES },
            { "1 Pet", BibleBookId.FIRST_PETER },
            { "2 Pet", BibleBookId.SECOND_PETER },
            // No locations exist for 1 John. { "", BibleBookId.FIRST_JOHN },
            // No locations exist for 2 John. { "", BibleBookId.SECOND_JOHN },
            // No locations exist for 3 John. { "", BibleBookId.THIRD_JOHN },
            { "Jude", BibleBookId.JUDE },
            { "Rev", BibleBookId.REVELATION },
        };

        // GET THE BOOK ID FROM THE STRING.
        Id = stringToBookIdLookup[bookString];
    }
}
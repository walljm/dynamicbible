﻿using Microsoft.Extensions.Logging;

namespace DynamicBible.DataPreparation;

internal class Program
{
    private static void Main()
    {
        using ILoggerFactory factory = LoggerFactory.Create(builder => builder.AddConsole());
        var logger = factory.CreateLogger<BibleProcessor>();
        new BibleProcessor(logger)
           .CreateText(
                Path.Combine(
                    Environment.CurrentDirectory,
                    "..",
                    "..",
                    "..",
                    "Data",
                    "kjv_plus"
                )
            );

        // StepBibleProcessor.ProcessStep(
        //     "/Users/walljm/projects/walljm/dynamicbible/data/stepbible/TAGNT Act-Rev - Translators Amalgamated Greek NT - STEPBible.org CC-BY.txt"
        // );
    }
}
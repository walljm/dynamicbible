import { Component, Inject, ViewChild } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA,MatDialogRef } from '@angular/material/dialog';
import { MatSelectionList } from '@angular/material/list';
import { AppService } from 'src/app/services/app.service';

import { CardItem } from '../../models/card-state';

@Component({
  selector: 'app-add-to-page-modal-modal',
  templateUrl: 'add-to-page-modal.component.html',
  styleUrls: ['./add-to-page-modal.component.scss'],
})
export class AddToPageModalComponent {
  form: UntypedFormGroup;
  title: string;
  pages$ = this.appService.select((state) => (state.savedPages === null ? null : state.savedPages.value));

  @ViewChild('pageList')
  pageList: MatSelectionList;

  constructor(
    @Inject(MAT_DIALOG_DATA) public card: CardItem,
    public dialogRef: MatDialogRef<AddToPageModalComponent>,
    private appService: AppService,
    private fb: UntypedFormBuilder
  ) {
    this.form = this.fb.group({
      title: this.title,
    });
  }

  cancel() {
    this.dialogRef.close();
  }

  addPage() {
    this.appService.savePage(this.form.get('title').value);
    this.form.get('title').setValue('');
  }

  save() {
    for (const page of this.pageList.selectedOptions.selected) {
      this.appService.addCardToSavedPage(page.value.id, this.card);
    }

    this.dialogRef.close();
  }
}

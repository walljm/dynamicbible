using DynamicBible.DataPreparation.Models.Geolocation;
using DynamicBibleUtility.Geolocation;
using Microsoft.Extensions.Logging;

namespace DynamicBible.DataPreparation;

public class GeoProcessor
{
    /// <summary>
    /// Prompts the user for a geolocation data file (<see cref="OpenBibleDotInfoLocationParser" />)
    /// and converts a chosen file into the appropriate output JSON files for the Dynamic Bible app.
    /// </summary>
    public void CreateGeolocationJson(string geoLocationsFilepath, ILogger<GeoProcessor>? logger = null)
    {
        // LET THE USER CHOOSE THE FILE WITH GEOLOCATION DATA.
        try
        {
            // READ THE LOCATION INFORMATION FROM THE FILE.
            var locations = OpenBibleDotInfoLocationParser.Parse(geoLocationsFilepath);
            logger?.LogInformation($"Parsed {locations.Count()} locations.");

            // CREATE MORE USEFUL INDICES FOR THE LOCATIONS.
            // Indexing in these different ways is useful for quick lookups in the Dynamic Bible app.
            var locationsByName = new BibleLocationIndexByName(locations);
            logger?.LogInformation("Finished indexing locations by name.");
            var locationsByVerse = new BibleLocationIndexByVerse(locations);
            logger?.LogInformation("Finished indexing locations by verse.");
            var locationsByStrongsNumbers = new BibleLocationIndexByStrongsNumbers(locations);
            logger?.LogInformation("Finished indexing locations by Strong's numbers.");

            // WRITE OUT THE GEOLOCATION DATA TO JSON FORMAT.
            const string LOCATIONS_BY_NAME_JSON_FILENAME = "locations_by_name.json";
            var locations_by_name_in_json = JSON.Serialize(locationsByName.NameToLocationLookup);
            File.WriteAllText(LOCATIONS_BY_NAME_JSON_FILENAME, locations_by_name_in_json);
            logger?.LogInformation($"Wrote locations by name to {LOCATIONS_BY_NAME_JSON_FILENAME} in current working directory.\n");

            const string LOCATION_NAMES_BY_VERSE_JSON_FILENAME = "location_names_by_verse.json";
            var location_names_by_verse_in_json = JSON.Serialize(locationsByVerse.VerseToLocationNameLookup);
            File.WriteAllText(LOCATION_NAMES_BY_VERSE_JSON_FILENAME, location_names_by_verse_in_json);
            logger?.LogInformation($"Wrote location names by verse to {LOCATION_NAMES_BY_VERSE_JSON_FILENAME} in current working directory.\n");

            const string LOCATION_NAMES_BY_STRONGS_NUMBER = "location_names_by_strongs.json";
            var location_names_by_strongs_number_in_json = JSON.Serialize(locationsByStrongsNumbers.StrongsNumberToLocationNameLookup);
            File.WriteAllText(LOCATION_NAMES_BY_STRONGS_NUMBER, location_names_by_strongs_number_in_json);
            logger?.LogInformation($"Wrote location names by Strong's number to {LOCATION_NAMES_BY_STRONGS_NUMBER} in current working directory.\n");

            const string LOCATIONS_BY_STRONGS_NUMBER = "locations_by_strongs.json";
            var locations_by_strongs_number_in_json = JSON.Serialize(locationsByStrongsNumbers.StrongsNumberToLocationLookup);
            File.WriteAllText(LOCATIONS_BY_STRONGS_NUMBER, locations_by_strongs_number_in_json);
            logger?.LogInformation($"Wrote locations by Strong's number to {LOCATIONS_BY_STRONGS_NUMBER} in current working directory.\n");

            // INFORM THE USER THAT CREATING THE GEOLOCATION JSON FILES IS COMPLETE.
            logger?.LogInformation("Done.\n");
        }
        catch (Exception exception)
        {
            logger?.LogInformation($"Exception while processing geolocations: {exception}\n");
        }
    }
}
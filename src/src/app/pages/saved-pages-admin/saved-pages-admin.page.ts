import { Component, OnInit } from '@angular/core';
import { SubscriberBase } from 'src/app/common/subscriber-base';
import { AppService } from 'src/app/services/app.service';
import { NavService } from 'src/app/services/nav.service';

@Component({
  selector: 'app-saved-pages-admin',
  templateUrl: './saved-pages-admin.page.html',
  styleUrls: ['./saved-pages-admin.page.scss'],
})
export class SavedPagesAdminPageComponent extends SubscriberBase implements OnInit {
  savedPages$ = this.appService.select((state) => (state.savedPages === null ? null : state.savedPages.value));

  constructor(public navService: NavService, private appService: AppService) {
    super();
  }

  ngOnInit() {
    this.navService.closeNav(); // close the nav immediately.
  }
}

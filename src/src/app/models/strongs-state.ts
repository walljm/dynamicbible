export type StrongsDictionary = 'heb' | 'grk';

export interface StrongsResult {
  readonly prefix: string;
  readonly sn: number;
  readonly def: StrongsDefinition;
  readonly rmac: RMACDefinition;
  readonly crossrefs: StrongsCrossReference;
  readonly rmaccode: string;
}

export interface StrongsDefinition {
  readonly n: number;
  readonly i: string;
  readonly tr: string;
  readonly de: readonly StrongsDefinitionPart[];
  readonly lemma: string;
  readonly p: string;
}

export interface StrongsDefinitionPart {
  readonly sn: string;
  readonly w: string;
}
export interface StrongsCrossReference {
  readonly id: string; // strongs id H1|G1
  readonly t: string; // strongs testament grk|heb
  readonly d: string; // strongs word/data definition Aaron {ah-ar-ohn'}
  readonly ss: readonly [
    {
      readonly w: string; // translated word
      readonly rs: readonly [
        {
          readonly r: string; // reference
        }
      ];
    }
  ];
}

export interface RMACDefinition {
  readonly id: string;
  readonly d: readonly string[];
}

export interface RMACCrossReference {
  readonly i: string;
  readonly r: string;
}

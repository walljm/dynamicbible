import { Injectable,OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SubscriberBase implements OnDestroy {
  protected subscriptions: Subscription[] = [];

  public ngOnDestroy(): void {
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
  }

  protected addSubscriptions(...subs: Subscription[]) {
    this.subscriptions.push(...subs);
  }
  protected addSubscription(sub: Subscription) {
    this.subscriptions.push(sub);
  }
}

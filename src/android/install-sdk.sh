#!/usr/bin/env bash

# Install the signing keys
echo "Creating store key file and signing properties"
base64 -d $play_store_key > ./play_store_key.pfx
cp $play_store_signing_properties ./signing.properties
cp $play_store_credential ./play-store.json

# Install Build Essentials
echo sdk.dir="/home/user/android-sdk-linux" > ./local.properties

# diagnostics, show me what you've got and where you're at
ls -laF
pwd

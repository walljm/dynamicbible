import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { BehaviorSubject, Observable } from 'rxjs';
import { BibleReference } from 'src/app/common/bible-reference';
import { SubscriberBase } from 'src/app/common/subscriber-base';
import { CardItem } from 'src/app/models/card-state';
import { StrongsResult } from 'src/app/models/strongs-state';
import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-strongs-modal',
  templateUrl: 'strongs-modal.component.html',
  styleUrls: ['./strongs-modal.component.scss'],
  preserveWhitespaces: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StrongsModalComponent extends SubscriberBase {
  icon$: Observable<string>;
  results$: BehaviorSubject<StrongsResult[]> = new BehaviorSubject([]);
  selectedIndex = 0;
  title: string;
  asModal = false;

  constructor(
    @Inject(MAT_DIALOG_DATA) public cardItems: CardItem[],
    public dialogRef: MatDialogRef<StrongsModalComponent>,
    private appService: AppService,
    protected dialog: MatDialog
  ) {
    super();
    this.title = cardItems.map((o) => o.qry).reduce((p, c) => `${p}, ${c}`);
    this.results$.next(cardItems.map((o) => o.data as StrongsResult));
    this.icon$ = appService.select((state) => state.settings.value.cardIcons.strongs);

    this.addSubscription(
      this.appService
        .select((state) => state.settings.value.displaySettings.showStrongsAsModal)
        .subscribe((asModal) => {
          this.asModal = asModal;
        })
    );
  }

  cancel() {
    this.dialogRef.close();
  }

  async openStrongs(q: string) {
    const dict = q.substring(0, 1) === 'H' ? 'heb' : 'grk';
    const sn = q.substring(1);
    if (this.asModal) {
      const currIdx = this.results$.value.findIndex((o) => o.sn.toString() === sn);
      if (currIdx > -1) {
        this.selectedIndex = currIdx;
        return;
      }
      const card = await this.appService.getStrongsCard(sn, dict);
      const strongs = card.data as StrongsResult;
      const ordered = [...this.results$.value.filter((o) => `${o.prefix}${o.sn}` !== q), strongs];
      this.results$.next(ordered);
      this.selectedIndex = ordered.length - 1;
    } else {
      this.appService.getStrongs([sn], dict, this.cardItems[0]);
    }
  }

  openPassage(p: string) {
    const ref = BibleReference.makePassageFromReferenceKey(p);
    this.appService.getPassage(ref, this.cardItems[0]);
  }
}

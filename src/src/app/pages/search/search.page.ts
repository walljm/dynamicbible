import { ChangeDetectionStrategy, Component, ElementRef,OnInit, ViewChild } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatAutocomplete,MatAutocompleteTrigger } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, NavigationEnd,Router } from '@angular/router';
import { getFromCardCache } from 'src/app/common/card-cache-operations';
import { CardItem, CardType } from 'src/app/models/card-state';
import { AppService } from 'src/app/services/app.service';

import { SubscriberBase } from '../../common/subscriber-base';
import { VersePickerModalComponent } from '../../components/verse-picker-modal/verse-picker-modal.component';
import { NavService } from '../../services/nav.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SearchPageComponent extends SubscriberBase implements OnInit {
  cards$ = this.appService.select((state) => state.currentCards.value.map((o) => getFromCardCache(o, state.cardCache)));
  suggestions$ = this.appService.select((state) => state.autocomplete);

  savedPagedLoaded = false;
  clearSearchAfterQuery = true;

  searchControl = new UntypedFormControl();

  @ViewChild(MatAutocomplete)
  autoComplete: MatAutocomplete;

  @ViewChild('autoCompleteInput', { read: MatAutocompleteTrigger })
  autoCompleteTrigger: MatAutocompleteTrigger;

  @ViewChild('contentView')
  contentView: ElementRef;

  constructor(
    private activatedRoute: ActivatedRoute,
    private appService: AppService,
    private router: Router,
    public navService: NavService,
    public dialog: MatDialog
  ) {
    super();

    // you need to know when certain things are loaded, so you can then do stuff.
    //   this will watch the state and store those items in your class so you can monitor them.
    this.addSubscription(
      this.appService.state$.subscribe((state) => {
        this.savedPagedLoaded = state.savedPagesLoaded;
        this.clearSearchAfterQuery = state.settings.value.displaySettings.clearSearchAfterQuery;
      })
    );
  }

  ngOnInit() {
    this.addSubscription(
      this.router.events.subscribe((evt) => {
        if (evt instanceof NavigationEnd) {
          this.init();
        }
      })
    );

    this.init();

    // subscribe to autocomplete input control's changes
    this.addSubscription(
      this.searchControl.valueChanges.subscribe((value: string) => this.appService.getAutoComplete(value.toLowerCase()))
    );
  }

  init() {
    if (this.activatedRoute.snapshot.paramMap.has('term')) {
      // if a route was passed in, perform a search.
      const term = this.activatedRoute.snapshot.paramMap.get('term');
      this.search(term);
    } else if (this.activatedRoute.snapshot.paramMap.has('id')) {
      // if this is a saved page
      const id = this.activatedRoute.snapshot.paramMap.get('id');
      this.onSavedPagedLoaded(id);
    }
    this.navService.closeNav(); // close the nav immediately.
  }

  // monitor the saved paged loaded property,
  //  and fire the action once it turns true, then stop.
  onSavedPagedLoaded(id: string) {
    if (this.savedPagedLoaded) {
      this.appService.getSavedPage(id);
      this.contentView?.nativeElement?.focus();
      return; // you did it! stop trying.
    }
    // the saved page hasn't loaded yet, wait then try again.
    setTimeout(() => {
      this.onSavedPagedLoaded(id);
    }, 100);
  }

  launchPicker() {
    this.dialog.open(VersePickerModalComponent, {
      autoFocus: 'content',
    });
  }

  //#region Search

  removeCard(card: CardItem) {
    this.appService.removeCard(card);
  }

  async searchWithTarget(target: EventTarget) {
    const term = (target as HTMLInputElement).value;
    await this.search(term);
  }
  async search(search: string) {
    // clear search box.
    if (this.clearSearchAfterQuery) {
      this.searchControl.setValue('');
    }

    if (this.autoCompleteTrigger) {
      this.autoCompleteTrigger.closePanel();
    }

    try {
      const terms = search.split(';');
      for (const term of terms) {
        const q = term.trim();
        if (q !== '') {
          await this.appService.addCard(q);
        }
      }
    } catch (error) {
      console.log(error);
    }
  }

  isError(t: CardItem) {
    return t.type === CardType.Error;
  }
  isPassage(t: CardItem) {
    return t.type === CardType.Passage;
  }
  isNote(t: CardItem) {
    return t.type === CardType.Note;
  }
  isStrongs(t: CardItem) {
    return t.type === CardType.Strongs;
  }
  isWords(t: CardItem) {
    return t.type === CardType.Word;
  }

  //#endregion

  //#region Typeahead/Autocomplete

  select(selection: any): void {
    this.search(selection);
  }

  //#endregion
}

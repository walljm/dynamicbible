import { Component } from '@angular/core';

import { NavService } from '../../services/nav.service';

@Component({
  selector: 'app-help-modal-component',
  templateUrl: './help-modal.component.html',
  styleUrls: ['./help-modal.component.scss'],
  preserveWhitespaces: true,
})
export class HelpModalComponent {
  constructor(public navService: NavService) {}
}

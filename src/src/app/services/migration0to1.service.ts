import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/compat/database';
import { DataSnapshot } from '@angular/fire/compat/database/interfaces';
import { UUID } from 'angular2-uuid';

import { Overlap } from '../common/bible-reference';
import { Storable, StorableType } from '../common/storable';
import { Settings, User } from '../models/app-state';
import { CardType, DataReference } from '../models/card-state';
import { SavedPage } from '../models/page-state';
import { AppService } from './app.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root',
})
export class MigrationVersion0to1 {
  private settingsPath = 'settings'; // the very first settings
  private settingsRemoteObject: AngularFireObject<Version0Settings>;

  constructor(private remote: AngularFireDatabase, private appService: AppService) {}

  migrate(user: User, storage: StorageService) {
    this.settingsRemoteObject = this.remote.object<Version0Settings>(`/${this.settingsPath}/${user.uid}`);
    this.settingsRemoteObject.query.once('value').then((data: DataSnapshot) => {
      if (!data.exists()) {
        return;
      }
      const v: Version0Settings = data.val();
      if (v === null || v === undefined) {
        return; // no settings to migrate.
      }

      const settings = this.toDisplaySettings(v);
      const savedPages = this.toSavedPages(v);
      const currentCards = this.toCurrentCards(v);

      storage.setRemoteData(
        settings,
        savedPages,
        {
          type: StorableType.modified,
          createdOn: new Date(0).toISOString(),
          value: [],
        },
        currentCards
      );
    });
  }

  private toDisplaySettings(v0: Version0Settings) {
    return new Storable({
      displaySettings: {
        showStrongsAsModal: v0.strongs_modal ? v0.strongs_modal : false,
        appendCardToBottom: v0.append_to_bottom ? v0.append_to_bottom : true,
        insertCardNextToItem: v0.insert_next_to_item ? v0.insert_next_to_item : true,
        clearSearchAfterQuery: v0.clear_search_after_query ? v0.clear_search_after_query : true,
        cardFontSize: v0.font_size ? v0.font_size : 12,
        cardFontFamily: v0.font_family ? v0.font_family : 'PT Serif',
        showVersesOnNewLine: v0.verses_on_new_line ? v0.verses_on_new_line : false,
        showVerseNumbers: v0.show_verse_numbers ? v0.show_verse_numbers : false,
        showParagraphs: v0.show_paragraphs ? v0.show_paragraphs : true,
        showParagraphHeadings: v0.show_paragraph_headings ? v0.show_paragraph_headings : true,
        syncCardsAcrossDevices: v0.sync_search_items ? v0.sync_search_items : false,
      },
      pageSettings: {
        mergeStrategy: Overlap.Equal,
      },
      cardIcons: {
        words: 'font_download',
        passage: 'menu_book',
        strongs: 'speaker_notes',
        note: 'note',
        savedPage: 'inbox',
      },
    } as Settings);
  }

  private toSavedPages(v0: Version0Settings) {
    return new Storable(
      v0.saved_pages
        ? v0.saved_pages.map((p) => {
            return {
              queries: p.queries.map((q) => {
                return {
                  qry: q.qry,
                  type: CardType[q.type],
                } as DataReference;
              }),
              title: p.title,
              id: UUID.UUID().toString(),
            } as SavedPage;
          })
        : []
    );
  }

  private toCurrentCards(v0: Version0Settings) {
    return new Storable(
      v0.items
        ? v0.items.map((o) => {
            return {
              qry: o.qry,
              type: CardType[o.type],
            };
          })
        : []
    );
  }
}

interface Version0Settings {
  append_to_bottom: boolean;
  clear_search_after_query: boolean;
  font_size: number;
  font_family: string;
  insert_next_to_item: boolean;
  show_paragraph_headings: boolean;
  show_paragraphs: boolean;
  show_verse_numbers: boolean;
  strongs_modal: boolean;
  sync_search_items: boolean;
  uid: string;
  username: string;
  verses_on_new_line: boolean;
  saved_pages: Version0SavedPage[];
  items: Version0Item[];
}

interface Version0SavedPage {
  queries: Version0Item[];
  title: string;
}

interface Version0Item {
  dict: string;
  qry: string;
  type: string;
}

import {Injectable } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Injectable({
  providedIn: 'root',
})
export class NavService {
  private sidenav: MatSidenav;
  private settings: MatSidenav;

  public afterViewInit(sidenav: MatSidenav, settings: MatSidenav) {
    this.sidenav = sidenav;
    this.settings = settings;
  }

  public openNav() {
    return this.sidenav?.open();
  }

  public async closeNav() {
    const r = await this.sidenav?.close();
    return r;
  }

  public toggleNav(): void {
    this.sidenav?.toggle();
  }

  public openSettings() {
    return this.settings?.open();
  }

  public async closeSettings() {
    const r = await this.settings?.close();
    return r;
  }

  public toggleSettings(): void {
    this.settings?.toggle();
  }
}

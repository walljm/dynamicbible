﻿using System.Collections.Generic;

namespace DynamicBibleUtility
{
    #region RMAC

    public class RMAC
    {
        public string id { get; set; }
        public List<string> d { get; set; }
    }

    public class RMACCrossRef
    {
        //  <s id='1' r='n-li' />
        public string i { get; set; }

        public string r { get; set; }
    }

    #endregion RMAC

    #region Strongs Dict

    public class SDict
    {
        public Dictionary<string, StrongDictEntry> Entries { get; set; }
    }

    //"G208":{"lemma":"ἀκυρόω","kjv_def":"disannul, make of none effect","strongs_def":" to invalidate","derivation":"from G1 (Α) (as a negative particle) and G2964 (κυρόω);"},

    public class StrongDictEntry
    {
        // number
        public int n { get; set; }

        public string i { get; set; } // G1/H1
        public string p { get; set; } // pronounciation
        public string lemma { get; set; } // greek/hebrew
        public string tr { get; set; } // translation
        public List<StrongsDictEntryPart> de { get; set; } = new List<StrongsDictEntryPart>(); // Description
    }

    public class StrongsDictEntryPart
    {
        public string sn {get;set; }
        public string w {get;set; }
    }

    #endregion Strongs Dict

    #region Strongs Cross Refs

    public class Strongs
    {
        public string id = "";
        public string t = "";
        public string d = "";
        public List<StrongRef> ss = new List<StrongRef>();
    }

    public class StrongRef
    {
        public string w = "";
        public List<BibleRef> rs = new List<BibleRef>();
    }

    public class BibleRef
    {
        public string r = "";
    }

    #endregion Strongs Cross Refs

    #region Bible

    public class Book
    {
        public int bk = -1;
        public List<Chapter> chs = new List<Chapter>();
    }

    public class Chapter
    {
        public int ch = -1;
        public List<Verse> vss = new List<Verse>();
    }

    public class Verse
    {
        public int v = -1;
        public List<Text> w = new List<Text>();
    }

    public class Text
    {
        public Text(string text, string strongs)
        {
            t = text;
            s = strongs;
        }

        public Text(string text)
        {
            t = text;
        }

        public string t = "";
        public string s = "";
    }

    #endregion Bible
}
using System.Text.Json.Serialization;

namespace DynamicBible.DataPreparation.Models;

public class RMAC
{
    [JsonPropertyName("id")]
    public string? Id { get; set; }

    [JsonPropertyName("d")]
    public List<string>? Description { get; set; }
}

public class RMACCrossRef
{
    //  <s id='1' r='n-li' />
    [JsonPropertyName("i")]
    public string? Id { get; set; }

    [JsonPropertyName("r")]
    public string? Reference { get; set; }
}